import React, { useState, useEffect } from "react";
import { Form, Alert, InputGroup, Button, ButtonGroup } from "react-bootstrap";
import BookDataService from "./helper";
import PhoneInput from 'react-phone-number-input'
import styled from "styled-components";
import Navbar from './Navbar'
import { Timestamp } from "firebase/firestore";


const Donorpost = ({id, setBookId }) => {
  const [name, setName] = useState("");
  const [gender, setGender] = useState("");
  const [age, setAge] = useState("");
  const [mobile, setMobile] = useState("");
  const [blood, setBlood] = useState("");
  const [location, setLocation] = useState("");
  const [dzongkha, setDzongkha] = useState("");
  const [createdAt, setcreatedAt] = useState("");
  const [status, setStatus] = useState("Available");
  const [flag, setFlag] = useState(true);
  const [message, setMessage] = useState({ error: false, msg: "" });
  

  const handleSubmit = async (e) => {
    e.preventDefault();
    setMessage("");
    if (name === "" || age === "" || gender==="" || mobile==="" || blood==="" || location===""
    || dzongkha==="") {
      setMessage({ error: true, msg: "All fields are mandatory!" });
      return;
    }
    const newBook = {
        name,
        gender,
        age,
        mobile,
        blood,
        location,
        dzongkha,
        status,
        createdAt:Timestamp.now().toDate()
    };
    console.log(newBook);

    try {
      if (id !== undefined && id !== "") {
        await BookDataService.updateBook(id, newBook);
        setBookId("");
        setMessage({ error: false, msg: "Updated successfully!" });
      } else {
        await BookDataService.addBooks(newBook);
        setMessage({ error: false, msg: "Donor added successfully!" });
      }
    } catch (err) {
      setMessage({ error: true, msg: err.message });
    }

    setName("");
    setGender("");
    setAge("");
    setMobile("");
    setBlood("");
    setLocation("");
    setDzongkha("");
  };

  const editHandler = async () => {
    setMessage("");
    try {
      const docSnap = await BookDataService.getBook(id);
      console.log("the record is :", docSnap.data());
      setName(docSnap.data().name);
      setGender(docSnap.data().gender);
      setAge(docSnap.data().age);
      setMobile(docSnap.data().mobile);
      setBlood(docSnap.data().blood);
      setLocation(docSnap.data().location);
      setDzongkha(docSnap.data().dzongkha);
      setcreatedAt(docSnap.data().createdAt);
      setStatus(docSnap.data().status);
    } catch (err) {
      setMessage({ error: true, msg: err.message });
    }
  };



  useEffect(() => {
    console.log("The id here is : ", id);
    if (id !== undefined && id !== "") {
      editHandler();
    }
  }, [id]);
  return (
    <>
   {/* <Navbar/> */}
  
      <div className="p-4 box" style={{marginTop:60}}>
        {message?.msg && (
          <Alert
            variant={message?.error ? "danger" : "success"}
            dismissible
            onClose={() => setMessage("")}
          >
            {message?.msg}
          </Alert>
        )}

        <Form onSubmit={handleSubmit} style={{marginTop:'5%',width:'40%',
        backgroundColor:'#cccc',
        marginLeft:'30%',
        borderRadius:10}}>
          <Form.Group className="mb-3" controlId="formBookTitle" 
          style={{width:'70%',marginLeft:'15%'}}>
            <InputGroup>
              <Form.Control
                type="text"
                placeholder="Name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </InputGroup>
          </Form.Group>
          <div onChange={(e)=>setGender(e.target.value)}
           style={{width:'70%',marginLeft:'35%'}}>
            <input type="radio" value="Male" name="gender" /> Male
            <input type="radio" value="Female" name="gender" /> Female
            <input type="radio" value="Other" name="gender" /> Other
         </div>

          <Form.Group className="mb-3" controlId="formBookAuthor"
           style={{width:'70%',marginLeft:'15%'}}>
            <InputGroup>
              <Form.Control
                type="text"
                placeholder="age"
                value={age}
                onChange={(e) => setAge(e.target.value)}
              />
            </InputGroup>
            
           
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBookAuthor"
           style={{width:'70%',marginLeft:'15%'}}>
            <InputGroup>
              <Form.Control
                type="number"
                placeholder="Phonenumber"
                value={mobile}
                onChange={(e) => setMobile(e.target.value)}
              />
            </InputGroup>
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBookAuthor"
           style={{width:'70%',marginLeft:'15%'}}>
        <InputGroup>
          <select value={blood} onChange={(e)=>setBlood(e.target.value)}> 
            <option value="Blood Group">Blood Group</option>
            <option value="O+">O+</option>
            <option value="O-">O-</option>
            <option value="A+">A+</option>
            <option value="A-">A-</option>
            <option value="B-">B-</option>
            <option value="B+">B+</option>
            <option value="AB-">AB-</option>
            <option value="AB+">AB+</option>
          </select>
          </InputGroup>
          </Form.Group> 
  
          <Form.Group className="mb-3" controlId="formBookAuthor"
           style={{width:'70%',marginLeft:'15%'}}>
            <InputGroup>
              <Form.Control
                type="text"
                placeholder="Location"
                value={location}
                onChange={(e) => setLocation(e.target.value)}
              />
            </InputGroup>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBookAuthor"
           style={{width:'70%',marginLeft:'15%'}}>
        <InputGroup>
          <select value={dzongkha} onChange={(e)=>setDzongkha(e.target.value)}> 
            <option value="Select Dzonghhag">Select Dzongkhag</option>
            <option value="Thimphu">Thimphu</option>
            <option value="Paro">Paro</option>
            <option value="Haa">Haa</option>
            <option value="Gasa">Gasa</option>
            <option value="Punakha">Punakha</option>
            <option value="Dagana">Danaga</option>
            <option value="Tsirang">Tsirang</option>
            <option value="Sarpang">Sarpang</option>
            <option value="Samdrup Jongkhar">Samdrup Jongkhar</option>
            <option value="Chukha">Chukha</option>
            <option value="Pemagatshel">Pemagatshel</option>
            <option value="Tashigang">Tashigang</option>
            <option value="Zhemgang">Zhemgang</option>
            <option value="Mongar">Mongar</option>
            <option value="Dagana">Danaga</option>
            <option value="Bumthang">Bumthang</option>
            <option value="Lhuntse">Lhuntse</option>
            <option value="Trongsa">Trongsa</option>
            <option value="Wangduephodrang">Wangduephodrang</option>
          </select>
          </InputGroup>
          </Form.Group> 
          <ButtonGroup aria-label="Basic example" className="mb-3"
          style={{width:'70%',marginLeft:'15%'}}>
            <Button
              disabled={flag}
              variant="success"
              onClick={(e) => {
                setStatus("Available");
                setFlag(true);
              }}
            >
              Available
            </Button>
            <Button
              variant="danger"
              disabled={!flag}
              onClick={(e) => {
                setStatus("Not Available");
                setFlag(false);
              }}
            >
              Not Available
            </Button>
          </ButtonGroup>
          <div className="d-grid gap-2">
            <Button variant="primary" type="Submit"
             style={{width:'70%',marginLeft:'15%'}}>
              Add/ Update
            </Button>
          </div>
        </Form>
      </div>
    </>
  );
};

export default Donorpost;


// import React, { useEffect, useState } from 'react'
// import {Button,Form,Grid,Loader} from "semantic-ui-react";
// import { storage,db } from '../firebase';
// import { useParams,useNavigate} from 'react-router-dom';
// import {ref,getDownloadURL, uploadBytesResumable } from 'firebase/storage';
// import { addDoc,getDoc,doc, collection, serverTimestamp, updateDoc } from 'firebase/firestore';
// import Navigationbar from '../component/Navigationbar';

// const initialState={
// title:"",
// artist:"",
// lyric:"",
    
// }
// const Donorpost = () => {
//     const [data,setData] = useState(initialState);
//     const {title,artist,lyric}=data;
//     const [file,setFile] = useState(null);
//     const [progress,setProgress] = useState(null);
//     const [errors,setErrors] = useState({});
//     const [isSubmit,setIsSubmit] = useState(false);
//     const navigate=useNavigate();
//     const {id}=useParams();
    
//     useEffect(()=> {
//     id && getSingleUser();
//     },[id])


//     const getSingleUser= async () => {
//         const docRef= doc(db,"Zhungdra",id);
//         const snapshot=await getDoc(docRef);
//         if(snapshot.exists()){
//             setData({...snapshot.data()});
//         }
//     };

   

//     useEffect(() => {
//         const uploadFile = () => {
//           const name=new Date().getTime()+ file.name;
//           const storageRef = ref(storage, `Zhungdra/${file.name}`);
//           const uploadTask = uploadBytesResumable(storageRef, file);

//           uploadTask.on(
//             "state_changed",
//             (snapshot) => {
//               const progress =
//                 (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
//               console.log("Upload is " + progress + "% done");
//               setProgress(progress);
//               switch (snapshot.state) {
//                 case "paused":
//                   console.log("Upload is paused");
//                   break;
//                 case "running":
//                   console.log("Upload is running");
//                   break;
//                 default:
//                   break;
//               }
//             },
//             (error) => {
//               console.log(error);
//             },
//             () => {
//               getDownloadURL(uploadTask.snapshot.ref).then((downloadUrl) => {
//                 setData((prev) => ({...prev, audioUrl:downloadUrl }));
//               });
//             }
//           );
//         };
    
//         file && uploadFile();
//       }, [file]);

// const  handleChange=(e)=>{
//     setData({...data,[e.target.name]:e.target.value})
// };
// const validate=()=>{
//    let errors={};
//    if(!title){
//        errors.title="Title cannot be empty"
//    }
 
// if(!lyric){
//     errors.lyric="Lyric cannot be empty";
    
// }
// return errors;
// }
// const handleSubmit= async (e)=>{
//     e.preventDefault();
//     let errors=validate();
//     if(Object.keys(errors).length) return setErrors(errors);
//     setIsSubmit(true);
//     if(!id){
//         try {
//             await addDoc(collection(db,"Zhungdra"),{
//                 ...data,
//                 timestamp: serverTimestamp()
//             });
          
//         } catch (error){
//             console.log(error);
//         }       
//     } else {
//         try {
//             await updateDoc(doc(db,"Zhungdra",id),{
//                 ...data,
//                 timestamp: serverTimestamp()
//             });
          
//         } catch (error){
//             console.log(error);
//         } 
//     }
//     navigate("/Viewzhungdra");
// }
//   return (
//     <div>
//       <Navigationbar/>
//         <Grid centered verticalAlign='middle' columns="3" style={{height:"80vh"}}>
//             <Grid.Row>
//                 <Grid.Column textAlign='center'>
//                     <div>
//                         {isSubmit ? <Loader active inline="centered" size='huge'/>:
//                         (
//                             <>
//                                 <h2>{id ? "Update Zhungdra" : "Add Zhungdra" }</h2>
//                                 <Form onSubmit={handleSubmit}
//                                 style={{
//                                         width: 650, 
//                                         padding: 25,
//                                         height:'100%',
//                                         borderWidth:1,
//                                         borderColor:'#cccc',
//                                         backgroundColor:'#cccc',
//                                         marginTop:'10%',
//                                         borderRadius:6}}>
//                                     <Form.Input
//                                       label="Title"
//                                       style={{ borderWidth:2,
//                                              borderColor:'red',}}
//                                       error={errors.title ? {content:errors.title}:null}
//                                       placeholder="Enter Title"
//                                       name="title"
//                                       onChange={handleChange}
//                                       value={title}
//                                       autoFocus/>
//                                     <Form.Input
                              
//                                     label="Artist"
//                                     placeholder="Artist name"
//                                     name="artist"
//                                     onChange={handleChange}
//                                     value={artist}
//                                     autoFocus/>
                                      
//                                     <Form.TextArea
//                                     error={errors.lyric ? {content:errors.lyric}:null}
//                                     label="Lyric"
//                                     style={{height:'300px'}}
//                                     placeholder="Write your Lyric here"
//                                     name="lyric"
//                                     onChange={handleChange}
//                                     value={lyric}
                                    
//                                     autoFocus/>
//                                     <Form.Input
//                                         label="Upload"
//                                         type='file'
//                                         onChange={(e)=>setFile(e.target.files[0])}
//                                     />
//                                     <Button 
//                                         primary 
//                                         type='submit' 
//                                         disabled={progress  !==null && progress<100}>
//                                         Submit
//                                     </Button>
//                                 </Form>
//                             </>
//                         )}
//                     </div>
//                 </Grid.Column>
//             </Grid.Row>
//         </Grid>
//     </div>
//   )
// }

// export default Donorpost;