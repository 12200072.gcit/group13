// ;


// import { initializeApp } from "firebase/app";
// import { getFirestore } from 'firebase/firestore';
// import { getStorage } from "firebase/storage";
// import { getAuth } from "firebase/auth";


// const firebaseConfig = {

//   apiKey: "AIzaSyDfr_oVeWC9JtvtVuuvB6ZREggHYQi6fhE",
//   authDomain: "luyang-ef300.firebaseapp.com",
//   databaseURL: "https://luyang-ef300-default-rtdb.firebaseio.com",
//   projectId: "luyang-ef300",
//   storageBucket: "luyang-ef300.appspot.com",
//   messagingSenderId: "5432122993",
//   appId: "1:5432122993:web:59cf95feb7dd4faa28c5d0",
//   measurementId: "G-DCTHH1H6YR"

// };


// // Initialize Firebase
// const app = initializeApp(firebaseConfig);
// export const storage=getStorage(app);
// export const db=getFirestore(app);
// export const auth=getAuth(app);
// export default getFirestore();



import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore';
import { getStorage } from "firebase/storage";
import { getAuth } from "firebase/auth";

const firebaseConfig = {

  apiKey: "AIzaSyASbVoc7jZNgAcMWzrzYhNbO7NxgHAih1g",
  authDomain: "bloodapp-7b0aa.firebaseapp.com",
  databaseURL: "https://bloodapp-7b0aa-default-rtdb.firebaseio.com",
  projectId: "bloodapp-7b0aa",
  storageBucket: "bloodapp-7b0aa.appspot.com",
  messagingSenderId: "566280080920",
  appId: "1:566280080920:web:1664b7d43bd052eba13d6d",
  measurementId: "G-G8K3PBZRGW"

};


// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const storage=getStorage(app);
export const db=getFirestore(app);
export const auth=getAuth(app);
export default getFirestore();
