import './App.css';

import {BrowserRouter,Routes,Route} from "react-router-dom";
import Feedback from './pages/feedback';
import Login from './pages/Login'
import Viewrequest from './pages/Viewrequesr';
import Addrequest from './pages/Addrequest';
import Viewdonor from './pages/Viewdonor';
import Adddonor from './pages/Adddonor';
import Viewfeedback from './pages/feedback';

function App() {
  return (
    <BrowserRouter>
       <div className="App">
         <Routes>
         <Route path='/' element={<Login/>}/>
           <Route path='/adddonor' element={<Adddonor/>}/>
           <Route path='/Addrequest' element={<Addrequest/>}/>
           <Route path='/feedback' element={<Feedback/>}/>
           <Route path='/Viewdonor' element={<Viewdonor/>}/>
           <Route path='/Viewrequest' element={<Viewrequest/>}/>
           <Route path='/Viewfeedback' element={<Viewfeedback/>}/>
           <Route path='/update/:id' element={<Adddonor/>}/>
           <Route path='/update1/:id' element={<Addrequest/>}/>
         </Routes>
      </div>
    </BrowserRouter>
   
  );
}

export default App;
