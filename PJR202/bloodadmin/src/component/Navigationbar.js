


import { Nav, Navbar, NavLink,NavDropdown } from "react-bootstrap";
import { Link } from "react-router-dom";

const Navigationbar = () => {
    return (
        <Navbar collapseOnSelect expand="sm" 
        style={{backgroundColor:'#bb121a',
                position:'sticky',
                top:0,
                width:'100%',
                zIndex:1,
               }}>
            <Navbar.Toggle aria-controls="navbarScroll" data-bs-toggle="collapse" data-bs-target="#navbarScroll" />
            <Navbar.Collapse id="navbarScroll">
                <Nav>
                    <NavLink  
                    style={{marginLeft:30,color:'white',fontSize:22,fontFamily:'serif',fontWeight:'bold'}}>Donate Blood</NavLink>
                    <NavLink  eventKey="2" as={Link} to="/Viewdonor"
                    style={{marginLeft:30,color:'white',fontSize:22,fontFamily:'serif'}}>View Donor</NavLink>
                    <NavLink  eventKey="3" as={Link} to="/Viewrequest"
                    style={{marginLeft:30,color:'white',fontSize:22,fontFamily:'serif'}}>View Request</NavLink>

                      <NavDropdown style={{marginLeft:30,color:'white',fontSize:20,fontFamily:'serif',
                           position:'relative',display:'inline-block',zIndex:1}} title={<text style={{color:'white'}}>Post</text>}>
                    
                        <NavDropdown.Item href="/Adddonor">Add Donor</NavDropdown.Item>
                        <NavDropdown.Item href="/Addrequest">Add Request</NavDropdown.Item>
                      
                    </NavDropdown>
                    <NavLink  eventKey="6" as={Link} to="/Viewfeedback"
                    style={{marginLeft:30,color:'white',fontSize:22,fontFamily:'serif'}}>View Feedback</NavLink>
                    <NavLink  eventKey="7" as={Link} to="/"
                    style={{marginLeft:30,color:'white',fontSize:22,fontFamily:'serif'}}>Logout</NavLink>
                </Nav>
            </Navbar.Collapse>     
        </Navbar>
    );
}
 
export default Navigationbar;;