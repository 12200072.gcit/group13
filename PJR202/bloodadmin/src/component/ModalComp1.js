import React from 'react'
import {Modal,Header,Image,Button, ModalDescription,Form} from "semantic-ui-react";

import '../../src/pages/form.css'


const ModalComp1 = ({
    open,
    setOpen,
    img,
    name,
    gender,
    age,
    mobile,
    blood,
    require,
    location,
    dzongkha,
    detail,
    id,
    handleDelete,
}) => {
  return (

    <Modal
     style={{width:'100%'}}
    onClose={()=> setOpen(false)} 
    onOpen={()=> setOpen(true)} 
    open={open}>
       <Modal.Header>More detail</Modal.Header>   
            <div className='center' style={{marginTop:'4%'}}> 
            <div className='auth'  style={{border:'1px solid #cccc',backgroundColor:'#095aba',
          borderRadius:6}}>
            <form  name='login_form' style={{border:'1px solid #cccc',
            backgroundColor:'white',
             alignItems:'flex-start',marginLeft:'5px'}}>  
            <p style={{fontFamily:'sans-serif'}}>
               Name:{name}
            </p>
            <p style={{fontFamily:'sans-serif'}}>
               Gender:{gender}
            </p>
            <p style={{fontFamily:'sans-serif'}}>
               Age:{age}
            </p>
            <p style={{fontFamily:'sans-serif'}}>
               Phonenumber:{mobile}
            </p>
            <p style={{fontFamily:'sans-serif'}}>
               Blood Group:{blood}
            </p>
            <p style={{fontFamily:'sans-serif'}}>
               Required Unit:{require}
            </p>
            <p style={{fontFamily:'sans-serif'}}>
               Location:{location}
            </p>
            <p style={{fontFamily:'sans-serif'}}>
               Dzongkhag:{dzongkha}
            </p>
            <p style={{fontFamily:'sans-serif'}}>
               More detail
            </p>
            <p style={{fontSize:'3vh',fontFamily:'sans-serif'}}>
               {detail}
            </p>
            </form>
            <Button color='black' onClick={()=>setOpen(false)}>
               Cancel
           </Button>
           <Button 
           color='red'
           icon="checkmark" 
           content="Delete" 
           labelPosition='right' 
           onClick={()=>handleDelete(id)}/>   
            </div>
            </div>
   </Modal>   
  )
}

export default ModalComp1;