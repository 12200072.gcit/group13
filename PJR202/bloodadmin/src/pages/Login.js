import {useState} from 'react'
import { Link } from 'react-router-dom'
import './form.css'
import {signInWithEmailAndPassword} from 'firebase/auth'
import {auth} from '../firebase'
import {useNavigate} from 'react-router-dom'
import Logo from '../asset/logo.png'



function Login(){

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('') 
  const [error, setError] = useState('')
  const[loading,setLoading]=useState(false);

  const navigate = useNavigate()

  const login = e => {
    e.preventDefault()
    signInWithEmailAndPassword(auth, email, password)
    .then(() => {
      setLoading(false);
      navigate('/Viewdonor')
   
    })
    .catch(err => setError(err.message))
    setLoading(false);
  }

  return(
    <div className='center'>
       
      <div className='auth'>
      <img src={Logo} style={{width:'35%',height:115,borderRadius:100}}/>
    
        <h1>Admin</h1>
        {error && <div className='auth__error'>{error}</div>}
        <form onSubmit={login} name='login_form'>
          <input 
            type='email' 
            value={email}
            required
            placeholder="Enter your email"
            onChange={e => setEmail(e.target.value)}/>

          <input 
            type='password'
            value={password}
            required
            placeholder='Enter your password'
            onChange={e => setPassword(e.target.value)}/>

          <button type='submit'
          disabled={loading}>Login</button>
        </form>
       
      </div>
    </div>
    
  )
}

export default Login