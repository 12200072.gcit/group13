import React, { useEffect, useState } from 'react'
import {Button,Form,Grid,Loader} from "semantic-ui-react";
import { storage,db } from '../firebase';
import { useParams,useNavigate} from 'react-router-dom';
import {ref,getDownloadURL, uploadBytesResumable } from 'firebase/storage';
import { addDoc,getDoc,doc, collection, serverTimestamp, updateDoc } from 'firebase/firestore';
import Navigationbar from '../component/Navigationbar';
import './form.css'

const initialState={
      name:"",
      gender:"",
      age:"",
      mobile:"",
      blood:"",
      require:"",
      location:"",
      dzongkha:"",
      detail:"", 
}
const Addrequest = () => {
    const [data,setData] = useState(initialState);
    const {name,gender,age,mobile,blood,require,location,dzongkha,detail}=data;
    const [file,setFile] = useState(null);
    const [progress,setProgress] = useState(null);
    const [errors,setErrors] = useState({});
    const [isSubmit,setIsSubmit] = useState(false);
    const navigate=useNavigate();
    const {id}=useParams();
    
    useEffect(()=> {
    id && getSingleUser();
    },[id])


    const getSingleUser= async () => {
        const docRef= doc(db,"Request",id);
        const snapshot=await getDoc(docRef);
        if(snapshot.exists()){
            setData({...snapshot.data()});
        }
    };

   

    useEffect(() => {
        const uploadFile = () => {
          const name=new Date().getTime()+ file.name;
          const storageRef = ref(storage, `Request/${file.name}`);
          const uploadTask = uploadBytesResumable(storageRef, file);

          uploadTask.on(
            "state_changed",
            (snapshot) => {
              const progress =
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
              console.log("Upload is " + progress + "% done");
              setProgress(progress);
              switch (snapshot.state) {
                case "paused":
                  console.log("Upload is paused");
                  break;
                case "running":
                  console.log("Upload is running");
                  break;
                default:
                  break;
              }
            },
            (error) => {
              console.log(error);
            },
            () => {
              getDownloadURL(uploadTask.snapshot.ref).then((downloadUrl) => {
                setData((prev) => ({...prev, audioUrl:downloadUrl }));
              });
            }
          );
        };
    
        file && uploadFile();
      }, [file]);

const  handleChange=(e)=>{
    setData({...data,[e.target.name]:e.target.value})
};
const validate=()=>{
   let errors={};
   if(!name){
       errors.name="Name cannot be empty"
   }
 
if(!gender){
    errors.gender=" Please select gender";
    
}
if(!age){
  errors.age="Please enter age";
  
}
if(!mobile){
  errors.mobile="Please enter phonenumber";
  
}
if(!blood){
  errors.blood="Please select blood group";
  
}
if(!require){
  errors.require="Please enter require unit";
  
}
if(!location){
  errors.location="Please enter location";
  

}
if(!dzongkha){
  errors.dzongkha="Please select dzongkhag";
  
}


return errors;
}
const handleSubmit= async (e)=>{
    e.preventDefault();
    let errors=validate();
    if(Object.keys(errors).length) return setErrors(errors);
    setIsSubmit(true);
    if(!id){
        try {
            await addDoc(collection(db,"Request"),{
                ...data,
                createdAt: serverTimestamp()
            });
          
        } catch (error){
            console.log(error);
        }       
    } else {
        try {
            await updateDoc(doc(db,"Request",id),{
                ...data,
                timestamp: serverTimestamp()
            });
          
        } catch (error){
            console.log(error);
        } 
    }
    navigate("/Viewrequest");
}
  return (
    <>
    <Navigationbar/>
    
    <div className='center' style={{marginTop:'25%'}}> 
    <div className='auth'>
                {isSubmit ? <Loader active inline="centered" size='huge'/>:
                (
                    <>
                        <h2>{id ? "Update Request" : "Add Request" }</h2>
                        <Form onSubmit={handleSubmit}
                        style={{
                                width: 650, 
                                padding: 25,
                                height:'100%',
                                borderWidth:1,
                                borderColor:'#cccc',
                                backgroundColor:'#095aba',
                                marginTop:'10%',
                                borderRadius:6}}>
                            
                            <Form.Input
                              style={{ borderWidth:2,
                                      borderColor:'red',}}
                              error={errors.name ? {content:errors.name}:null}
                              placeholder="Name"
                              name="name"
                              onChange={handleChange}
                              value={name}
                              autoFocus/>
          

                          <div 
                              style={{ borderWidth:2,
                              borderColor:'red',}}
                              error={errors.gender ? {content:errors.gender}:null}
                              name="gender"
                              onChange={handleChange}
                              value={gender}
                              autoFocus>
                              <input type="radio" value="Male" name="gender" /> Male
                              <input type="radio" value="Female" name="gender" /> Female
                              <input type="radio" value="Other" name="gender" /> Other
                          </div>


                          <Form.Input
                              style={{ borderWidth:2,
                                borderColor:'red',}}
                              error={errors.age ? {content:errors.age}:null}
                      
                            placeholder="Age"
                            name="age"
                            onChange={handleChange}
                            value={age}
                            type='number'
                            autoFocus/>
                              
                              
                              <Form.Input
                              style={{ borderWidth:2,
                                borderColor:'red',}}
                              error={errors.mobile ? {content:errors.mobile}:null}
                      
                            placeholder="Phonenumber"
                            type='number'
                            name="mobile"
                            onChange={handleChange}
                            value={mobile}
                            autoFocus/>
                              
                            
                        
                                <Form.Input>
                                <select 
                                    error={errors.blood ? {content:errors.blood}:null}
                                    name="blood"
                                    onChange={handleChange}
                                    value={blood}
          
                                    autoFocus> 
                                <option value="Blood Group">Blood Group</option>
                                <option value="O+">O+</option>
                                <option value="O-">O-</option>
                                <option value="A+">A+</option>
                                <option value="A-">A-</option>
                                <option value="B-">B-</option>
                                <option value="B+">B+</option>
                                <option value="AB-">AB-</option>
                                <option value="AB+">AB+</option>
                                </select>
                                </Form.Input>


                          
                            <Form.Input
                            error={errors.require ? {content:errors.require}:null}
                            name="require"
                            placeholder="Required unit"
                            onChange={handleChange}
                            value={require}
                            
                            autoFocus/>


                          <Form.Input
                            error={errors.location ? {content:errors.location}:null}
                            name="location"
                            placeholder="Location"
                            onChange={handleChange}
                            value={location}
                            
                            autoFocus/>


                  <Form.Input>
                    <select
                    error={errors.dzongkha ? {content:errors.dzongkha}:null}
            
                
                  name="dzongkha"
                  onChange={handleChange}
                  value={dzongkha}
                  autoFocus> 
                      <option value="Select Dzonghhag">Select Dzongkhag</option>
                      <option value="Thimphu">Thimphu</option>
                      <option value="Paro">Paro</option>
                      <option value="Haa">Haa</option>
                      <option value="Gasa">Gasa</option>
                      <option value="Punakha">Punakha</option>
                      <option value="Dagana">Danaga</option>
                      <option value="Tsirang">Tsirang</option>
                      <option value="Sarpang">Sarpang</option>
                      <option value="Samdrup Jongkhar">Samdrup Jongkhar</option>
                      <option value="Chukha">Chukha</option>
                      <option value="Pemagatshel">Pemagatshel</option>
                      <option value="Tashigang">Tashigang</option>
                      <option value="Zhemgang">Zhemgang</option>
                      <option value="Mongar">Mongar</option>
                      <option value="Dagana">Danaga</option>
                      <option value="Bumthang">Bumthang</option>
                      <option value="Lhuntse">Lhuntse</option>
                      <option value="Trongsa">Trongsa</option>
                      <option value="Wangduephodrang">Wangduephodrang</option>
                    </select>
                  </Form.Input>

                  <Form.TextArea
                            placeholder="More detail..."
                            name="detail"
                            onChange={handleChange}
                            value={detail}
                            cols={10}
                            rows={10}
                            autoFocus/>
                            
                            <Button 
                                style={{backgroundColor:'white'}} 
                                type='submit' 
                                disabled={progress  !==null && progress<100}>
                                Submit
                            </Button>
                        </Form>
                    </>
                )}
            </div>
    </div>
    </>
  )
}

export default Addrequest;
