import React,{useEffect,useState} from 'react'
import {db} from "../firebase";
import { Button,Card,Grid,Container,Image, CardHeader, CardDescription, CardContent } from 'semantic-ui-react';
import { useNavigate } from 'react-router-dom';
import { collection, deleteDoc, doc, onSnapshot } from 'firebase/firestore';
import Modaldonor from '../component/Modaldonor';
import Spinner from '../component/Spinner';
import Navigationbar from '../component/Navigationbar'

import './form.css'
<meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>



const Viewdonor = () => {
    const [users,setUsers] = useState([]);
    const [open,setOpen] = useState(false);
    const [content,setContent] = useState([]);
    const [loading,setLoading]=useState(false);
    const navigate=useNavigate();

    useEffect(()=>{
        setLoading(true);
        const unsub =onSnapshot(collection(db,"Donor"), (snapshot)=>{
            let list=[];
            snapshot.docs.forEach((doc)=>{
                list.push({id:doc.id, ...doc.data()});
            });
            setUsers(list);
            setLoading(false)
        },
        
        (error)=>{
            console.log(error);
        }
    );
        return ()=>{
            unsub();
        };
    },[]);
    if(loading){
        return <Spinner/>
    }
 const handleModal=(item)=>{
     setOpen(true);
     setContent(item);
 }
 const handleDelete= async(id)=>{
     if(window.confirm("Are You sure want to delete?")){
         try{
             setOpen(false);
             await deleteDoc(doc(db,"Donor",id));
             setUsers(users.filter((content)=> content.id !==id));
         }
         catch(err){
             console.log(err);
         }
     }
 };
  return (
    <>
    <Navigationbar/>   
 
                {users && users.map((item)=>(
            <div className='center'>
            <div className='auth'  style={{border:'1px solid #cccc',backgroundColor:'#095aba',
        borderRadius:6}}>
            <form  name='login_form' style={{border:'1px solid #cccc',
            backgroundColor:'white',borderRadius:6,alignItems:'flex-start',marginLeft:'5px'}}>  
            <p style={{fontFamily:'sans-serif'}}>
               Name:{item.name}
            </p>
            <p style={{fontFamily:'sans-serif'}}>
               Gender:{item.gender}
            </p>
            <p style={{fontFamily:'sans-serif'}}>
               Age:{item.age}
            </p>
            <p style={{fontFamily:'sans-serif'}}>
               Phonenumber:{item.mobile}
            </p>
            <p style={{fontFamily:'sans-serif'}}>
               Blood Group:{item.blood}
            </p>
            <p style={{fontFamily:'sans-serif'}}>
               Location:{item.location}
            </p>
            <p style={{fontFamily:'sans-serif'}}>
               Dzongkhag:{item.dzongkha}
            </p>
            </form>
            <div>
                <Button color='green'
                onClick={()=>navigate(`/update/${item.id}`)}>
                    update
                </Button>
                <Button color='purple' onClick={()=>handleModal(item)}>
                    View
                </Button>
                {open && (
                    <Modaldonor
                    open={open}
                    setOpen={setOpen}
                    handleDelete={handleDelete}
                {...content}
                    ></Modaldonor>
                )}
            </div>
            </div>
            </div>
            ))}


    
    </>
  )
}

export default Viewdonor