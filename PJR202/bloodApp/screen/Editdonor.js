import React, { Component } from 'react';

import firebase from '../src/FirebaseConnection';
import { Alert,ActivityIndicator, View, StyleSheet, TextInput, ScrollView } from 'react-native';
import CustomInput from '../src/components/CustomInput';
import Button from '../src/components/Button';




class   Editdonor extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      mobile: '',
      isLoading: true
    };
  }
 
  componentDidMount() {
    const docRef = firebase.firestore().collection('Donor').doc(this.props.route.params.userkey)
    docRef.get().then((res) => {
      if (res.exists) {
        const user = res.data();
        this.setState({
          key: res.id,
          name: user.name,
          mobile: user.mobile,
          isLoading: false
        });
      } else {
        console.log("No document found.");
      }
    });
  }

  inputEl = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  editOther() {
    this.setState({
      isLoading: true,
    });
    const docUpdate = firebase.firestore().collection('Donor').doc(this.state.key);
    docUpdate.set({
      name: this.state.name,
      mobile: this.state.mobile,
    }).then((docRef) => {
      this.setState({
        key: '',
        name: '',
        mobile: '',
        isLoading: false,
      });
      Alert.alert('Updated Successful');
     
      this.props.navigation.navigate('Other');
    })
    .catch((error) => {
      console.error(error);
      this.setState({
        isLoading: false,
      });
    });
  }

  deleteOther() {
    const docRef = firebase.firestore().collection('Donor').doc(this.props.route.params.userkey)
      docRef.delete().then((res) => {
          console.log('Doc deleted.')
          Alert.alert("Deleted Successful");
        //   this.props.navigation.navigate('Other');
      })
  }

  alertDialog=()=>{
    Alert.alert(
      'Delete',
      'Really?',
      [
        {text: 'Yes', onPress: () => this.deleteOther()},
        {text: 'No', onPress: () => console.log('Other lyric not deleted'), style: 'cancel'},
      ],
      { 
        cancelable: true 
      }
    );
  }

  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.loader}>
          <ActivityIndicator size="large" color="green"/>
        </View>
      )
    }
    return (
      <>
      <View style={styles.container}>
      <ScrollView >
        <View>
          <CustomInput
              placeholder='Title'
              value={this.state.name}
              onChangeText={(val) => this.inputEl(val, 'name')}
          />
        </View>
        <View >
          <CustomInput
              multiline={true}
              placeholder='mobile'
              numberOfLines={6}
              value={this.state.mobile}
              onChangeText={(val) => this.inputEl(val, 'mobile')}
          />
        </View>
        <View style={styles.button}>
            <Button
            mode='contained'
            onPress={() => this.editOther()} 
            color="green">
                    Update
            </Button>
          </View>
         <View>
          <Button
            mode='contained'
            onPress={this.alertDialog}
            color="red"
          >Delete</Button>
        </View>
    
      </ScrollView>
      <HomeButton/>
      </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 30
  },
  formEl: {
    flex: 1,
    padding: 0,
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#cccccc',
  },
  loader: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',    
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },
  button: {
    marginBottom: 8, 
  }
})

export default  Editdonor;

