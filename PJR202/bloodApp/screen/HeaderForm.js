import { View, Text,StyleSheet,StatusBar,Button } from 'react-native'
import React from 'react';
import { LinearGradient } from 'expo-linear-gradient';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { FontAwesome5,
  MaterialCommunityIcons,
  MaterialIcons,
  Ionicons,
  AntDesign} from '@expo/vector-icons';


const HeaderForm = ({children}) => {
  const navigation=useNavigation();
  return (
    <View style={styles.container}>
   <Text style={{textAlign:'center',
   marginTop:50,fontFamily:'serif',
   fontSize:20,color:'white',marginLeft:115}}>
     {children}
   </Text>
  
</View>
      
  )
}
export default HeaderForm;

const styles = StyleSheet.create({
    
    container:{
      height:100,
      backgroundColor:'#B00020',
      flexDirection:'row'
      
  }
})