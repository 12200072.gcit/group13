import React, {useState} from 'react';
import { StyleSheet,
     Text, 
     View, 
     ScrollView,
     TouchableOpacity, 
     ActivityIndicator,
     Image,
    TextInput,
    SafeAreaView,
    KeyboardAvoidingView,
    
     StatusBar } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import {Picker} from '@react-native-picker/picker';
import { Formik } from 'formik'
import * as yup from 'yup';
import firebase from '../src/FirebaseConnection';
import Constants from 'expo-constants'; 
import CustomInput from '../src/components/CustomInput';
import Button from '../src/components/Button';
import * as Animatable from 'react-native-animatable';
import { set } from 'react-native-reanimated';
import BackgroundImage from './component/BackgroundImage';
import Header from './component/Header';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const reviewSchema = yup.object({
  name:yup.
  string().
  required('Name is required'),

  email: yup.
  string().
  email('Please Enter Valid Email').required('Email is Required'),

  password: 
  yup.
  string().
  required('Password is Required').min(8),
  confirmPassword: 
  yup
    .string()
    .required("Please confirm your password")
    .when("password", {
      is: password => (password && password.length > 0 ? true : false),
      then: yup.string().oneOf([yup.ref("password")], "Password doesn't match")
    })
})


export default function SignupDonor({navigation}) {
  
  const [emailError, setEmailError] = useState(null)
  const [passwordError, setPasswordError] = useState(null)
  const [loading, setLoading] = useState(false);
  const [getDisabled, setDisabled] = useState(false);

  const createUser = async (values) => {
    setLoading(true)
    setDisabled(true)
    firebase.database().ref('users').push().set({
      email:values.email,
      password:values.password,
      name:values.name,

    })
    firebase.auth().createUserWithEmailAndPassword(values.email, values.password)
   
    .then((userCredential) => {
     
      // Signed in 
      var user = userCredential.user;
      // console.log('usama ---> ',user.uid)
      var obj = {uid: user.uid, ...values}
      // console.log('data',obj)
      navigation.navigate('Donate Blood',obj)  
      setLoading(false)
      setDisabled(false)    
      // ...
    })
    .catch((error) => {
      setLoading(false)
      setDisabled(false)
      var errorCode = error.code;
      var errorMessage = error.message;
      console.log(errorMessage)
      var error = errorMessage.includes("Password")
      if(error){
        setLoading(false)
        setDisabled(false)
        setPasswordError(errorMessage)        
        setEmailError(null)
        console.log(error)
      } else{
        setLoading(false)
        setDisabled(false)
        setPasswordError(null)        
        setEmailError(errorMessage)
        console.log(error)
      }
    });
  
  }

  return (
        <>
        <Header>Register</Header>
        <KeyboardAwareScrollView>
      
        <Animatable.View animation='fadeInUpBig'  style={styles.container}>
        <BackgroundImage/>
   
        
          <StatusBar style='light'/>

        {/* ------------ Icons ------------ */}
        <TouchableOpacity
          style={{marginTop: 35, marginLeft: 20}}
          onPress={()=> navigation.navigate('LoginScreen')}
        >
        </TouchableOpacity>

        {/* -------- Logo Container -------------- */}
        <View style={styles.LogoContainer}>
          <Image
            source={require('../assets/logo.png')}
            style={styles.LogoImage}
          />
        </View>

        {/* -------- Text Container --------- */}
        <View style={styles.TextContainer}>
          <Text
            style={{color: '#d60505', fontSize: 26, fontFamily:'serif', marginLeft: 128}}>
            Sign Up
          </Text>
        </View>

          {/* -------- Form Container --------- */}
        <Formik
          validationSchema = {reviewSchema}
          initialValues={{
            name:'',
            email: '',
            password: '',
            confirmPassword:'',
            donor: true,
          
          }}
          onSubmit = {(values, actions) => {
          // console.log(values)
          createUser(values)
        
          }}
        >
        {(props)=>(
          <View style={styles.FormContainer}>

           <TextInput
            style={styles.input}
              placeholder = 'Name'
              onChangeText = {props.handleChange('name')}
              value = {props.values.name}
              onBlur = {props.handleBlur('name')}
              autoCapitalize='none'
            />
            <Text style={styles.errorText}>{props.touched.name && props.errors.name}</Text> 

            <TextInput
            style={styles.input}
              placeholder = 'Email'
              onChangeText = {props.handleChange('email')}
              value = {props.values.email}
              onBlur = {props.handleBlur('email')}
              autoCapitalize='none'
            />
            {emailError ? 
              <Text style={styles.errorText}>{emailError}</Text>
              :
              null
            }
            <Text style={styles.errorText}>{props.touched.email && props.errors.email}</Text>

            <TextInput 
              style={styles.input}
              placeholder = 'Password'
              onChangeText = {props.handleChange('password')}
              value = {props.values.password}
              onBlur = {props.handleBlur('password')}
              autoCapitalize='none'
              secureTextEntry={true}
            />
            {passwordError ? 
              <Text style={styles.errorText}>{passwordError}</Text>
              :
              null
            }
            <Text style={styles.errorText}>{props.touched.password && props.errors.password}</Text>
            <TextInput
                style={styles.input}
                placeholder = 'ConfirmPassword'
                mode='outlined'
                onChangeText = {props.handleChange('confirmPassword')}
                value = {props.values.confirmPassword}
                onBlur = {props.handleBlur('confirmPassword')}
                secureTextEntry={true}
                autoCapitalize='none'
              />
              
              <Text style={styles.errorText}>{props.touched.confirmPassword && props.errors.confirmPassword}</Text>
              
              {loading ? <ActivityIndicator size={100} animating={true} color="#d60505" style={styles.loading} /> : null }

               <TouchableOpacity  
               style={{width:'98%',alignItems:'center', backgroundColor:'#841584',padding:10}}
               onPress={props.handleSubmit}
               disabled={getDisabled}>
                  <Text style={{fontFamily:'serif',fontSize:18,color:'white'}}>Submit</Text>
              </TouchableOpacity>
          
        </View>
        )}
        </Formik>
        </Animatable.View>
        </KeyboardAwareScrollView>  
        </>
);
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#fff',
    height:650
  },
  LogoContainer:{
    alignItems: 'center',
  },
  FormContainer:{
    paddingHorizontal:20,
    justifyContent: 'center',
    alignItems:'center',
    height:390,
    borderRadius:6,
    borderWidth:2,
    backgroundColor:'#ffffff',
    width:'96%',
    marginLeft:10,
    borderColor:'#ccc'



  },
  LogoImage: {
    height: 160,
    width: 160,
    borderRadius:90
   
  },
  
  errorText: {
  color: 'crimson',
},
input:{
  borderColor: '#eee',
  alignItems:'center',
  width: '97%',
  padding: 10,
  fontSize: 14,
  shadowColor:'#B81100',
  elevation:2,
  shadowOpacity:1.7
},

  loading: {
    position: 'absolute',
    zIndex: 1,
    backgroundColor: 'white',
    opacity: 0.8,
    height: '100%',
    width: '100%'
  }        
});
