import React,{useState, useEffect} from 'react';
import { StyleSheet, Text, View,TouchableOpacity, ScrollView } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { StatusBar } from 'expo-status-bar';
import Constants from 'expo-constants'; 
import firebase from '../src/FirebaseConnection';
import CustomInput from '../src/components/CustomInput';
import {Picker} from '@react-native-picker/picker';
import Button from '../src/components/Button';
import * as Animatable from 'react-native-animatable';
import Makerequest from './Makerequest';
import ImageSlider from './ImageSlider';
import Header from './Header';
import BackgroundImage from './component/BackgroundImage';




export default function Home({navigation}) {
  const [user, setUser] = useState([])


  useEffect(() => {
    // Fetching Doctor Data
    const getData = async () => {
      try {
        const value = await AsyncStorage.getItem('user')
        if(value !== null) {
          console.log('data',value)
          const userData =  firebase.database().ref('users').child(value);
          userData.on('value',data => {
          setUser(data.val())
          // console.log('Data Recieved Hogaya', user)
          }) 
        }
      } catch(e) {
        // error reading value
      }
    }

    getData()

  }, []);


  return (
    <>
   
    <View style={styles.container}>
    <BackgroundImage/>
    <Animatable.View
    animation='fadeInUpBig'>
      <ScrollView>
        <ImageSlider/>

      <View >
      <View
      style={styles.secondCont}>

      <TouchableOpacity style={styles.box}>  
        <View style={styles.inner}>
        <View style={styles.donationButtonContainer}>
                <TouchableOpacity style={styles.donationButton} onPress={()=>{
                    navigation.navigate("Viewdonor");
                }}>
                    <Text style={styles.donationButtonText}>View Donor</Text>
                </TouchableOpacity>
            </View>
        </View>
      </TouchableOpacity>

      <TouchableOpacity style={styles.box} >
        <View style={styles.inner}>
        {/* <Image style={styles.image} source={require('../assets/image/13.png')}/> */}
        <View style={styles.donationButtonContainer}>
                <TouchableOpacity style={styles.donationButton} onPress={()=>{
                    navigation.navigate("Viewrequest");
                }}>
                    <Text style={styles.donationButtonText}>Request Blood</Text>
                </TouchableOpacity>
            </View>
        </View>
      </TouchableOpacity>

      <TouchableOpacity style={styles.box} onPress={()=>{navigation.navigate('Makerequest')}}>
        <View style={styles.inner}>
        {/* <Image style={styles.Image} source={require('../assets/image/14.png')}/> */}
            <View style={styles.donationButtonContainer}>
                <TouchableOpacity style={styles.donationButton} onPress={()=>{
                    navigation.navigate("Makerequest");
                }}>
                    <Text style={styles.donationButtonText}>Request Blood</Text>
                </TouchableOpacity>
            </View>
        </View>
      </TouchableOpacity>

      <TouchableOpacity style={styles.box}>
        <View style={styles.inner}>
        <View style={styles.donationButtonContainer}>
                <TouchableOpacity style={styles.donationButton} onPress={()=>{
                    navigation.navigate("Donate Blood");
                }}>
                    <Text style={styles.donationButtonText}>Donate Blood</Text>
                </TouchableOpacity>
            </View>
      
        </View>
      </TouchableOpacity>
      </View>
    </View>


      
      
        </ScrollView>
        </Animatable.View>
      </View>
      

     
    </>   
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#eee',
  },

    container1: {
      flex:1,
    
      paddingTop: Constants.statusBarHeight,
      backgroundColor:'#eee',
   
    },
    secondCont:{
      alignItems:'center',
      justifyContent:'center',
      width:'100%',
      flexDirection:'row',
      flexWrap:'wrap',
      padding:5,
      marginTop:5,
    },
    box:{
      width:'50%',
      marginTop:5,
      height:180,
      padding:5,
      borderRadius:5,
      opacity:1.7,
      elevation:5,
      shadowColor:'blue',
      shadowOffset: {
        width: 5,
        height: 0,
      },
      shadowOpacity: 0.2,
      shadowRadius:10,
      elevation:10,


    },
    inner:{
      flex:1,
      backgroundColor:'#B81100',
      alignItems:'center',
      justifyContent:'center',
      borderRadius:6,

    },
    text:{
      fontSize:17,
      fontFamily:'serif',
      color:'white'
    },
    image:{
      width:100,
      height:100,
    },
    Image:{
      width:80,
      height:80

    },
    donationContainer:{
      flexDirection:"row",
  },
  donationButtonContainer:{
      alignItems:"center",
      width:'95%'
  },
  
  donationButton:{
     backgroundColor:"#fff",
     padding:10,
     marginBottom:10,
     width:"100%",
     alignItems : "center",
     borderRadius:5,
  },
  donationButtonText:{
      color: "#000",
      fontFamily : "serif",
      
  },
});

