import React,{useState, useEffect} from 'react';
import { StyleSheet, Text, View,TouchableOpacity, ScrollView ,
BackHandler,Alert} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { StatusBar } from 'expo-status-bar';
import Constants from 'expo-constants'; 
import firebase from '../src/FirebaseConnection';
import CustomInput from '../src/components/CustomInput';
import {Picker} from '@react-native-picker/picker';
import Button from '../src/components/Button';
import * as Animatable from 'react-native-animatable';
import Makerequest from './Makerequest';
import ImageSlider from './ImageSlider';
import {LinearGradient} from 'expo-linear-gradient';
import HeaderForm from './HeaderForm';
import BackgroundImage from './component/BackgroundImage'
import Header from './component/Header';
import { Feather} from '@expo/vector-icons';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import 'react-native-gesture-handler'
import LottieView from 'lottie-react-native';
import Imageslider from './ImageSlider';

const MainHome=({navigation})=> {
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backAction);

    return () =>
        BackHandler.removeEventListener('hardwareBackPress', backAction);
}, []);

  const backAction = () => {
    if (navigation.isFocused()) {
        Alert.alert('Hold on!', 'Are you sure you want to exit app?', [
           {
               text: 'Cancel',
                onPress: () => null,
                style: 'cancel',
            },
           { text: 'YES', onPress: () => BackHandler.exitApp() },
        ]);
        return true;
    }
};
  
   return (
    <>
    <Imageslider/>
    <BackgroundImage/>
        <StatusBar style="light" />
    <Animatable.View
    animation='fadeInUpBig'>
      <ScrollView>

      <View
      style={styles.secondCont}>

      <TouchableOpacity style={styles.box} onPress={()=>{
                    navigation.navigate("Viewdonor")}}>  
     
        <View style={styles.inner}>
        <LottieView
                    source={require('../assets/pema.json')}
                    autoPlay
                    loop={true}
                    speed={1.5}
                    style={{width:'100%',height:140}}
                  
                />
        <View style={styles.donationButtonContainer}  >
  
                <TouchableOpacity  style={styles.donationButton} >
                    <Text style={styles.donationButtonText}>View All Donor</Text>
                </TouchableOpacity>
        
            </View>   
        </View>
      </TouchableOpacity>

      <TouchableOpacity style={styles.box} onPress={()=>{
                    navigation.navigate("Viewrequest");
                }}>
    
        <View style={styles.inner}>
        
        <LottieView
                    source={require('../assets/vi.json')}
                    autoPlay
                    loop={true}
                    speed={0.5}
                    style={{width:'80%',height:140}}
                  
                /> 
         <View style={styles.donationButtonContainer}  >
                <TouchableOpacity style={styles.donationButton} onPress={()=>{
                    navigation.navigate("Viewrequest");
                }} >
                    <Text style={styles.donationButtonText}>View All Request</Text>
                </TouchableOpacity>
            </View>
        </View>
      </TouchableOpacity>

      <TouchableOpacity style={styles.box}  onPress={()=>{navigation.navigate("Login");}}>
     
      
        <View style={styles.inner}>
        <LottieView
                    source={require('../assets/re.json')}
                    autoPlay
                    loop={true}
                    speed={0.5}
                    style={{width:'80%',height:140}}
                  
                />
        <View style={styles.donationButtonContainer}>
            <TouchableOpacity style={styles.donationButton} onPress={()=>{navigation.navigate("Login");}}>
                               <Text style={styles.donationButtonText}>Request Blood</Text>
                           </TouchableOpacity>
                    </View>
        </View>
      </TouchableOpacity>

      <TouchableOpacity style={styles.box} onPress={()=>{
                    navigation.navigate("LoginDonor");
                }}>
  
        <View style={styles.inner}>
        <LottieView
                    source={require('../assets/d.json')}
                    autoPlay
                    loop={true}
                    speed={0.6}
                    style={{width:'80%',height:140}}
                  
                />
             <View style={styles.donationButtonContainer} >
                <TouchableOpacity style={styles.donationButton} onPress={()=>{navigation.navigate("LoginDonor");}}>
                    <Text style={styles.donationButtonText}>Donate Blood</Text>
                </TouchableOpacity>
            </View>
        </View>
    
      </TouchableOpacity>
  
      </View>
     
  
    
    </ScrollView>
    </Animatable.View> 
    </>   
  );
}
export default MainHome;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#B00020',
  },

    container1: {
      flex:1,
      backgroundColor:'#B00020',
   
    },
    secondCont:{
      alignItems:'center',
      justifyContent:'center',
      width:'100%',
      flexDirection:'row',
      flexWrap:'wrap',
      padding:6,
 
     
  
    },
    box:{
      width:'45%',
      marginTop:10,
      marginLeft:10,
      height:200,
      marginRight:5,
      opacity:1.7,
      elevation:20,
      borderWidth:0.3,
      borderColor:'#095aba',
      shadowColor:'blue',
      backgroundColor:'#ffffff',
      shadowOffset: {
        width: 5,
        height: 5,
      },
      shadowOpacity: 1.4,
      shadowRadius:5,
      elevation:20,
    },
    inner:{
      flex:1,
      backgroundColor:'#ffff',
      alignItems:'center',
      justifyContent:'center',
  

    },
    text:{
      fontSize:17,
      fontFamily:'serif',
      color:'white'
    },
    image:{
      width:100,
      height:100,
    },
    Image:{
      width:80,
      height:80

    },
    donationImage : {
        width:150, 
        height:150,
    },
    donationContainer:{
        flexDirection:"row",
        
    },
    donationButtonContainer:{
        alignItems:"center",
        width:'95%',
        marginTop:5
    },
    
    donationButton:{
       backgroundColor:"#fff",
       padding:10,
       marginBottom:10,
       width:"100%",
       alignItems : "center",
       borderRadius:5,
    },
    donationButtonText:{
        color: "#000",
        fontFamily : "serif",
        
    },
    ImageBackground:{
      opacity:0.5
  
    },
    actionButtonIcon: {
      fontSize:25,
      height: 20,
      color: 'white',
    },
});

