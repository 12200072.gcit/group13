

import React,{useState} from 'react';
import { StyleSheet,
     Text, 
     View, 
     ScrollView,
     StatusBar,
     Image,
     TouchableOpacity,
     ActivityIndicator } from 'react-native';
import { Formik } from 'formik'
import firebase from '../src/FirebaseConnection'
import * as yup from 'yup';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CustomInput from '../src/components/CustomInput';
import Button from '../src/components/Button';
import * as Animatable from 'react-native-animatable';
import { TextInput } from 'react-native';
import BackgroundImage from './component/BackgroundImage';
import Header from './component/Header';
import { backgroundColor } from 'react-native/Libraries/Components/View/ReactNativeStyleAttributes';




const reviewSchema = yup.object({
  email: yup.string().email('Please Enter Valid Email').required('Email is Required'),
  password: yup.string().required('Password is Required'),
})

export default function LoginScreen({navigation}) {
  const [loading, setLoading] = useState(false);
  const [getDisabled, setDisabled] = useState(false);
  const [emailError, setEmailError] = useState(null)
  const [passwordError, setPasswordError] = useState(null)

  const storeData = async (value) => {
    try {
      await AsyncStorage.setItem('user', value)
      navigation.navigate('Makerequest')
      console.log('gs')
    } catch (e) {
      // saving error
    }
  }

  const LoginUser = async (values) => {

    setDisabled(true);
    setLoading(true);
    firebase.auth().signInWithEmailAndPassword(values.email, values.password)

    .then((userCredential) => {
      setDisabled(false);
      setLoading(false);
      var user = userCredential.user;
      console.log(user.uid)
      storeData(user.uid)
   
    })
   
    .catch((error) => {
      setDisabled(false);
      setLoading(false);
      var errorCode = error.code;
      var errorMessage = error.message;
      var error = errorMessage.includes("Password")
        console.log(errorMessage)
      if(error){
        setDisabled(false);
        setLoading(false);
        setPasswordError('The Password is Invalid')        
        setEmailError(null)
        console.log(error)
      } else{
        setDisabled(false);
        setLoading(false);
        setEmailError(`${values.email} is not registered emails`)        
        setPasswordError(null)        
        console.log(error)
     
      }
    });

   
   
  }

  return (
      <>
      <Header>Login</Header>
      <BackgroundImage/>
      <Animatable.View animation='fadeInUpBig' style={{marginBottom:100}}>
          <ScrollView>
            {loading && loading ? (<ActivityIndicator size={100} color='red' style={styles.loading}/>) : null}
        

          <View style={styles.LogoContainer}>
            <Image
                source={require('../assets/logo.png')}
              style={styles.LogoImage}
            />
          </View>

 
            <Text
              style={{color: '#d60505', fontSize: 26, fontFamily:'serif', marginLeft: 125,
              position:'relative',bottom:55}}>
              Welcome
            </Text>
        
          <Formik
            validationSchema = {reviewSchema}
            initialValues={{
              email: '',
              password: '',
            }}
            onSubmit = {(values, actions) => {
            actions.resetForm()
            LoginUser(values)
            // console.log(values)
            }}
          >
          {(props)=>(
            <View style={styles.FormContainer}>
           
              <TextInput
                style={styles.input}
                onChangeText = {props.handleChange('email')}
                value = {props.values.email}          
                placeholder = 'Email'
                autoCapitalize='none'
              />
              {emailError ? 
                <Text style={styles.errorText}>{emailError}</Text>
                :
                null
              }
              <Text style={styles.errorText}>{props.touched.email && props.errors.email}</Text>
             


              <TextInput
              style={styles.input}
         
                onChangeText = {props.handleChange('password')}
                value = {props.values.password}
                placeholder = 'Password'
                secureTextEntry={true}
                autoCapitalize='none'
                
                
              />
              <View style={{marginLeft:185}}>
                    <TouchableOpacity onPress={()=>{navigation.navigate('Forgot Password')}}>
                      <Text style={{fontFamily:'serif'}}>
                          Forgot password? 
                      </Text>
                  </TouchableOpacity>
                </View>
              {passwordError ? 
                <Text style={styles.errorText}>{passwordError}</Text>
                :
                null
              }
              <Text style={styles.errorText}>{props.touched.password && props.errors.password}</Text>

              <TouchableOpacity  
               style={{width:'98%',alignItems:'center', backgroundColor:'#841584',padding:10}}
                onPress={props.handleSubmit}>
                  <Text style={{fontFamily:'serif',fontSize:18,color:'white'}}>Login</Text>
              </TouchableOpacity>

              <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center',marginTop:5}}>
                    <Text style={{fontFamily:'serif'}}>
                        Don't have an account?
                    </Text>
                    <TouchableOpacity  onPress={()=>{navigation.navigate('Register')}}>
                        <Text style={{fontSize:15,fontWeight:'bold',fontFamily:'serif',marginLeft:10}}>
                            Signup
                        </Text>
                    </TouchableOpacity>
              </View> 
            </View>
    
            
            )}
          </Formik>
          </ScrollView>
    
      </Animatable.View>  
      </>  
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFEFE',
    
  },
  LogoContainer:{
    flex:4,
    height: 200,
    width:'10%',
    marginLeft:150,
    alignItems: 'center',
    marginTop:15
  },

  FormContainer:{
    flex: 1,
    paddingHorizontal: 15,
    justifyContent: 'center',
    alignItems:'center',
    borderWidth:2,
    width:'96%',
    height:300,
    marginLeft:8,
    borderColor:'#ccc',
    backgroundColor:'#ffffff',
    borderRadius:6,
    position:'relative',
    bottom:60


  },
  LogoImage: {
    height: 120,
    width: 120,
    borderRadius:90,
    marginLeft:40
  },
  errorText: {
    color: 'crimson',
  },
  input:{
    borderColor: '#eee',
    alignItems:'center',
    width: '97%',
    marginTop:5,
    padding: 10,
    fontSize: 14,
    shadowColor:'#B81100',
    elevation:1,
    shadowOpacity:1.7,
    borderRadius:2
  },
  loading: {
    position: 'absolute',
    zIndex: 1,
    backgroundColor: 'white',
    opacity: 0.8,
    height: '100%',
    width: '100%'
  },      
});