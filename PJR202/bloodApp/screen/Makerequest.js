import React, { Component,useRef } from 'react';
import {StyleSheet, ScrollView, ActivityIndicator, View, TextInput,Alert,Timestamp,
  TouchableOpacity,
  Text,
  Keyboard} from 'react-native';
import firebase from '../src/FirebaseConnection';
import CustomInput from '../src/components/CustomInput'
import Button from '../src/components/Button';
import {Picker} from '@react-native-picker/picker'
import RadioForm from 'react-native-simple-radio-button';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import PhoneInput from "react-native-phone-number-input";
import BackgroundImage from './component/BackgroundImage';
import Headerform from './component/Headerform';
import LottieView from 'lottie-react-native';

const options = [
  {value: 'Male', label: 'Male'},
  { value: 'Famale', label: 'Female'},
  { value: 'Other',label: 'Other' },
]; 
const phoneInput = useRef<PhoneInput>(null);
class Makerequest extends Component {

  constructor() {
    super();
    this.ref = firebase.firestore().collection('Request');
    const timestamp = firebase.firestore.FieldValue.serverTimestamp();
    this.state = {
      name: '',
      gender:'',
      age:'',
      mobile: '',
      blood:'',
      require:'',
      location:'',
      dzongkha:'',
      detail:'',
      createdAt:timestamp,
      isLoading: false
    };
  }


  onValUpdate = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);

   
  }


  addRequest() {
    if(this.state.name ===''){
      Alert.alert(" Please enter your name."
      )
    }else if(!isNaN(this.state.name)){
      Alert.alert("Invalid input value")
    }
    else if(this.state.gender===''){
      Alert.alert("Select your gender."
      )
    }else if(this.state.age===''){
      Alert.alert('Please enter your age')
      
    }else if(isNaN(this.state.age)){
      Alert.alert("Invalid input value")

    }
    else if(this.state.age===''  || (this.state.age<=18 || this.state.age>65)){
      Alert.alert('Sorry! you are not eligible', 'Age shoud be between 18 to 65',
      [{text:"Ok"}])
      

    }else if(this.state.mobile===''){
      Alert.alert("Please enter your phonenumber."
      )
    }else if(isNaN(this.state.mobile)){
      Alert.alert('Invalid phonenumber.')
    }else if(this.state.mobile ==8){
      Alert.alert("Invalid phonenumber.")
    }
    else if(this.state.blood===''){
      Alert.alert("Please select your bloodgroup."
      )
    }else if(this.state.require===''){
      Alert.alert('Please enter required unit')
      
    }else if(this.state.location===''){
      Alert.alert("Please select your location"
      )
    }
    else if(this.state.dzongkha===''){
      Alert.alert("Please select your dzongkhag"
      )
    }
    else if(this.state.detail===''){
      Alert.alert("Please enter detail"
      )
    }
     else {
      this.setState({
        isLoading: true,
      });      
      this.ref.add({
        name:this.state.name,
        gender:this.state.gender,
        age:this.state.age,
        mobile:this.state.mobile,
        blood:this.state.blood,
        require:this.state.require,
        location:this.state.location,
        dzongkha:this.state.dzongkha,
        detail:this.state.detail,
        createdAt:this.state.createdAt,

      }).then((res) => {
        this.setState({
          name: '',
          gender:'',
          mobile: '',
          age:'',
          blood:'',
          require:'',
          location:'',
          dzongkha:'',
          detail:'',
          createdAt:'',
          isLoading: false,
        });
        // this.props.navigation.goBack();
        Alert.alert("Successfull submitted.")
      
      })
      .catch((err) => {
        console.error("Error occured: ", err);
        this.setState({
          isLoading: false,
        });
      });
    }
  }
  
  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.loading}>
          <LottieView
                    source={require('../assets/loading.json')}
                    autoPlay
                    loop={true}
                    speed={0.3}
                    style={{width:'60%',height:100}}
                
                />
        </View>
      )
    }
   
    return (
      <>
        <Headerform></Headerform>
        <BackgroundImage/>
     
       
        <View style={styles.formcontainer}> 
        <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
    
          <TextInput
              style={styles.input}
              placeholder='Name'
              value={this.state.name}
              onChangeText={(val) => this.onValUpdate(val, 'name')}
          />
  

        <RadioForm
          size="small"
          style={{marginLeft:20,margin:5,marginTop:15}}
          radio_props={options}
          initial={0}
          selectedValue={this.state.gender}
          formHorizontal={true}
          onPress={(val)=>this.onValUpdate(val,'gender')}/>


          <TextInput
              style={styles.input}
              keyboardType="number-pad"
              maxLength={2}
              placeholder='Age'
              value={this.state.age}
              onChangeText={(val) => this.onValUpdate(val, 'age')}
              
          />
    
        <View style={styles.pickerView}>
        <PhoneInput
            style={{width:'100%',backgroundColor:'#ffffff'}}
             ref={phoneInput}
            defaultValue={this.state.mobile}
            defaultCode="BT"
            maxLength={8}
            onChangeText={(val) => {
              this.onValUpdate(val,'mobile');
            }}
         
          />

        </View>
        
        <View style = {styles.pickerView}>
          <Picker 
            dropdownIconColor= '#d60505'
            selectedValue={this.state.blood}
            onValueChange={(val)=>this.onValUpdate(val,'blood')}
          >
            <Picker.Item label="Choose Your Blood Group" value="" />
            <Picker.Item  label="O+" value="O+" />
            <Picker.Item  label="O-" value="O-" />
            <Picker.Item  label="A+" value="A+" />
            <Picker.Item  label="A-" value="A-" />
            <Picker.Item  label="B+" value="B+" />
            <Picker.Item  label="B-" value="B-" />
            <Picker.Item  label="AB+" value="AB+" />
            <Picker.Item  label="AB-" value="AB-" />
          </Picker>
        </View>
        <TextInput
              style={[styles.input,{marginTop:15}]}
              placeholder='Required Unit'
              keyboardType='number'
              maxLength={2} 
              value={this.state.require}

              onChangeText={(val) => this.onValUpdate(val, 'require')}
          />
      
        

          <TextInput
              style={[styles.input,{marginTop:15}]}
              placeholder='Location'
              value={this.state.location}
              onChangeText={(val) => this.onValUpdate(val, 'location')}
          />
      

        <View style = {styles.pickerView}>
          <Picker 
            dropdownIconColor= '#d60505'
            selectedValue={this.state.dzongkha}
            onValueChange={(val)=>this.onValUpdate(val,'dzongkha')}
          >
            <Picker.Item label="Choose Your Dzongkhag" value="" />
            <Picker.Item  label="Thimphu" value="Thimphu" />
            <Picker.Item  label="Paro" value="Paro" />
            <Picker.Item  label="Haa" value="Haa" />
            <Picker.Item  label="Gasa" value="Gasa" />
            <Picker.Item  label="Punakha" value="Punakha" />
            <Picker.Item  label="Dagana" value="Danaga" />
            <Picker.Item  label="Tsirang" value="Tsirang" />
            <Picker.Item  label="Sarpang" value="Sarpang" />
            <Picker.Item  label="Samdrup Jongkhar" value="Samdrup Jongkhar" />
            <Picker.Item  label="Chukha" value="chukha" />
            <Picker.Item  label="Pemagatshel" value="Pemagatshel" />
            <Picker.Item  label="Tashigang" value="Tashigang" />
            <Picker.Item  label="Tashiyangtse" value="Tashiyangtshe" />
            <Picker.Item  label="Zhemgang" value="Zhamgang" />
            <Picker.Item  label="Mongar" value="Mongar" />
            <Picker.Item  label="Bumthang" value="Bumthang" />
            <Picker.Item  label="Lhuntse" value="Lhuntse" />
            <Picker.Item  label="Trongsa" value="Trongsa" />
            <Picker.Item  label="Wangduephodrang" value="Wangduephodrang" />
          </Picker>
        </View>

        <CustomInput
          multiline={true}
          label='More detail...'
          mode='outlined'
          numberOfLines={3}

          value={this.state.detail}
          onChangeText={(val) => this.onValUpdate(val, 'detail')}


        />     
        <TouchableOpacity  
               style={{width:'98%',alignItems:'center', backgroundColor:'#841584',padding:10,marginTop:15}}
               onPress={() => this.addRequest()} >
                  <Text style={{fontFamily:'serif',fontSize:18,color:'white'}}>Submit</Text>
              </TouchableOpacity>
          </ScrollView>    
        </View>   

    
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  loading: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',    
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },
  pickerView: {
    fontSize: 14,
    borderColor: '#eee',
    width: '97%',
    backgroundColor:'#ffffff',
    borderWidth:1,
    marginTop:15,
    shadowColor:'#B81100',
    elevation:5,
    shadowOpacity:1.7,
    height:50
  
},
input:{
  borderColor: '#eee',
  alignItems:'center',
  width: '97%',
  padding: 10,
  fontSize: 14,
  shadowColor:'#B81100',
  elevation:2,
  shadowOpacity:1.7,
  marginTop:10,
},
formcontainer:{
  flex: 1,
  paddingHorizontal: 15,
  justifyContent: 'center',
  alignItems:'center',
  marginTop:25,
  borderWidth:2,
  width:'95%',
  height:500,
  marginLeft:10,
  borderColor:'#ccc',
  backgroundColor:'#ffffff',
  borderRadius:6,
  marginBottom:15,

}
})
export default Makerequest;