import { View, Text } from 'react-native'
import React from 'react'
import BackgroundImage from './component/BackgroundImage'


export default function About() {
  return (
    <View style={{flex:1}}>
      <BackgroundImage/>
      <View style={{backgroundColor:'#eeee',height:450,width:'90%',
      marginLeft:20,
      padding:35,
      marginTop:35,
      elevation:15,
      shadowColor:'blue',
      shadowOffset: {
        width: 5,
        height:5,
      },
      shadowOpacity: 10,
      shadowRadius:10,
      elevation:10,
      borderBottomLeftRadius:60,
      borderTopRightRadius:60,
      }}>

        <Text style={{fontSize:15,fontFamily:'serif'}}>
        Our App connect blood donors with recipients,
        without any intermediary such as blood banks,
        for an efficient and seamless process.
        </Text>
        <Text
         style={{fontSize:15,fontFamily:'serif',marginTop:20}}>      
         <Text   style={{fontSize:15,fontFamily:'serif'}}>Features:{'\n'}</Text>
        -Keep records of donor{'\n'}
        -Keep recipient records{'\n'}
        -Convenient appointment scheduling between donors and recipient{'\n'}
        -Find blood donors{'\n'}
        -Request blood{'\n'}
        - Share donor’s information{'\n'}
        </Text>
      </View>
    </View>
  )
}