import * as React from 'react';
import { Text, View, StyleSheet,Image } from 'react-native';
import { SliderBox } from "react-native-image-slider-box"; 
import { LinearGradient } from 'expo-linear-gradient';

export default class Imageslider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      images: [ 
        'https://bloodsafety.gov.bt/wp-content/uploads/2022/01/slide1.jpg',
        'https://scontent-sin6-4.xx.fbcdn.net/v/t31.18172-8/411046_417488801653936_701855765_o.jpg?_nc_cat=101&ccb=1-7&_nc_sid=924451&_nc_ohc=PLPfaacTsywAX8okABe&_nc_ht=scontent-sin6-4.xx&oh=00_AT8BD0pYmIGC76NDd0HsZ3sCpRqJP8nIGxc9LvFVNAm5sw&oe=62CAC15D',
        'https://s3sdghub.s3.eu-west-1.amazonaws.com/core-cms/public/styles/media_image_large/public/images/posts/_107320074_blooddonor976.jpg?itok=Th5aTK49',
         'https://stanfordbloodcenter.org/wp-content/uploads/2020/06/Blood-facts_10-illustration-graphics__canteen.png',
         'https://assets.irinnews.org/s3fs-public/styles/social-large/public/images/200802082.jpg?IBaimv5LGT5CMeV543mu2cacQ67PeYfD&itok=1AwrR4SE',
         'https://media.defense.gov/2019/Nov/25/2002217579/-1/-1/0/191206-F-PO640-013.JPG',
         'https://www.dailybhutan.com/media/1053/blood-500x330.jpg',
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTi2cCq9Yo_WitDW-pOVejieSWEbEvq0hkgB6aTJJwds-Z6niOEiZaFAvOZiQb7FLW4ig&usqp=CAU',
           
      ]
    };
  }
  render() {
    return (
      <LinearGradient colors={['#B00020','pink']}  style={styles.container}>
         <View>
        <SliderBox 
          style={{width:'95%',height:155,}}
          autoplay={true}
          loop={true}
          images={this.state.images}
       
        />
        </View>

      </LinearGradient>
     
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'flex-start',
    backgroundColor: 'blue',
    padding:10,
  }
});
