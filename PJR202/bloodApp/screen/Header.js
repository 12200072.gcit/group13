import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity,StatusBar,BackHandler,Alert} from 'react-native';


export default function Header(props) {
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backAction);

    return () =>
        BackHandler.removeEventListener('hardwareBackPress', backAction);
}, []);

  const backAction = () => {
    if (navigation.isFocused()) {
        Alert.alert('Hold on!', 'Are you sure you want to exit app?', [
           {
               text: 'Cancel',
                onPress: () => null,
                style: 'cancel',
            },
           { text: 'YES', onPress: () => BackHandler.exitApp() },
        ]);
        return true;
    }
};
  
    const {image, Logout} = props

  return (
    <View style={styles.container}>
      <StatusBar style='light'/>
       
       
          <TouchableOpacity
              onPress={backAction}
            >
            <View style={{backgroundColor:'white',height:25,width:'20%',marginLeft:265,marginTop:50,borderRadius:7}}>
                <Text style={{fontFamily:'serif',fontSize:16, color: 'black'}}>
                    Logout
                </Text>
            </View>  
             
        </TouchableOpacity>

        </View>
    
  );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
      backgroundColor: '#B00020',
      height: 85,
     justifyContent: 'space-between',
     alignItems: 'center',
     paddingHorizontal: 15,
      shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    },
    Image: {
        resizeMode: 'contain',
        width: 40,
        height: 40,
        borderRadius: 400,
        marginVertical: 40,
     },
     LogoImage: {
        resizeMode: 'contain',
        // height: 20,
        width: 120,
      },
  });