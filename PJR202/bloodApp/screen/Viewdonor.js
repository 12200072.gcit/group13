import React, { Component } from 'react';
import firebase from '../src/FirebaseConnection';
import { StyleSheet, ScrollView, ActivityIndicator, View,Text} from 'react-native';
import { ListItem } from 'react-native-elements'
import { useNavigation } from '@react-navigation/native';
import BackgroundImage from './component/BackgroundImage';
import Header from './component/Header';
import Homebutton from './component/Homebutton'
import LottieView from 'lottie-react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { AntDesign,FontAwesome,Entypo } from '@expo/vector-icons';






class Viewdonor extends Component {
  
  constructor() {
    super();
    this.docs = firebase.firestore().collection('Donor');
    this.state = {
      isLoading: true,
      Viewdonor: []
    };
  }

  componentDidMount() {
    this.unsubscribe = this.docs.onSnapshot(this.fetchCollection);
  }

  componentWillUnmount(){
    this.unsubscribe();
  }

  fetchCollection = (querySnapshot) => {
    const Viewdonor = [];
    querySnapshot.forEach((res) => {
      const { name,blood,createdAt,gender,location} = res.data();
      Viewdonor.push({
        key:res.id,
        name,
        blood,
        gender,
        location,
        createdAt,
      });
    });
    this.setState({
      Viewdonor,
      isLoading: false
   });
  }



  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.loader}>
           <LottieView
                    source={require('../assets/loading.json')}
                    autoPlay
                    loop={true}
                    speed={0.3}
                    style={{width:'60%',height:100}}
                
                />
        </View>
      )
    }    
    return (
      <>
      <Header>View Donor</Header>
      <BackgroundImage/>
      <ScrollView style={styles.wrapper}>
          {
            this.state.Viewdonor.map((res, i) => {
              return (

                <TouchableOpacity
                    style={{marginTop:20,width:'90%',marginLeft:15,}}
                    key={i} 
                      onPress={() => {
                          this.props.navigation.navigate('Moredetail',{
                            userkey:res.key
                          });
                        }}                   
                      bottomDivider>
                      <View style={{flexDirection:'row',height:95,backgroundColor:'#ffff'}}>
                       <View style={{backgroundColor:'red',height:95,marginBottom:100,
                       alignItems:'center',justifyContent:'center',
                      width:'20%'}}>
                         <Text style={{fontSize:20,color:'white'}}>{res.blood}</Text>
                       </View>
                       <View style={{flexDirection:'column'}}>
                          <View>
                            <Text>
                            <AntDesign name="user" size={20} color="blue" />
                              <Text style={{fontWeight:'bold',marginTop:10}}>
                                {res.name}
                              </Text>
                            </Text>
                              <View style={{marginTop:5}}>
                                <Text>
                                <Entypo name="location" size={20} color="green" /> 
                                <Text style={{fontWeight:'bold'}}>{res.location}</Text>

                                </Text>
                                 
                              </View>
                        
                          </View>
                          <View style={{flexDirection:'row',marginTop:10}}>
                          <View>
                              <Text style={{fontSize:12}}>Posted:{new Date(res.createdAt.seconds * 1000).toLocaleDateString("en-US")}</Text>
                          </View>
                          <View style={{marginLeft:50}}>
                              <Text style={{fontSize:12}}>More detail...</Text>
                          </View>   
                          </View>     
                       </View>  
                     </View>

                </TouchableOpacity>
               
              );
            })
          }
      </ScrollView>
      <Homebutton/>
      </>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
   flex: 1,
   paddingBottom: 20,

  },
  loader: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',    
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  }
})

export default Viewdonor;

// import React, { useState, useEffect  } from 'react';
// import { StyleSheet, View, ScrollView, Picker, Text, TouchableOpacity,ImageBackground} from 'react-native';
// import { Table, Row, Rows, Cell, TableWrapper } from 'react-native-table-component';
// import SkeletonPlaceholder from "react-native-skeleton-placeholder";
// import call from 'react-native-phone-call';
// import firebase from '../src/FirebaseConnection';
// import {LinearGradient} from 'expo-linear-gradient'

// const About=()=>{
//   const [data , setData] = useState({
//   tableHead: ['Donor','Dzongkhag','Location','BloodGroup','PhoneNo:'],
//   allDonors: [],
//   tableData: [],
//   widthArr: [100, 100, 100, 100, 100],
//   });
//   let [loading, setLoading] = useState(true);
//   const [selectedValue, setSelectedValue] = useState("All");
//   const [selectedCityValue, setSelectedCityValue] = useState("All");

//   // useEffect(()=>{
//   //     const newData = [];
//   //     const allDonorsFromFirebase = [];

//   //     var donors = firebase.firestore().collection('Donor');
//   //     get('value', (snapshot) => {
//   //       const donorsData = snapshot.val();
//   //       const keys = Object.keys(donorsData);
//   //       keys.forEach((key)=>{
//   //         newData.push(donorsData[key]);
//   //       });
//   //       newData.forEach((data)=>{
//   //         const {donorName, dzongkhag,gender,location, bloodGroup, mobileNo} = data;
//   //         allDonorsFromFirebase.push([donorName,dzongkhag,location,bloodGroup, mobileNo]);
//   //       });
//   //           setData({...data,  tableData:allDonorsFromFirebase, allDonors:allDonorsFromFirebase});
//   //           setLoading(false);
//   //     });
//   // },[]);
//   useEffect(() => {
//       const newData = [];
//       const allDonorsFromFirebase = [];
    
//       var donors=firebase.firestore().collection('Donor').orderBy("dzongkha", "desc").get('value', (snapshot)  => {
//           const donorsData = snapshot.val();
//           const keys = Object.keys(donorsData);
//           keys.forEach((key)=>{
//             newData.push(donorsData[key]);
//           });
//           newData.forEach((data)=>{
//             const {donorName, dzongkhag,gender,location, bloodGroup, mobileNo} = data;
//             allDonorsFromFirebase.push([donorName,dzongkhag,location,bloodGroup, mobileNo]);
//           });
//               setData({...data,  tableData:allDonorsFromFirebase, allDonors:allDonorsFromFirebase});
//               setLoading(false);
//         });
//   }, []);

//   // function to filter results by blood group
//   const filterByGroup = (itemValue)=>{
//   if(selectedCityValue=="All"){
//     if(itemValue=="All"){
//       data.tableData =  data.allDonors;
//     }
//     else{
//       data.tableData =  data.allDonors.filter((d)=>{
//         return d[2]==itemValue;
//       });
//     }
//   }
//   else{
//     if(itemValue=="All"){
//       data.tableData =  data.allDonors.filter((d)=>{
//         return (d[1]==selectedCityValue);
//       });
//     }
//     else{
//       data.tableData =  data.allDonors.filter((d)=>{
//         return (d[2]==itemValue && d[1]==selectedCityValue);
//       });
//     }
//   }
// setSelectedValue(itemValue)
// }

//   const filterByCity = (itemValue)=>{
//     if(itemValue=="All"){
//       data.tableData = data.allDonors;
//     }
//     else{
//       data.tableData =  data.allDonors.filter((d)=>{
//         return d[1]==itemValue;
//       });
//     }
//       setSelectedCityValue(itemValue);
//   }



//   const element = (data, index) => (
//       <TouchableOpacity style={{justifyContent:"center", alignItems:"center"}} onPress={() => {
//         const args = {
//           number: data, // String value with the number to call
//         }
//         call(args).catch(console.error);
//       }}>
//         <View style={styles.callBtn}>
//           <Text style={styles.callBtnText}>{data}</Text>
//         </View>
//       </TouchableOpacity>
//     );

    
   
//   return (


 
//   loading?
  
//   <SkeletonPlaceholder backgroundColor="#e6e6e6">
//     <View style={{ margin:20 }}>
//     <View style={{ width: "10%", height: 40, borderRadius: 4, marginTop:10 }} />
//     <View style={{ width: "10%", height: 40, borderRadius: 4, marginTop:10 }} />
//     <View style={{ width: "10%", height: 40, borderRadius: 4, marginTop:10 }} />
//     <View style={{ width: "10%", height: 40, borderRadius: 4, marginTop:10 }} />
//     <View style={{ width: "10%", height: 40, borderRadius: 4, marginTop:10 }} />
//     <View style={{ width: "10%", height: 40, borderRadius: 4, marginTop:10 }} />
//     <View style={{ width: "10%", height: 40, borderRadius: 4, marginTop:10 }} />
//     <View style={{ width: "10%", height: 40, borderRadius: 4, marginTop:10 }} />
//     <View style={{ width: "10%", height: 40, borderRadius: 4, marginTop:10 }} />
//     <View style={{ width: "10%", height: 40, borderRadius: 4, marginTop:10 }} />
//     </View>
// </SkeletonPlaceholder>:

// <LinearGradient colors={['#ebdf75','orange']}>
// <View>
// <ImageBackground source={require('../assets/images/pe.webp')}
// style={{position:'absolute',flex:1,width:'100%',height:650}} imageStyle={styles.ImageBackground}/>
//       <View style={{alignItems:'center',marginTop:10}}>
//             <Text style={{fontSize:18,fontFamily:'serif'}}>Search Donor and call </Text>
//       </View>
//       <View style={styles.filters}>
//           <View style={styles.inputConatiner}>
//       <Text style={styles.labelText}> Dzongkhag</Text>
//       <View style={styles.selectBox}>
//       <Picker
//           selectedValue={selectedCityValue}
//           style={styles.picker}
//           onValueChange={
//             (itemValue, itemIndex) => {
//               filterByCity(itemValue);
//           }}
//           >
//           <Picker.Item label="All" value="All" />
//           <Picker.Item  label="Thimphu" value="Thimphu" />
//           <Picker.Item  label="Paro" value="Paro" />
//           <Picker.Item  label="Haa" value="Haa" />
//           <Picker.Item  label="Gasa" value="Gasa" />
//           <Picker.Item  label="Punakha" value="Punakha" />
//           <Picker.Item  label="Dagana" value="Danaga" />
//           <Picker.Item  label="Tsirang" value="Tsirang" />
//           <Picker.Item  label="Sarpang" value="Sarpang" />
//           <Picker.Item  label="Samdrup Jongkhar" value="Samdrup Jongkhar" />
//           <Picker.Item  label="Chukha" value="chukha" />
//           <Picker.Item  label="Pemagatshel" value="Pemagatshel" />
//           <Picker.Item  label="Tashigang" value="Tashigang" />
//           <Picker.Item  label="Tashiyangtse" value="Tashiyangtshe" />
//           <Picker.Item  label="Zhemgang" value="Zhamgang" />
//           <Picker.Item  label="Mongar" value="Mongar" />
//           <Picker.Item  label="Bumthang" value="Bumthang" />
//           <Picker.Item  label="Lhuntse" value="Lhuntse" />
//           <Picker.Item  label="Trongsa" value="Trongsa" />
//           <Picker.Item  label="Wangduephodrang" value="Wangduephodrang" />

//       </Picker>
//       </View>
//       </View>
//       <View style={styles.inputConatiner}>
//       <Text style={styles.labelText}> Blood Group  </Text>
//       <View style={styles.selectBox}>
//       <Picker
//           selectedValue={selectedValue}
//           style={styles.picker}
//           onValueChange={(itemValue, itemIndex) => {
//             filterByGroup(itemValue);    
//           }}
//           >
//           <Picker.Item label="All" value="All" />
//           <Picker.Item label="O+" value="O+" />
//           <Picker.Item label="O-" value="O-" />
//           <Picker.Item label="A+" value="A+" />
//           <Picker.Item label="A-" value="A-" />
//           <Picker.Item label="B+" value="B+" />
//           <Picker.Item label="B-" value="B-" />
//           <Picker.Item label="AB+" value="AB+" />
//           <Picker.Item label="AB-" value="AB-" />
//       </Picker>
//       </View>
//       </View>
//       </View>
//     <ScrollView  horizontal={true}>
//     <View style={styles.container}>
//     <Table borderStyle={{borderWidth: 1, borderColor: '#a6a6a6',}}>
 
//         <Row data={data.tableHead} widthArr={data.widthArr} style={styles.head} textStyle={styles.headText}/>
//         {
          
//           data.tableData.map((rowData, index) => (
            
//             <TableWrapper key={index} style={styles.row} >
//               {
            
//                 rowData.map((cellData, cellIndex) => (
              
//                   <Cell  key={cellIndex} data={cellIndex === 4 ? element(cellData, index) : cellData} textStyle={styles.text} style={styles.cell}  />
                  
//                 ))
                
        
//               }     
//             </TableWrapper>
//           ))
          
//         }
        
//       </Table>
//     </View>
//   </ScrollView>
// </View>
// </LinearGradient>
    
//   )
// }

// const styles = StyleSheet.create({
// cell:{
//   width:100
// },
// container: { 
//   flex: 1, 
//   padding: 5, 
//   backgroundColor: '#eee' 
// },
// head: { 
//     backgroundColor: '#B81100',
   
// },
// text: { 
//     margin: 6 
// },
// headText:{
// margin:6,
// color: 'white',
// fontFamily:'serif'
// },
// inputConatiner:{
//   width:"44%",
//   // backgroundColor:"red",
//   padding:1,
// },
// row: { 
//   flexDirection: 'row', 
//   backgroundColor: '#fff' 
// },
// callBtn: { 
   
//     padding:5, 
//     alignItems : "center",
//     justifyContent : "center",
//     backgroundColor: 'green',  
//     borderRadius: 2 ,
// },
// callBtnText: { 
//     textAlign: 'center', 
//     color: 'white',
   
// },
// picker:{  
//   width: "100%", 
//   marginLeft:10, 
//   height:30,
//   padding:5,
//   color:"#666666", 
// },
// filters:{
//   backgroundColor: '#eee',
//   width: "100%", 
//   margin:2,
//   justifyContent : "space-around",
//   alignItems : "center",
//   flexDirection:"row",
//   marginTop:30,
// },
// labelText:{
//   padding:1,
//   color:"red",
//   fontFamily:'serif',
//   fontSize:18
// },

// selectBox:{
//   width:"100%",
//   backgroundColor:"white", 
//   borderRadius:6,
//   alignItems:"center",
//   marginLeft:"auto",
//   marginRight:"auto",
//   marginTop:5,
// },
// ImageBackground:{
// opacity:0.69
// }
// });
// export default About;


