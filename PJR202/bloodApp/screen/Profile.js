
import React,{useState, useEffect} from 'react';
import { StyleSheet, Text, View,TouchableOpacity, ScrollView ,Image,Linking} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { StatusBar } from 'expo-status-bar';
import Constants from 'expo-constants'; 
import firebase from '../src/FirebaseConnection';
import CustomInput from '../src/components/CustomInput';
import {Picker} from '@react-native-picker/picker';
import Button from '../src/components/Button';
import * as Animatable from 'react-native-animatable';
import Makerequest from './Makerequest';
import ImageSlider from './ImageSlider';
import Header from './Header';
import { Feather } from '@expo/vector-icons';
import BackgroundImage from './component/BackgroundImage';




export default function Profile({navigation}) {

  const makeCall = () => {

    let phoneNumber = '';

    if (Platform.OS === 'android') {
        phoneNumber = 'tel:${+975 17 50 81 06}';
    } else {
        phoneNumber = 'telprompt:${+975 17 50 81 06}';
    }

    Linking.openURL(phoneNumber);
};

const Tandin = () => {

  let phoneNumber = '';

  if (Platform.OS === 'android') {
      phoneNumber = 'tel:${+975 17 71 55 66}';
  } else {
      phoneNumber = 'telprompt:${+975 17 71 55 66}';
  }

  Linking.openURL(phoneNumber);
};

const Wangmo = () => {

  let phoneNumber = '';

  if (Platform.OS === 'android') {
      phoneNumber = 'tel:${+97517356558}';
  } else {
      phoneNumber = 'telprompt:${+97517356558}';
  }

  Linking.openURL(phoneNumber);
};


const Norbu = () => {

  let phoneNumber = '';

  if (Platform.OS === 'android') {
      phoneNumber = 'tel:${+97577761949}';
  } else {
      phoneNumber = 'telprompt:${+97577761949}';
  }

  Linking.openURL(phoneNumber);
};


 
  return (
    <>
   
    <View style={styles.container}>
    <BackgroundImage/>
    <Animatable.View
    animation='fadeInUpBig'>
      <ScrollView>
       

      <View>
      <View>
          <Text
          style={{fontSize:20,fontFamily:'serif',
          width:'100%',
          marginLeft:8,marginTop:35}}>
             If need some any help, contact the{'\n'}following members!
          </Text>
        </View>
      <View style={styles.secondCont}>
      
      <View style={styles.box}>  
      
        <Image style={styles.Image} source={require('../assets/images/phu.jpeg')}/>
        <Text style={{fontSize:13,fontFamily:'serif'}}>Phuntsho Wangmo</Text>
        <TouchableOpacity>
          <View style={{backgroundColor:'#cccc',
            width:'50%',height:40,
            justifyContent:'center',alignItems:'center',
            borderRadius:60,
            marginLeft:40}}>
                <Feather name="phone-call" size={25} color="green"
                  onPress={makeCall}/>
            </View>
        </TouchableOpacity>
       
   
               
                
       
      </View>

      <View style={styles.box} >
   
        <Image style={styles.Image} source={require('../assets/images/tan.jpeg')}/>
        <Text style={{fontSize:13,fontFamily:'serif'}}>Tandin Wangchuck</Text>
        <TouchableOpacity>
        <View style={{backgroundColor:'#cccc',
          width:'50%',height:40,
          justifyContent:'center',alignItems:'center',
          borderRadius:60,
          marginLeft:40}}>
              <Feather name="phone-call" size={25} color="green"
                onPress={Tandin}/>
            </View>
        </TouchableOpacity>
      
               
         

      </View>

      <View style={styles.box}>
  
            <Image style={styles.Image} source={require('../assets/images/pema.jpeg')}/>
            <Text style={{fontSize:13,fontFamily:'serif'}}>Pema Wangmo</Text>

          <TouchableOpacity>
              <View style={{backgroundColor:'#cccc',
              width:'50%',height:40,
              justifyContent:'center',alignItems:'center',
              borderRadius:60,
              marginLeft:40}}>
                  <Feather name="phone-call" size={25} color="green"
                    onPress={Wangmo}/>
              </View>
          </TouchableOpacity>
         
            
        
         </View>
      <View style={styles.box}>

  
        <Image style={styles.Image} source={require('../assets/images/norbu.jpeg')}/>
        <Text style={{fontSize:13,fontFamily:'serif'}}>Pema Norbu</Text>
        <TouchableOpacity>
          <View style={{backgroundColor:'#cccc',
            width:'50%',height:40,
            justifyContent:'center',alignItems:'center',
            borderRadius:60,
            marginLeft:40}}>
                <Feather name="phone-call" size={25} color="green"
                  onPress={Norbu}/>
            </View>
        </TouchableOpacity>
        
      </View>
      </View>
    </View>


      
      
        </ScrollView>
        </Animatable.View>
      </View>
      

     
    </>   
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#eee',
  },

    container1: {
      flex:1,
      paddingTop: Constants.statusBarHeight,
      backgroundColor:'#eee',
    },
    secondCont:{
      alignItems:'center',
      justifyContent:'center',
      width:'100%',
      flexDirection:'row',
      flexWrap:'wrap',
      padding:5,
      marginTop:30,
    },
    box:{
      width:'45%',
      marginTop:10,
      marginLeft:5,
      marginRight:5,
      height:200,
      padding:5,
      borderRadius:5,
      opacity:1.7,
      elevation:3,
      shadowColor:'blue',
      backgroundColor:'#ffffff',
      shadowOffset: {
        width: 5,
        height: 0,
      },
      shadowOpacity: 0.2,
      shadowRadius:10,
      elevation:3,


    },
    inner:{
      flex:1,
      backgroundColor:'#ffffff',
      alignItems:'center',
      justifyContent:'center',
      borderRadius:6,

    },
    text:{
      fontSize:17,
      fontFamily:'serif',
      color:'white'
    },
    image:{
      width:100,
      height:100,
    },
    Image:{
      width:'100%',
      height:130,
    

    },
    donationContainer:{
      flexDirection:"row",
  },
  donationButtonContainer:{
      alignItems:"center",
      width:'95%'
  },
  
  donationButton:{
     backgroundColor:"#fff",
     padding:10,
     marginBottom:10,
     width:"100%",
     alignItems : "center",
     borderRadius:5,
  },
  donationButtonText:{
      color: "#000",
      fontFamily : "serif",
      
  },
});

