import React, { Component } from 'react';
import { View,Image,Text} from 'react-native';
import LottieView from 'lottie-react-native'



export default class Splash extends Component {
    constructor(props) {
        super();
    }

    render() {
        return (
            <>
            <View style={{flex:1}}>
        
            <View
                style={{
                    flex: 1
                   }}>
                <LottieView
                    source={require('../assets/blood.json')}
                    autoPlay
                    loop={false}
                    speed={0.3}
                    style={{height:400,marginTop:100}}
                    onAnimationFinish={() => {
                        this.props.navigation.navigate('Main')
                        console.log('Animation Finished!')
                        
                    }}
                />
                 
                 
                <Image source={require('../assets/logo.png')}
                style={{width:'40%',height:150,borderRadius:90,marginLeft:115,position:'absolute',top:285}}/>
                <Text style={{textAlign:'center',fontSize:26,fontFamily:'serif'}}>WELCOME</Text>

             
            </View>
            </View>
            </>
        )
    }
}



