import React, { Component } from 'react';
import firebase from '../src/FirebaseConnection';
import { StyleSheet, ScrollView, ActivityIndicator, View,Text} from 'react-native';
import { ListItem } from 'react-native-elements'
import { useNavigation } from '@react-navigation/native';
import BackgroundImage from './component/BackgroundImage';
import Header from './component/Header';
import Homebutton from './component/Homebutton'
import LottieView from 'lottie-react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { AntDesign,FontAwesome,Entypo } from '@expo/vector-icons';






class Viewrequest extends Component {
  
  constructor() {
    super();
    this.docs = firebase.firestore().collection('Request');
    this.state = {
      isLoading: true,
      Viewrequest: []
    };
  }

  componentDidMount() {
    this.unsubscribe = this.docs.onSnapshot(this.fetchCollection);
  }

  componentWillUnmount(){
    this.unsubscribe();
  }

  fetchCollection = (querySnapshot) => {
    const Viewrequest = [];
    querySnapshot.forEach((res) => {
      const { name,blood,createdAt,gender,location} = res.data();
      Viewrequest.push({
        key:res.id,
        name,
        blood,
        gender,
        location,
        createdAt,
      });
    });
    this.setState({
      Viewrequest,
      isLoading: false
   });
  }



  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.loader}>
           <LottieView
                    source={require('../assets/loading.json')}
                    autoPlay
                    loop={true}
                    speed={0.3}
                    style={{width:'60%',height:100}}
                
                />
        </View>
      )
    }    
    return (
      <>
      <Header>View Request</Header>
      <BackgroundImage/>
      <ScrollView style={styles.wrapper}>
          {
            this.state.Viewrequest.map((res, i) => {
              return (

                <TouchableOpacity
                    style={{marginTop:20,width:'90%',marginLeft:15,}}
                    key={i} 
                      onPress={() => {
                          this.props.navigation.navigate('Requestedit',{
                            userkey:res.key
                          });
                        }}                   
                      bottomDivider>
                      <View style={{flexDirection:'row',height:95,backgroundColor:'#ffff'}}>
                       <View style={{backgroundColor:'red',height:95,marginBottom:100,
                       alignItems:'center',justifyContent:'center',
                      width:'20%'}}>
                         <Text style={{fontSize:20,color:'white'}}>{res.blood}</Text>
                       </View>
                       <View style={{flexDirection:'column'}}>
                          <View>
                            <Text>
                            <AntDesign name="user" size={20} color="blue" />
                              <Text style={{fontWeight:'bold',marginTop:10}}>
                                {res.name}
                              </Text>
                            </Text>
                              <View style={{marginTop:5}}>
                                <Text>
                                <Entypo name="location" size={20} color="green" /> 
                                <Text style={{fontWeight:'bold'}}>{res.location}</Text>

                                </Text>
                                 
                              </View>
                        
                          </View>
                          <View style={{flexDirection:'row',marginTop:10}}>
                          <View>
                              <Text style={{fontSize:12}}>Posted:{new Date(res.createdAt.seconds * 1000).toLocaleDateString("en-US")}</Text>
                          </View>
                          <View style={{marginLeft:50}}>
                              <Text style={{fontSize:12}}>More detail...</Text>
                          </View>   
                          </View>     
                       </View>  
                     </View>

                </TouchableOpacity>
               
              );
            })
          }
      </ScrollView>
      <Homebutton/>
      </>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
   flex: 1,
   paddingBottom: 20,

  },
  loader: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',    
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  }
})

export default Viewrequest;

