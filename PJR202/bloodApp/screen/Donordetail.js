import React, { Component } from 'react';
import firebase from '../src/FirebaseConnection';
import { Share,Alert,ActivityIndicator, View, StyleSheet, 
  TextInput, 
  ScrollView,
  TouchableOpacity,
Text} from 'react-native';
import Sharebutton from './component/Sharebutton';
import { Feather,FontAwesome} from '@expo/vector-icons';
import {useNavigation} from '@react-navigation/native';
import LottieView from 'lottie-react-native';


class Donordetail extends Component {
  // navigation=useNavigation();
  constructor() {
    super();
    this.state = {
      name: '',
      gender:'',
      age:'',
      mobile:'',
      blood:'',
      location:'',
      dzongkha:'',

      isLoading: true
    };
  }
  onValUpdate = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);   
  }
 
  componentDidMount() {
    const docRef = firebase.firestore().collection('Donor').doc(this.props.route.params.userkey)
    docRef.get().then((res) => {
      if (res.exists) {
        const user = res.data();
        this.setState({
          key: res.id,
          name: user.name,
          gender: user.gender,
          age: user.age,
          mobile:user.mobile,
          blood:user.blood,
          location:user.location,
          dzongkha:user.dzongkha,
          isLoading: false
        });
      } else {
        console.log("No document found.");
      }
    });
  }
  inputEl = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }
  shareData = async () => {
		try {
			await Share.share({
        message:`Donor\nName:${this.state.name}\nGender:${this.state.gender},\nAge:${this.state.age},\nPhonenumber:${this.state.mobile},\nBloodgroup:${this.state.blood},\nLocation:${this.state.gender},\nDzongkhag:${this.state.dzongkha}`
			});
		} catch (error) {
			alert(error.message);
		}
	};

 

  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.loader}>
            <LottieView
                    source={require('../assets/loading.json')}
                    autoPlay
                    loop={true}
                    speed={0.3}
                    style={{width:'60%',height:100}}
                
                />
        </View>
      )
    }
    return (
      <>
      <View style={styles.container}>
    
       <Text style={styles.text}>Name:{this.state.name}</Text>
       <Text  style={styles.text}>Gender:{this.state.gender}</Text>
       <Text  style={styles.text}>Age:{this.state.age}</Text>
       <Text  style={styles.text}>Phonenumber:{this.state.mobile}</Text>
       <Text  style={styles.text}>Bloodgroup:{this.state.blood}</Text>
       <Text  style={styles.text}>Location:{this.state.location}</Text>
       <Text  style={styles.text}>Dzongkhag:{this.state.dzongkha}</Text>

       <ScrollView>
    
        <View style={{flexDirection:'row',backgroundColor:'#bd640d'}}>
   
   
          <TouchableOpacity onPress={this.shareData}>
        <View  style={{borderColor:'white',borderWidth:1,
        width:100,
        height:50,
        alignItems:'center',
        justifyContent:'center'}}>
          <Feather name="phone-call" size={24} color="white" />  
        </View>
        </TouchableOpacity>


        <TouchableOpacity onPress={this.shareData}>
        <View  style={{borderColor:'white',borderWidth:1,
        width:100,
        height:50,
        alignItems:'center',
        justifyContent:'center'}}>
          <Sharebutton/>    
        </View>
        </TouchableOpacity>
       
      </View>
      </ScrollView> 
     
 
      </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 25,
    marginTop:100,
    backgroundColor:'#ccc',   

  },
  formEl: {
    flex: 1,
    padding: 0,
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#cccccc',
  },
  loader: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',    
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },
  button: {
    marginBottom: 8, 
  },
  text:{
    fontSize:15,
    fontFamily:'serif',
    marginTop:5

  }
})

export default Donordetail;