import { View, Text,StyleSheet,StatusBar,Button } from 'react-native'
import React from 'react';
import { LinearGradient } from 'expo-linear-gradient';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { FontAwesome5,
  MaterialCommunityIcons,
  MaterialIcons,
  FontAwesome,
  Ionicons,
  AntDesign} from '@expo/vector-icons';


const Header = ({children}) => {
  const navigation=useNavigation();
  return (
    <LinearGradient colors={[, '#3b5998', '#192f6a']}>
    <View style={styles.container}>


    <TouchableOpacity onPress={() => navigation.goBack()}>
      <View style={{marginTop:50,marginLeft:20}}>
      <FontAwesome name="arrow-circle-left" size={30} color="white" />
      </View>
   
   </TouchableOpacity>
   <Text style={{textAlign:'center',
   marginTop:50,fontFamily:'serif',
   fontSize:20,color:'white',marginLeft:65}}>
     {children}
   </Text>
 
  
</View>
</LinearGradient> 
      
  )
}
export default Header;

const styles = StyleSheet.create({
    
    container:{
      height:100,
      backgroundColor:'#B00020',
      flexDirection:'row',
      zIndex:1,
      
  }
})