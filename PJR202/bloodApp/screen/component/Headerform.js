import { View, Text,StyleSheet,StatusBar,Button,BackHandler,Alert } from 'react-native'
import React,{useEffect}from 'react';
import { LinearGradient } from 'expo-linear-gradient';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { FontAwesome5,
  MaterialCommunityIcons,
  MaterialIcons,
  Ionicons,
  AntDesign} from '@expo/vector-icons';


const Headerform = ({children}) => {
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backAction);

    return () =>
        BackHandler.removeEventListener('hardwareBackPress', backAction);
}, []);

  const backAction = () => {
    if (navigation.isFocused()) {
        Alert.alert('Want to logout!', 'Are you sure?', [
           {
               text: 'Cancel',
                onPress: () => null,
                style: 'cancel',
            },
           { text: 'YES', onPress: () => navigation.replace('Main') },
        ]);
        return true;
    }
};
  const navigation=useNavigation();
  return (
    <View style={styles.container}>
    <TouchableOpacity onPress={backAction}>
       <Text style={{marginTop:50,fontSize:18,marginLeft:5,
        fontFamily:'serif',
        color:'white'}}>Logout</Text>
   </TouchableOpacity>
   <Text style={{textAlign:'center',
   marginTop:50,fontFamily:'serif',
   fontSize:20,color:'white',marginLeft:65}}>
     {children}
   </Text>
  
</View>
      
  )
}
export default Headerform;

const styles = StyleSheet.create({
    
    container:{
      height:100,
      backgroundColor:'#B00020',
      flexDirection:'row',
      zIndex:1,
      
  }
})