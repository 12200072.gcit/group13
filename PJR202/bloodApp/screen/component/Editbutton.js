import { View, Text,TouchableOpacity} from 'react-native'
import React from 'react'
import { useNavigation } from '@react-navigation/native'
import { FontAwesome5,Ionicons,Entypo } from '@expo/vector-icons';

export default function Editbutton() {
    const navigation=useNavigation();
  return (
      <TouchableOpacity onPress={()=>{navigation.navigate('Requestedit')}} 
      style={{position:'absolute',
      zIndex:1,
      top:650,
      left:290,
      backgroundColor:'#008cb8',
      width:50,
      height:50,
      alignItems:'center',
      justifyContent:'center',
    borderRadius:60}}>
          <Entypo name="pencil" size={24} color="black" />
      </TouchableOpacity>
 
  )
}