import React, { useState } from "react";
import { Pressable } from "react-native";
import { MaterialCommunityIcons ,Octicons} from "@expo/vector-icons";


export default function Sharebutton  (){
    const [liked,setLiked] = useState(false);


  return (
     <>
        <Pressable onPress={() => setLiked((isLiked) => !isLiked)}>
        <Octicons name={ liked ?"share-android":"share-android" } size={30} color={liked ? "green":"white"} />
    </Pressable>
     </> 
   
  );
};
