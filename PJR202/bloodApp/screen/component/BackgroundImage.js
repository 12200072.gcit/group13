import { View, Text,ImageBackground} from 'react-native'
import React from 'react'
import { LinearGradient } from 'expo-linear-gradient'

export default function BackgroundImage() {
  return (
    <View>
      <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']}>
      <ImageBackground
          source={require('../../assets/back.png')}
          resizeMode='stretch'
          style={{width:'100%',height:648,position:'absolute'}}/>

      </LinearGradient>
        
    </View>
  )
}