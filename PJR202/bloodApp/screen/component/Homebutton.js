import { View, Text,TouchableOpacity} from 'react-native'
import React from 'react'
import { useNavigation } from '@react-navigation/native'
import { FontAwesome5,Ionicons } from '@expo/vector-icons';
import LottieView from 'lottie-react-native';

export default function Homebutton({}) {
  const navigation=useNavigation();

  return (
      <TouchableOpacity onPress={()=>{navigation.navigate('Main')}} 
      style={{position:'absolute',
      zIndex:1,
      top:650,
      left:290,
      backgroundColor:'#008cb8',
      width:50,
      height:50,
      alignItems:'center',
      justifyContent:'center',
    borderRadius:60}}>
          <LottieView
              
              style={{width:'50%',height:50}}
              source={require('../../assets/Home.json')}
              autoPlay
              loop={true}
              speed={0.5}
             
        
   />
      </TouchableOpacity>
 
  )
}