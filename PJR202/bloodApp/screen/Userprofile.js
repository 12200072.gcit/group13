import React, {useState} from 'react';
import { StyleSheet, Text, View, Switch, TouchableOpacity } from 'react-native';
import firebase from '../src/FirebaseConnection';

export default function Userprofile(props) {
    const {username,cid,bloodGroup} = props

        firebase.database().ref('Request').update({
        })
     


  return (
      <>

    <View style={styles.container}>
        <View style={{flexDirection: 'row', flex:1, justifyContent: 'space-between'}}>
            <Text style={styles.text}>
                Name
            </Text>
            <Text style={styles.text}>
                {username}
            </Text>
        </View>

    </View>

    <View style={styles.container}>
        <View style={{flexDirection: 'row', flex:1, justifyContent: 'space-between'}}>
            <Text style={styles.text}>
                Blood Group
            </Text>
            <Text style={styles.text}>
                {bloodGroup}
            </Text>
        </View>
    </View>

    <View style={styles.container}>
        <View style={{flexDirection: 'row', flex:1, justifyContent: 'space-between'}}>
            <Text style={styles.text}>
                cid
            </Text>
            <Text style={styles.text}>
                {cid}
            </Text>
        </View>
        
    </View>
    </>
  );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
      backgroundColor: '#df1f26',
      height: 35,
     justifyContent: 'space-between',
     alignItems: 'center',
     paddingHorizontal: 15,
    },
    text: {
        fontWeight: 'bold',
        color: '#fff',
        fontSize: 16,   
    },
    donor: {
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#fff',
        width: '100%',
        height: 50,
        marginTop: 35,
        shadowColor: "#000",
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'space-between',
        borderRadius: 10,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    }
  });