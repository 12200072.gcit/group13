import { View, Text,StyleSheet,Image,TextInput,StatusBar, TouchableOpacity,Dimensions } from 'react-native'
import React from 'react';
import * as Animatable from 'react-native-animatable';
// import LinearGradient from 'react-native-linear-gradient';
import {LinearGradient} from 'expo-linear-gradient';

import Icon from 'react-native-vector-icons/FontAwesome5';



const StartScreen=({navigation})=>{
   
    
  return (
    <LinearGradient   colors={['#f00306','#9c7a7a']} style={styles.mainView}>
           <StatusBar backgroundColor='#850101' barStyle="light-content"/>
        <View style={styles.TopView}>
            <Animatable.Image
               animation="bounceIn"
                duraton="2500"
                style={styles.logo}
                resizeMode="stretch"
                source={require('../assets/logo.png')}
           />

        </View>
        <Animatable.View  
        animation='fadeInUpBig'
        duraton='2500'
        style={styles.BottomView}>
            <Text style={styles.title}>Stay connected with everyone and Donate Blood!</Text>
            <Text style={styles.text}>Sign in with account</Text>


            <View 
            style={styles.ButtonView1}>
                <TouchableOpacity onPress={()=>{navigation.navigate()}}>
                <LinearGradient
                    colors={['#960c0c','#850101']}
                    style={styles.signIn}>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <Text style={styles.textSign}>Get Started</Text>
                        <Icon style={{marginLeft:7}} name='angle-double-right' size={30} color='#fff'/>

                    </View>
                    </LinearGradient>
                        
                </TouchableOpacity>
            </View>
           

        </Animatable.View>
     
    </LinearGradient>
  )
}
export default StartScreen;
const {height} = Dimensions.get("screen");
const height_logo = height * 0.28;
const styles=StyleSheet.create({
    mainView:{
        flex:1,
       
    },
    TopView:{
   
        flex:2,
        justifyContent:'center',   
        alignItems:'center',
       
    },
    BottomView:{
        backgroundColor:'#FFFCFC',
        borderTopLeftRadius:30,
        borderTopRightRadius:30,
        height:'40%',
        borderTopColor:'#3653f1',
        borderTopWidth:1,
        

        
    },
    Image:{
        width:'50%',
        height:'50%',
        borderRadius:12,
    },
    Text:{
        fontSize:30,
        marginLeft:20,
        marginTop:30,
        fontWeight:'bold'
    },
    title: {
        color: '#05375a',
        fontSize: 30,
        marginLeft:20,
        fontFamily:'serif'
    },
    text: {
        color: 'grey',
        marginTop:5,
        fontSize:15,
        marginLeft:20,
        fontFamily:'serif'
    },
    textSign: {
        color: 'white',
        fontWeight: 'bold',
        fontFamily:'serif'
    
    },
    logo:{
            width: height_logo,
            height: height_logo,
            borderRadius:80,

    },
    signIn: {
        width: 150,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius:6,
        flexDirection: 'row',
        marginLeft:'55%',
    },
   
})


