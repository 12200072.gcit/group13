import { StyleSheet,Button,Text, View,Image, Platform, Alert,ActivityIndicator,TouchableOpacity} from 'react-native';
import React, {useEffect, useState} from 'react';
import firebase from './src/FirebaseConnection'
import * as ImagePicker from 'expo-image-picker';
import * as DocumentPicker from 'expo-document-picker';



const ImageV = () => {
  
    const [image, setImage] = useState(null);
    const [uploading,setUploading] = useState(false);

    useEffect(()=>{
        (async()=>{
            if(Platform.OS !== 'web'){
                const {status} = await ImagePicker.requestMediaLibraryPermissionsAsync();
                if(status !== 'granted'){
                    alert('sorry, we need camera roll permission to make this work!');
                }
            }
        })();
        },[]);
    
    
    const pickImage = async () => {
      let result = await DocumentPicker.getDocumentAsync({})
      console.log(result.uri);
      console.log(result);
  
      console.log(result);
  
      if (!result.cancelled) {
        setImage(result.uri);
      }
    };

    const PostImage = async () =>{
        const blob = await new Promise((resolve, reject)=> {
            const xhr = new XMLHttpRequest();
            xhr.onload = function(){
                resolve(xhr.response);
            };
            xhr.onerror = function(){
                reject(new TypeError('Network request failed'));
            };
            xhr.responseType = 'blob';
            xhr.open('GET', image, true);
            xhr.send(null);
        });
        const refer=firebase.database().ref('Request').push();
        const ref = firebase.storage().ref().child(new Date().toISOString())
        const snapshot = ref.put(blob)
      

        snapshot.on(firebase.storage.TaskEvent.STATE_CHANGED,()=>{
            setUploading(true)
        },
        (error)=>{
            setUploading(false);
            console.log(error);
            blob.close();
        },
        ()=>{
            snapshot.snapshot.ref.getDownloadURL().then((url)=>{
                setUploading(false)
                console.log("download  url : ", null);
                blob.close();
                
            });
        }
        );
    };


  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <View style={{borderWidth: 2}}>
            <Image source={{ uri: image }} style={{ width: 335, height: 200 }} />
        </View>

        <TouchableOpacity onPress={pickImage} style={styles.button}>
            <Text style={styles.text}>Choose picture</Text>
        </TouchableOpacity>

        {!uploading?<TouchableOpacity onPress={PostImage} style={styles.button}>
           <Text style={styles.text}>Upload picture</Text>
        </TouchableOpacity> :<ActivityIndicator size='large' color="#000" /> }
       
    </View>

  )
}

export default ImageV;

const styles = StyleSheet.create({
    button: {
        elevation: 8,
        backgroundColor: "#009688",
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 12
    },
    text:{
        fontSize: 18,
        color: "#fff",
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "uppercase"
    }
})