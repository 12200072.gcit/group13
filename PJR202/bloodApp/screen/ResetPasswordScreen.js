import React, { useState } from 'react'

import Header from '../scr1/components/Header'
import Button from '../scr1/components/Button'
import { emailValidator } from '../scr1/helpers/emailValidator'
import { sendEmailWithPassword } from '../scr1/api/auth-api'
import Toast from '../scr1/components/Toast'
import { StatusBar,View,TextInput,StyleSheet,Image,TouchableOpacity,Text} from 'react-native'
import * as Animatable from 'react-native-animatable';
import BackgroundImage from './component/BackgroundImage'
import Head from './component/Header'
export default function ResetPasswordScreen({ navigation }) {
  const [email, setEmail] = useState({ value: '', error: '' })
  const [loading, setLoading] = useState(false)
  const [toast, setToast] = useState({ value: '', type: '' })

  const sendResetPasswordEmail = async () => {
    const emailError = emailValidator(email.value)
    if (emailError) {
      setEmail({ ...email, error: emailError })
      return
    }
    setLoading(true)
    const response = await sendEmailWithPassword(email.value)
    if (response.error) {
      setToast({ type: 'error', message: response.error })
    } else {
      setToast({
        type: 'success',
        message: 'Email with password has been sent.',
      })
    }
    setLoading(false)
  }

  return (
    <>
    <Head>ResetPassword</Head>
     <BackgroundImage/>
    <Animatable.View style={{flex:1,alignItems:'center',justifyContent:'center'}}
    animation='fadeInUpBig'>
     
        <Image
            source={require('../assets/logo.png')}
            style={styles.LogoImage}
          />

      <Header>Restore Password</Header>
      <StatusBar style='light'/>
      
      <View style={{width:'90%',marginLeft:0,backgroundColor:'#ffffff',borderColor:'#ccc',
    borderWidth:2,height:150,borderRadius:8}}>
      <TextInput
        style={styles.input}
        placeholder="E-mail address"
        returnKeyType="done"
        value={email.value}
        onChangeText={(text) => setEmail({ value: text, error: '' })}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
        description="You will receive email with password reset link."
      />
  
      <TouchableOpacity  
               style={{width:'90%',alignItems:'center', backgroundColor:'#841584',padding:10,
              marginLeft:15,marginTop:20}}
               onPress={sendResetPasswordEmail}
               loading={loading}>
                  <Text style={{fontFamily:'serif',fontSize:18,color:'white'}}> Send Instructions</Text>
              </TouchableOpacity>

      </View>
    
      <Toast {...toast} onDismiss={() => setToast({ value: '', type: '' })} />
    </Animatable.View>
    </>
  )
}

const styles=StyleSheet.create({
  input:{
    borderColor: '#eee',
    alignItems:'center',
    width: '90%',
    padding: 14,
    fontSize: 16,
    shadowColor:'#B81100',
    elevation:2,
    shadowOpacity:1.7,
    marginLeft:15,
    marginTop:10
  },
  LogoImage: {
    height: 160,
    width: 160,
    borderRadius:90
   
  },
})
