import React, { Component } from 'react';
import firebase from '../src/FirebaseConnection';
import { StyleSheet, ScrollView, ActivityIndicator, View,Text,Image,Linking} from 'react-native';
import { ListItem } from 'react-native-elements'
import { useNavigation } from '@react-navigation/native';
import BackgroundImage from './component/BackgroundImage';
import Header from './component/Header';
import Homebutton from './component/Homebutton'
import LottieView from 'lottie-react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { AntDesign,FontAwesome,Entypo,Feather } from '@expo/vector-icons';







class Contact extends Component {
  
  constructor() {
    super();
    this.docs = firebase.firestore().collection('Donor');
    this.state = {
      isLoading: true,
      Contact: []
    };
  }

  componentDidMount() {
    this.unsubscribe = this.docs.onSnapshot(this.fetchCollection);
  }

  componentWillUnmount(){
    this.unsubscribe();
  }

  fetchCollection = (querySnapshot) => {
    const Contact = [];
    querySnapshot.forEach((res) => {
      const { name,blood,createdAt,gender,location,dzongkha,mobile} = res.data();
      Contact.push({
        key:res.id,
        name,
        blood,
        gender,
        location,
        createdAt,
        dzongkha,
        mobile,
      });
    });
    this.setState({
      Contact,
      isLoading: false
   });
  }

	




  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.loader}>
           <LottieView
                    source={require('../assets/loading.json')}
                    autoPlay
                    loop={true}
                    speed={0.3}
                    style={{width:'60%',height:100}}
                
                />
        </View>
      )
    }    
    return (
      <>
      <BackgroundImage/>
      <ScrollView style={styles.wrapper}>
          {
            this.state.Contact.map((res, i) => {

              
              return (

                <TouchableOpacity
                    style={{marginTop:20,width:'90%',marginLeft:15,}}
                    key={i} 
                    //   onPress={() => {
                    //       this.props.navigation.navigate('Moredetail',{
                    //         userkey:res.key
                    //       });
                    //     }}                   
                      bottomDivider>
                      <View style={{flexDirection:'row',height:95,backgroundColor:'#ffff'}}>
                       <View style={{backgroundColor:'red',height:95,marginBottom:100,
                       alignItems:'center',justifyContent:'center',
                      width:'20%'}}>
                         <Text style={{fontSize:20,color:'white'}}>{res.blood}</Text>
                       </View>
                       <View style={{flexDirection:'column'}}>
                        <View style={{flexDirection:'row',}}>
                            <View>
                                <Text>
                                <AntDesign name="user" size={20} color="blue" />
                                <Text style={{fontWeight:'bold',marginTop:10}}>
                                    {res.name}
                                </Text>
                                </Text>
                                <View style={{marginTop:5}}>
                                    <Text>
                                    <Entypo name="location" size={20} color="green" /> 
                                    <Text style={{fontWeight:'bold'}}>{res.location}</Text>

                                    </Text>
                                </View>
                                <View style={{marginTop:5}}>
                                    <Text>
                                    <Image source={require('../assets/dzo.png')}
                                    style={{width:20,height:20}}/>
                                    <Text style={{fontWeight:'bold'}}>{res.dzongkha}</Text>
                                    </Text>
                                </View>
                                </View>
                         
                              <View style={{width:'25%',
                               height:95,marginLeft:120,alignItems:'center',
                               justifyContent:'center'}}>
                                <TouchableOpacity onPress={()=>{
                                    
                                        let phoneNumber ='';
                                        if (Platform.OS === 'android') {
                                          phoneNumber = `tel:${res.mobile}`;
                                        }
                                        else {
                                          phoneNumber = `telprompt:${res.mobile}`;
                                        }
                                        Linking.openURL(phoneNumber)
                                        }}>
                                <LottieView  
                                    style={{width:'50%',height:50}}
                                    source={require('../assets/call.json')}
                                    autoPlay
                                    loop={true}
                                    speed={0.6}
                                    />
                                </TouchableOpacity>
                              </View>
                          </View>   
                       </View>  
                     </View>

                </TouchableOpacity>
              
              );
            })
          }
      </ScrollView>
      <Homebutton/>
      </>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
   flex: 1,
   paddingBottom: 20,

  },
  loader: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',    
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  }
})

export default Contact;

