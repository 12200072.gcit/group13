import React, { Component } from 'react';
import firebase from '../src/FirebaseConnection';
import { Share,Alert,ActivityIndicator, View, StyleSheet, 
  TextInput, 
  ScrollView,
  Linking,
  TouchableOpacity,
  Image,
Text} from 'react-native';
import Sharebutton from './component/Sharebutton';
import { Feather,FontAwesome,Octicons,AntDesign,Entypo} from '@expo/vector-icons';
import BackgroundImage from './component/BackgroundImage';
import Header from './component/Header';
import Homebutton from './component/Homebutton';
import LottieView from 'lottie-react-native';
import CustomInput from '../src/components/CustomInput';



class Moredetail extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      gender:'',
      age:'',
      mobile:'',
      blood:'',
      location:'',
      createdAt:'',
      dzongkha:'',

      isLoading: true
    };
  }
  onValUpdate = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);   
  }
 
  componentDidMount() {
    const docRef = firebase.firestore().collection('Donor').doc(this.props.route.params.userkey)
    docRef.get().then((res) => {
      if (res.exists) {
        const user = res.data();
        this.setState({
          key: res.id,
          name: user.name,
          gender: user.gender,
          age: user.age,
          mobile:user.mobile,
          createdAt:user. createdAt,
          blood:user.blood,
          location:user.location,
          dzongkha:user.dzongkha,
          isLoading: false
        });
      } else {
        console.log("No document found.");
      }
    });
  }


dialCall = () => {

  let phoneNumber = '';

  if (Platform.OS === 'android') {
    phoneNumber = `tel:${this.state.mobile}`;
  }
  else {
    phoneNumber = `telprompt:${this.state.mobile}`;
  }

  Linking.openURL(phoneNumber);
};

  inputEl = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }
  shareData = async () => {
		try {
			await Share.share({
        message:`Donor\nName:${this.state.name}\nGender:${this.state.gender},\nAge:${this.state.age},\nPhonenumber:${this.state.mobile},\nBloodgroup:${this.state.blood},\nLocation:${this.state.gender},\nDzongkhag:${this.state.dzongkha},\nPosted:${this.state.createdAt}`
			});
		} catch (error) {
			alert(error.message);
		}
	};

 

  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.loader}>
            <LottieView
                    source={require('../assets/loading.json')}
                    autoPlay
                    loop={true}
                    speed={0.3}
                    style={{width:'60%',height:100}}
                
                />
        </View>
      )
    }
    return (
      <>
      <Header>{this.state.name}</Header>
      <BackgroundImage/>
      <View style={styles.container}>
      <View style={{
       padding:35,height:460,
       width:'90%',
       backgroundColor:'#eeee',
       marginLeft:15,
       elevation:15,
       borderRadius:20,
       shadowRadius:5,
       shadowColor:'blue',
       shadowOffset: {
         width: 5,
         height:5,
       },
       shadowOpacity: 10,
       shadowRadius:10,
       elevation:10,
       marginTop:35,
       }}>
      <ScrollView showsVerticalScrollIndicator={false} style={{height:500,width:'100%'}}>
        <View
        style={{justifyContent:'flex-start'}}>
       
          <Text  style={styles.text}>
          <AntDesign name="user" size={20} color="blue" />
          <Text style={styles.text}>{this.state.name}</Text>

          </Text>
          <Text  style={styles.text}>
          <FontAwesome name="transgender" size={20} color="black" />
          <Text style={styles.text}>{this.state.gender}</Text>
          </Text>
         
          <Text  style={styles.text}>Age:{this.state.age}</Text>
          <Text  style={styles.text}>
          <Feather name="phone-call" size={20} color="black" />
          <Text style={styles.text}>{this.state.mobile}</Text>
          </Text>
          <Text  style={styles.text}>
          <Image source={require('../assets/bloodic.png')}
          style={{width:25,height:25}}/>
            <Text style={styles.text}>{this.state.blood}</Text>
          </Text> 
        
          <Text  style={styles.text}>
          <Entypo name="location" size={20} color="green" /> 
          <Text  style={styles.text}>{this.state.location}</Text>
          </Text>
          <Text  style={styles.text}>
          <Image source={require('../assets/dzo.png')}
          style={{width:20,height:20}}/>
            <Text  style={styles.text}>{this.state.dzongkha}</Text>

          </Text>
        
          <Text  style={styles.text}>Posted:{new Date(this.state.createdAt.seconds * 1000).toLocaleDateString("en-US")}</Text>
          
        </View> 
     
        
      </ScrollView> 
      <View style={{flexDirection:'row',borderTopWidth:1,}}>
      <TouchableOpacity onPress={this.dialCall}>
        <View  style={{borderColor:'white',
        width:50,
        height:50,
        backgroundColor:'green',
        borderRadius:30,
        alignItems:'center',
        justifyContent:'center'}}>
            <LottieView  
              style={{width:'50%',height:50}}
              source={require('../assets/call.json')}
              autoPlay
              loop={true}
              speed={0.6}
            />
        </View>
        </TouchableOpacity>
        
        <TouchableOpacity onPress={this.shareData}>
        <View  style={{borderColor:'white',borderWidth:1,
          width:50,
          height:50,
          borderRadius:30,
          marginLeft:'65%',
          backgroundColor:'#5aa9fa',
          alignItems:'center',
          justifyContent:'center'}}>
            <LottieView  
              style={{width:'70%',height:100}}
              source={require('../assets/share.json')}
              autoPlay
              loop={true}
              speed={0.6}
            />
        </View>
        </TouchableOpacity>

      </View>
      </View>
      </View>
      <Homebutton/>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 0,
    marginTop:10,
   

  },
  formEl: {
    flex: 1,
    padding: 0,
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#cccccc',
  },
  loader: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',    
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },
  button: {
    marginBottom: 8, 
  },
  text:{
    fontSize:18,
    fontFamily:'serif',
    marginTop:10

  }
})

export default Moredetail;

