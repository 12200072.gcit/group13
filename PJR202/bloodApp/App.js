import React from 'react';
import { StyleSheet, Text, View,StatusBar } from 'react-native';
import Mystack from './navigation/Navigationstack';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import Splash from './screen/Splash';

export default function App() {
  return (
    <NavigationContainer>
      <StatusBar barStyle='#B00020' style='light'/>
         <Mystack/>
    </NavigationContainer>
   
  
 
  );
}



