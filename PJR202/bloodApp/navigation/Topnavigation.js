import * as React from 'react';
import { Text, View } from 'react-native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Viewdonor from '../screen/Viewdonor';
import Contact from '../screen/Contact';



const Tab = createMaterialTopTabNavigator();
export default function Mytop() {
  return (
      <Tab.Navigator
      screenOptions={{
      }}>
        <Tab.Screen name="Viewdonor" component={Viewdonor}
         options={{ title: 'View Donor' }}/>
        <Tab.Screen name="Contact" component={Contact}
         options={{ title: 'Contact' }}/>
      </Tab.Navigator>
  );
}