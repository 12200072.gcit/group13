import React , {useContext} from 'react';
import {View , Text, StyleSheet , Image} from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
// import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { DrawerContentScrollView , DrawerItem, DrawerItemList} from "@react-navigation/drawer";

 function DrawerContent(props) {
  
    return (
        <DrawerContentScrollView {...props}>
            <View style={styles.container}>
                <Image
                 source={require("../assets/icon.png")}
                 style={styles.image}
                 />
                <Text style={styles.text}>Sanan Ali</Text>
            </View>
            <DrawerItemList {...props} />
            <DrawerItem
            //    icon={({ color, size }) => (
            //     <MaterialIcons
            //       name="logout"
            //       color={color}
            //       size={size}
            //     />
            //   )}
              label="Logout"
              inactiveTintColor="black"
              labelStyle={{
                fontFamily:"Helvetica-Bold-Font",
              }}
              style={{
                  width:"100%",
                  marginLeft:0,
                  borderRadius:0,
                  padding:1,
                  
                  
              }}
             
            />    
              
        
        
      
        </DrawerContentScrollView>
        
    )
}
const styles = StyleSheet.create({
    container : {
        height :120,
        marginTop:-5,
        padding:10,
        backgroundColor:"#fe6e58",
        justifyContent:"center",
        alignItems: "center",
    },
    image: {
        width:90, 
        height:90,
        borderRadius:50
    },
    text: {
        color:"white",
        fontSize:18,
        fontFamily:"Helvetica-Bold-Font",
    }
})
export default DrawerContent;