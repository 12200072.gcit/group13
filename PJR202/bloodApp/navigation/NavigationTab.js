import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { View,Image,Text} from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';
import { tintColor } from 'react-native/Libraries/Components/View/ReactNativeStyleAttributes';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Dimensions } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import Viewdonor from '../screen/Viewdonor';
import Viewrequest from '../screen/Viewrequest';
import Home from '../screen/Home';
import Feedback from '../screen/Feedback';
import About from '../screen/About';
import Profile from '../screen/Profile';

const Tab = createBottomTabNavigator();
  
 export default function Mytab    () {

    return ( 
      <Tab.Navigator
         screenOptions={{
            tabBarLabelStyle: { fontSize: 12},
            tabBarItemStyle: { width:100,height:60},
            tabBarShowLabel:false,
            }}
      >
          <Tab.Screen  name="Home" component={Home}
            options={{
              headerShown:false,

            tabBarIcon:({focused}) =>(
            <View style={{alignItems:'center'}} >
            <FontAwesome5 name='home' size={20}
            color={focused? 'red':'#55a3c2'}/>
            <View style={{width:'200%',alignItems:'center'}}>
               <Text style={{color:focused? 'red':'black'}}>Home</Text>
            </View>
                  
            </View>  
        
        )

    }} />

       <Tab.Screen  name="Profile" component={Profile}
       options={{
        headerShown:false,

        tabBarIcon:({focused}) =>(
        <View style={{alignItems:'center'}}> 
          <FontAwesome5 name='user' size={20}
          color={focused? 'red':'#7d2e'}/>  
           <View style={{width:75, alignItems:'center'}}>
           <Text style={{color:focused? 'red':'black'}}>Profile</Text>
            </View>
        </View>  
        
        )

    }} />

     
        <Tab.Screen  name="Feedback" component={Feedback} 
        options={{
          headerShown:false,

          tabBarIcon:({focused}) =>(
          <View style={{alignItems:'center'}}>
            <FontAwesome5 name='newspaper' size={20}
              color={focused? 'red':'#c27b17'}/>
             <View style={{width:85,alignItems:'center'}}>
               <Text style={{color:focused? 'red':'black'}}>Feedback</Text>
            </View>
          </View>  
          )

      }}/>

    <Tab.Screen  name="About" component={About} 
        options={{
          headerShown:false,

          tabBarIcon:({focused}) =>(
          <View style={{alignItems:'center'}}>
            <FontAwesome5 name='info' size={20}
              color={focused? 'red':'#000'}/>
             <View style={{width:85,alignItems:'center'}}>
               <Text style={{color:focused? 'red':'black'}}>About</Text>
            </View>
          </View>  
          )

      }}/>
      </Tab.Navigator>

      
    );
  }




  
  