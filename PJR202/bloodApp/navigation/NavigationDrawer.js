import React,{useState,useEffect} from 'react';
import { StatusBar } from 'expo-status-bar';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList, useDrawerProgress} from '@react-navigation/drawer';
import {View,
    Text,
    Image,
    FlatList,
    BackHandler,
    Alert
  } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import Animated from 'react-native-reanimated';
import { TouchableOpacity } from 'react-native';
import Profile from '../screen/Profile';
import Feedback from '../screen/Feedback';
import About from '../screen/About';
import firebase from '../src/FirebaseConnection';
import { FontAwesome5,MaterialIcons } from '@expo/vector-icons';
import MainHome from '../screen/MainHome';
import Exit from '../screen/Exit';









const Drawer=createDrawerNavigator();

const UserView=()=>{
  


    
   
    return(
        <>
     
        <View style={{backgroundColor:'#B00020',height:240,alignItems:'center',justifyContent:'flex-end'}}>
            <Image source={require("../assets/logo.png")}
            style={{width:'55%',height:150,borderRadius:100}}/>
            <Text style={{color:'white',fontSize:20,fontFamily:'serif'}}>Donate Blood</Text>
        </View>
        
        </> 
    )
}

const CustomDrawerContent=(props)=>{
  const[load,setLoading]=useState(false);
 
    const Logout =async() => {
      setLoading(false);
       await firebase.auth().signOut().then(()=>{
         setLoading(false);
         props.navigation.navigate('MainHome');
       }).catch((err)=>{
         alert(err);
       })
        
      }
      const backAction = () => {
        Alert.alert("Hold on!", "Are you sure you want to exit?", [
          {
            text: "Cancel",
            onPress: () => null,
            style: "cancel"
          },
          { text: "YES", onPress: () => BackHandler.exitApp() }
        ]);
        return true;
      };

    
   const progress = useDrawerProgress();

   const translateX = Animated.interpolateNode(progress, {
     inputRange: [-0, 1],
     outputRange: [-100, 0],
   });
    return(


        <View style={{flex:1,backgroundColor:'#B00020'}}>
            <UserView/>
            <View style={{flex:1,backgroundColor:'white',borderTopLeftRadius:30,borderTopRightRadius:30}}>
            <DrawerContentScrollView {...props}>
            <Animated.View style={{ transform: [{ translateX }] }}>
                <DrawerItemList {...props} />  
            </Animated.View>                      
            </DrawerContentScrollView>
            <View style={{borderTopWidth:1,borderTopColor:'#ccc',padding:20}}>
              
              <TouchableOpacity onPress={backAction}>
              <View style={{flexDirection:'row'}}>
                  <Ionicons name='exit' size={23}/>

                  <Text style={{fontSize:15}}>Exit</Text>
              </View>    
              </TouchableOpacity>
            
          </View>
            </View>
            <StatusBar style='light'/>
        </View>
        
    )
}

function Mydrawer(){
  
    return(
     
            <Drawer.Navigator
            
            initialRouteName="MainHome"
            useLegacyImplementation
            drawerContent={(props) => <CustomDrawerContent {...props} />} 
            screenOptions={{headerStyle:{backgroundColor:'#B00020'},  
            headerTintColor:'#fff',
            headerTitleAlign:'center',
            drawerActiveTintColor:'white',
            drawerActiveBackgroundColor:'#B00020',
            
           }}>
                
                <Drawer.Screen 
                  options={{  
                    headerTitle:'',
                    drawerIcon: ({focused, size}) => (
                       <Ionicons
                          name="home"
                          size={24}
                          color={focused ? 'white' : '#55a3c2'}
                          
                       />
                    ),
                 }}
                name='Home' component={MainHome}/>

            
                
                <Drawer.Screen
                  options={{
          
                    title: 'Contact us',
                    drawerIcon: ({focused, size}) => (

          
                        <MaterialIcons name="security" size={24}  color={focused? 'white':'#097d2e'} />
                  
                    ),
                 }}
                 
                 name='Changepassword' component={Profile}/>

             
                
                 <Drawer.Screen
                  options={{
          
                    title: 'Feedback',
                    drawerIcon: ({focused, size}) => (

                        <FontAwesome5 name='newspaper' size={24}
                        color={focused? 'white':'#50097d'}/>
                    
                    ),
                 }}
                 
                 name='Feedback' component={Feedback}/>
                
                 <Drawer.Screen
                  options={{
                
                    title: 'About App',
                    drawerIcon: ({focused, size}) => (
                     <View style={{marginLeft:15}}>
                          <FontAwesome5 name='info' size={25}
                          color={focused? 'white':'black'}/>

                     </View>   
                       
                    ),
                 }}
                 
                 name='About' component={About}/>
            </Drawer.Navigator>
            
         
        
 
    )
}
export default Mydrawer;