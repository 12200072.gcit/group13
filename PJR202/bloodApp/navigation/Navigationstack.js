import { View, Text } from 'react-native'
import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer, StackRouter } from '@react-navigation/native';
import LoginScreen from '../screen/LoginScreen';
import RegisterScreen from '../screen/RegisterScreen';
import StartScreen from '../screen/StartScreen';
import UploadPicture from '../screen/UploadPicture';
import Home from '../screen/Home';
import Mydrawer from './NavigationDrawer';
import Viewrequest from '../screen/Viewrequest';
import Viewdonor from '../screen/Viewdonor';
import Makerequest from '../screen/Makerequest';
import 'react-native-gesture-handler';
import Editprofile from '../screen/Editprofile';
import  ResetPasswordScreen from '../screen/ResetPasswordScreen'
import MainHome from '../screen/MainHome';
import DonateBlood from '../screen/DonateBlood';
import Mytab from './NavigationTab';
import Moredetail from '../screen/Moredetail';
import Donordetail from '../screen/Donordetail';
import Requestedit from '../screen/Requestedit';
import LoginDonor from '../screen/LoginScreenDonate';
import SignupDonor from '../screen/RegisterScreenDonate';
import Splash from '../screen/Splash';
import Homebutton from '../screen/component/Homebutton';
import Exit from '../screen/Exit';
import Mytop from './Topnavigation';

const Stack=createStackNavigator();


export default function Mystack() {
  return (
     
          <Stack.Navigator 
          initialRouteName='Splash'
          screenOptions={{
            headerStyle:{
            backgroundColor:'#B00020',
          },  
          headerTitleStyle:{
            marginLeft:50,
            fontFamily:'serif',
            fontSize:19
          },
          headerTintColor:'#fff',

          
         }}>
              <Stack.Screen name='Splash' component={Splash} options={{ headerShown:false}}/>
             <Stack.Screen name='Main' component={Mydrawer} options={{
              headerStyle:{backgroundColor:'#8A0707'},headerTitle:'',headerTintColor:'#8A0707',headerShown:false}}/>   
              <Stack.Screen name='Login' component={LoginScreen}  options={{ headerShown:false}}/>
              <Stack.Screen name='Register' component={RegisterScreen}  options={{ headerShown:false}}/>

              <Stack.Screen name='Makerequest' component={Makerequest} options={{ headerShown:false}}/>
              <Stack.Screen name='Viewdonor' component={Viewdonor} options={{ headerShown:false}}/>
              <Stack.Screen name='Viewrequest' component={Viewrequest}  options={{ headerShown:false}}/>
              <Stack.Screen name='Forgot Password' component={ResetPasswordScreen} options={{ headerShown:false}}/>
              <Stack.Screen name='Donate Blood' component={DonateBlood} options={{ headerShown:false}}/>
              <Stack.Screen name='Moredetail' component={Moredetail} options={{ headerShown:false}}/>

              <Stack.Screen name='Donordetail' component={Donordetail}/>
              <Stack.Screen name='Requestedit' component={Requestedit} options={{ headerShown:false}}/>
              <Stack.Screen name='HomeScreen' component={Mydrawer} options={{ headerShown:false}}/>
              <Stack.Screen name='SignupDonor' component={SignupDonor} options={{ headerShown:false}}/>
              <Stack.Screen name='LoginDonor' component={LoginDonor} options={{ headerShown:false}}/>
              <Stack.Screen name='Homebutton' component={Homebutton} options={{ headerShown:false}}/>
              <Stack.Screen name='Exit' component={Exit} options={{ headerShown:false}}/>
            
            
          </Stack.Navigator>
   
  )
}
