import { DefaultTheme } from "react-native-paper";

export const theme ={
    ...DefaultTheme,
    colors:{
        ...DefaultTheme.colors,
        text:'#000000',
        primary:'#0750a7',
        tint:'#FCFCFD',
        secondary:'#0750a7',
        border:'#0e6b0f',
        error:'#f13a59',
        success:'#00B386',
        google:'#2E7D32'
    }
}