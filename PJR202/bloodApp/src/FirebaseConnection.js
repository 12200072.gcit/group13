import firebase from "firebase/compat";
import "firebase/compat/storage";



const firebaseConfig = {
    apiKey: "AIzaSyASbVoc7jZNgAcMWzrzYhNbO7NxgHAih1g",
    authDomain: "bloodapp-7b0aa.firebaseapp.com",
    databaseURL: "https://bloodapp-7b0aa-default-rtdb.firebaseio.com",
    projectId: "bloodapp-7b0aa",
    storageBucket: "bloodapp-7b0aa.appspot.com",
    messagingSenderId: "566280080920",
    appId: "1:566280080920:web:1664b7d43bd052eba13d6d",
    measurementId: "G-G8K3PBZRGW"
  
};

firebase.initializeApp(firebaseConfig);
const storage = firebase.storage();


export {storage, firebase as default };