import React from "react"
import { StyleSheet, View, Text } from "react-native"
import {TextInput as Input} from 'react-native-paper'
import { backgroundColor } from "react-native/Libraries/Components/View/ReactNativeStyleAttributes"
import { theme } from "../core/theme"

export default function CustomInput({errorText, description, ...props}){
    return(
        <View style={styles.container}>
            <Input
            style={styles.input}
            {...props}
            />
            {description && !errorText ?(
                <Text style={styles.description}>{description}</Text>
            ) : null}
            {errorText? <Text style={styles.error}>{errorText}</Text> : null}
        </View>
    )
}

const styles= StyleSheet.create({
    container:{
        width:'100%',
        marginVertical:6,
    },

    input:{

        borderColor:'#eee',
        backgroundColor:'white',
        },
    description:{
        fontSize:13,
        paddingTop:8,
    },
    error:{
        fontSize:13,
        color:theme.colors.error,
        paddingTop:8,
    }
})