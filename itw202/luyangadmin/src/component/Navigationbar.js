


import { Nav, Navbar, NavLink,NavDropdown } from "react-bootstrap";
import { Link } from "react-router-dom";

const Navigationbar = () => {
    return (
        <Navbar collapseOnSelect expand="sm" 
        style={{backgroundColor:'#096c6f',
                position:'sticky',
                top:0,
                width:'100%',
                zIndex:1,
               }}>
            <Navbar.Toggle aria-controls="navbarScroll" data-bs-toggle="collapse" data-bs-target="#navbarScroll" />
            <Navbar.Collapse id="navbarScroll">
                <Nav>
                    <NavLink  
                    style={{marginLeft:30,color:'white',fontSize:22,fontFamily:'serif',fontWeight:'bold'}}>Luyang</NavLink>
                    <NavLink  eventKey="2" as={Link} to="/Viewzhungdra"
                    style={{marginLeft:30,color:'white',fontSize:22,fontFamily:'serif'}}>View Zhungdra</NavLink>
                    <NavLink  eventKey="3" as={Link} to="/Viewboedra"
                    style={{marginLeft:30,color:'white',fontSize:22,fontFamily:'serif'}}>View boedra</NavLink>
                    <NavLink  eventKey="4" as={Link} to="/Viewrigser"
                    style={{marginLeft:30,color:'white',fontSize:22,fontFamily:'serif'}}>View Rigser</NavLink>
                    <NavLink  eventKey="5" as={Link} to="/Viewother"
                    style={{marginLeft:30,color:'white',fontSize:22,fontFamily:'serif'}}>View Other</NavLink>
                      <NavDropdown style={{marginLeft:30,color:'white',fontSize:20,fontFamily:'serif',
                           position:'relative',display:'inline-block',zIndex:1}} title={<text style={{color:'white'}}>Post</text>}>
                        <NavDropdown.Item href="/add">Add Zhundra</NavDropdown.Item>
                        <NavDropdown.Item href="/Addboedra">Add Boedra</NavDropdown.Item>
                        <NavDropdown.Item href="/Addrigser">Add Rigser</NavDropdown.Item>
                        <NavDropdown.Item href="/Addother">Add Other</NavDropdown.Item>
                    </NavDropdown>
                    <NavLink  eventKey="6" as={Link} to="/Viewfeedback"
                    style={{marginLeft:30,color:'white',fontSize:22,fontFamily:'serif'}}>View Feedback</NavLink>
                    <NavLink  eventKey="7" as={Link} to="/"
                    style={{marginLeft:30,color:'white',fontSize:22,fontFamily:'serif'}}>Logout</NavLink>
                </Nav>
            </Navbar.Collapse>     
        </Navbar>
    );
}
 
export default Navigationbar;;