import React from 'react'
import {Modal,Header,Image,Button, ModalDescription,Form} from "semantic-ui-react";

const ModalComp = ({
    open,
    setOpen,
    img,
    title,
    artist,
    lyric,
    audioUrl,
    id,
    handleDelete,
}) => {
  return (
    <Modal
    style={{width:'100%'}}
   onClose={()=> setOpen(false)} 
   onOpen={()=> setOpen(true)} 
   open={open}>
      <Modal.Header>Zhungdra</Modal.Header>   
           <div className='center' style={{marginTop:'4%'}}> 
           <div className='auth'  style={{border:'1px solid #cccc',backgroundColor:'#bd640d',
         borderRadius:6}}>
           <form  name='login_form' style={{border:'1px solid #cccc',
           backgroundColor:'white',
            alignItems:'flex-start',marginLeft:'5px'}}>  
           <p style={{fontFamily:'sans-serif'}}>
              Title:{title}
           </p>
           <p style={{fontFamily:'sans-serif'}}>
              Artist:{artist}
           </p>
           <p style={{fontFamily:'sans-serif'}}>
              Lyric:{lyric}
           </p>
           <p style={{fontFamily:'sans-serif'}}>
           <audio controls>
                <source src={audioUrl} type="audio/mpeg"/>
            </audio>  
            </p> 
           </form>
           <Button color='black' onClick={()=>setOpen(false)}>
              Cancel
          </Button>
          <Button 
          color='red'
          icon="checkmark" 
          content="Delete" 
          labelPosition='right' 
          onClick={()=>handleDelete(id)}/>   
           </div>
           </div>
  </Modal>   
  
  )
}

export default ModalComp