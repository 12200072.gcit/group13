// // Import the functions you need from the SDKs you need
// import { initializeApp } from "firebase/app";
// import {getFirestore} from 'firebase/firestore';
// import {getStorage} from 'firebase/storage';
// import { getAuth } from "firebase/auth";


// const firebaseConfig = {
//   apiKey: "AIzaSyBNcL1rIviIXVmYYtR20rU_HpnZndPta9o",
//   authDomain: "historyapp-dc68a.firebaseapp.com",
//   databaseURL: "https://historyapp-dc68a-default-rtdb.firebaseio.com",
//   projectId: "historyapp-dc68a",
//   storageBucket: "historyapp-dc68a.appspot.com",
//   messagingSenderId: "996759393931",
//   appId: "1:996759393931:web:417828dcba9a3c9344268e"
// };

// // Initialize Firebase
// const app = initializeApp(firebaseConfig);
// export const db=getFirestore(app);
// export const storage=getStorage(app);


import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore';
import { getStorage } from "firebase/storage";
import { getAuth } from "firebase/auth";


const firebaseConfig = {

  apiKey: "AIzaSyDfr_oVeWC9JtvtVuuvB6ZREggHYQi6fhE",
  authDomain: "luyang-ef300.firebaseapp.com",
  databaseURL: "https://luyang-ef300-default-rtdb.firebaseio.com",
  projectId: "luyang-ef300",
  storageBucket: "luyang-ef300.appspot.com",
  messagingSenderId: "5432122993",
  appId: "1:5432122993:web:59cf95feb7dd4faa28c5d0",
  measurementId: "G-DCTHH1H6YR"

};


// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const storage=getStorage(app);
export const db=getFirestore(app);
export const auth=getAuth(app);
export default getFirestore();