import './App.css';

import AddEdit from './pages/AddEdit';
import {BrowserRouter,Routes,Route} from "react-router-dom";
import Feedback from './pages/feedback';
import Login from './pages/Login'
import Viewzhungdra from './pages/Viewzhungdra';
import Addboedra from './pages/Addboedra';
import Viewboedra from './pages/Viewboedra';
import Addrigser from './pages/Addrigser';
import Addother from './pages/Addother';
import Viewrigser from './pages/Viewrigser';
import Viewother from './pages/Viewother';
import Viewfeedback from './pages/feedback';

function App() {
  return (
    <BrowserRouter>
       <div className="App">
         <Routes>
         <Route path='/' element={<Login/>}/>
         
           <Route path='/add' element={<AddEdit/>}/>
           <Route path='/Addboedra' element={<Addboedra/>}/>
           <Route path='/feedback' element={<Feedback/>}/>
           <Route path='/Viewzhungdra' element={<Viewzhungdra/>}/>
           <Route path='/Viewboedra' element={<Viewboedra/>}/>
           <Route path='/Viewother' element={<Viewother/>}/>
           <Route path='/Viewrigser' element={<Viewrigser/>}/>
           <Route path='/Addrigser' element={<Addrigser/>}/>
           <Route path='/Viewfeedback' element={<Viewfeedback/>}/>
           <Route path='/Addother' element={<Addother/>}/>
           <Route path='/update/:id' element={<AddEdit/>}/>
           <Route path='/update1/:id' element={<Addboedra/>}/>
           <Route path='/update2/:id' element={<Addrigser/>}/>
           <Route path='/update3/:id' element={<Addother/>}/>
         </Routes>
      </div>
    </BrowserRouter>
   
  );
}

export default App;
