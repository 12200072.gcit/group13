import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import Navigationbar from "../component/Navigationbar";
import BookDataService from '../component/helper';
import Table from 'react-bootstrap/Table'


const Feedback = ({getBookId}) => {
  const [books, setBooks] = useState([]);

  useEffect(() => {
    getBooks();
  }, []);

  const getBooks = async () => {
    const data = await BookDataService.getAllBooks();
    console.log(data.docs);
    setBooks(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
  };

  const deleteHandler = async (id) => {
    await BookDataService.deleteBook(id);
    alert("Deleted successfully!")
    getBooks();
  };
    

  return (
    <>
    <Navigationbar/>
    {books.map((doc, index) => {
          return(
              <div className='center'>
              <div className='auth'
              style={{border:'1px solid #cccc',backgroundColor:'#bd640d',
              borderRadius:6}}>
                <h3 style={{color:'white'}}>Feedback</h3>
                <form name='login_form'
                style={{border:'1px solid #cccc',
                backgroundColor:'white',borderRadius:6,alignItems:'flex-start',marginLeft:'5px'}}
                key={doc.id}>
                  
                  <p>Name:{doc.name}</p>
         
                  <textarea
                    maxRows={10}
                    aria-label="maximum height"
                    placeholder="Maximum 4 rows"
                    defaultValue={doc.feedback}
                    style={{ width:'100%',height:200}}
                  />
               
                </form>
                <Button
                      variant="danger"
                      className="delete"
                      onClick={(e) => deleteHandler(doc.id)}
                    >
                      Delete
                    </Button>
              </div>
              </div>
          );
        })}
    </>
  );
};

export default Feedback;