import React,{useEffect,useState} from 'react'
import {db} from "../firebase";
import { Button,Card,Grid,Container,Image, CardHeader, CardDescription, CardContent } from 'semantic-ui-react';
import { useNavigate } from 'react-router-dom';
import { collection, deleteDoc, doc, onSnapshot } from 'firebase/firestore';
import Spinner from '../component/Spinner';
import Navigationbar from '../component/Navigationbar'

import { Table, Thead, Tbody, Tr, Th, Td } from 'react-super-responsive-table';
import 'react-super-responsive-table/dist/SuperResponsiveTableStyle.css';
import ModalRigser from '../component/ModalRigser';


const Viewrigser = () => {
    const [users,setUsers] = useState([]);
    const [open,setOpen] = useState(false);
    const [content,setContent] = useState([]);
    const [loading,setLoading]=useState(false);
    const navigate=useNavigate();

    useEffect(()=>{
        setLoading(true);
        const unsub =onSnapshot(collection(db, "Rigser"), (snapshot)=>{
            let list=[];
            snapshot.docs.forEach((doc)=>{
                list.push({id:doc.id, ...doc.data()});
            });
            setUsers(list);
            setLoading(false)
        },
        
        (error)=>{
            console.log(error);
        }
    );
        return ()=>{
            unsub();
        };
    },[]);
    if(loading){
        return <Spinner/>
    }
 const handleModal=(item)=>{
     setOpen(true);
     setContent(item);
 }
 const handleDelete= async(id)=>{
     if(window.confirm("Are You sure want to delete?")){
         try{
             setOpen(false);
             await deleteDoc(doc(db,"Rigser",id));
             setUsers(users.filter((content)=> content.id !==id));
         }
         catch(err){
             console.log(err);
         }
     }
 };
  return (
    <>
    <Navigationbar/>    
    {users && users.map((item)=>(
            <div className='center'>
            <div className='auth'  style={{border:'1px solid #cccc',backgroundColor:'#bd640d',
            borderRadius:6}}>
            <form  name='login_form' style={{border:'1px solid #cccc',
            backgroundColor:'white',borderRadius:6,alignItems:'flex-start',marginLeft:'5px'}}>  
            <p style={{fontFamily:'sans-serif'}}>
               Title:{item.title}
            </p>
            <p style={{fontFamily:'sans-serif'}}>
               Artist:{item.artist}
            </p>
            <p style={{fontFamily:'sans-serif'}}>
               Lyric:{item.lyric}
            </p>
            <p>

            <audio controls>
                <source src={item.audioUrl} type="audio/mpeg"/>
            </audio>   
            </p>
           
            </form>
            <div>
                <Button color='green'
                onClick={()=>navigate(`/update2/${item.id}`)}>
                    update
                </Button>
                <Button color='purple' onClick={()=>handleModal(item)}>
                    View
                </Button>
                {open && (
                    <ModalRigser
                    open={open}
                    setOpen={setOpen}
                    handleDelete={handleDelete}
                {...content}
                    ></ModalRigser>
                )}
            </div>
            </div>
            </div>
            ))}
    </>
  )
}

export default Viewrigser