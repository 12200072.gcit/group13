import React, { useEffect, useState } from 'react'
import {Button,Form,Grid,Loader} from "semantic-ui-react";
import { storage,db } from '../firebase';
import { useParams,useNavigate} from 'react-router-dom';
import {ref,getDownloadURL, uploadBytesResumable } from 'firebase/storage';
import { addDoc,getDoc,doc, collection, serverTimestamp, updateDoc } from 'firebase/firestore';
import Navigationbar from '../component/Navigationbar';
import './form.css'

const initialState={
title:"",
artist:"",
lyric:"",
    
}
const Addother = () => {
    const [data,setData] = useState(initialState);
    const {title,artist,lyric}=data;
    const [file,setFile] = useState(null);
    const [progress,setProgress] = useState(null);
    const [errors,setErrors] = useState({});
    const [isSubmit,setIsSubmit] = useState(false);
    const navigate=useNavigate();
    const {id}=useParams();
    
    useEffect(()=> {
    id && getSingleUser();
    },[id])

  
    const getSingleUser= async () => {
        const docRef= doc(db,"Other",id);
        const snapshot=await getDoc(docRef);
        if(snapshot.exists()){
            setData({...snapshot.data()});
        }
    };

 


    useEffect(() => {
        const uploadFile = () => {
          const name=new Date().getTime()+ file.name;
          const storageRef = ref(storage, `Other/${file.name}`);
          const uploadTask = uploadBytesResumable(storageRef, file);

          uploadTask.on(
            "state_changed",
            (snapshot) => {
              const progress =
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
              console.log("Upload is " + progress + "% done");
              setProgress(progress);
              switch (snapshot.state) {
                case "paused":
                  console.log("Upload is paused");
                  break;
                case "running":
                  console.log("Upload is running");
                  break;
                default:
                  break;
              }
            },
            (error) => {
              console.log(error);
            },
            () => {
              getDownloadURL(uploadTask.snapshot.ref).then((downloadUrl) => {
                setData((prev) => ({...prev, audioUrl:downloadUrl }));
              });
            }
          );
        };
    
        file && uploadFile();
      }, [file]);

const  handleChange=(e)=>{
    setData({...data,[e.target.name]:e.target.value})
};
const validate=()=>{
   let errors={};
   if(!title){
       errors.title="Title cannot be empty"
   }
 
if(!lyric){
    errors.lyric="Lyric cannot be empty";
    
}
return errors;
}
const handleSubmit= async (e)=>{
    e.preventDefault();
    let errors=validate();
    if(Object.keys(errors).length) return setErrors(errors);
    setIsSubmit(true);
    if(!id){
        try {
            await addDoc(collection(db,"Other"),{
                ...data,
                timestamp: serverTimestamp()
            });
          
        } catch (error){
            console.log(error);
        }       
    } else {
        try {
            await updateDoc(doc(db,"Other",id),{
                ...data,
                timestamp: serverTimestamp()
            });
           
        } catch (error){
            console.log(error);
        } 
    }
    navigate("/Viewother");
}
  return (
    <div>
      <Navigationbar/>
      <div className='center' style={{marginTop:'25%'}}> 
    <div className='auth'
    style={{backgroundColor:'#bd640d'}}>
                        {isSubmit ? <Loader active inline="centered" size='huge'/>:
                        (
                            <>
                                <h2>{id ? "Update Other" : "Add Other" }</h2>
                                <Form onSubmit={handleSubmit}
                                style={{
                                        width: 650, 
                                        padding: 25,
                              
                                        borderWidth:1,
                                        borderColor:'#cccc',
                                        backgroundColor:'#cccc',
                      
                                        borderRadius:6}}>
                                    <Form.Input
                                      label="Title"
                                      style={{ borderWidth:2,
                                             borderColor:'red',}}
                                      error={errors.title ? {content:errors.title}:null}
                                      placeholder="Enter Title"
                                      name="title"
                                      onChange={handleChange}
                                      value={title}
                                      autoFocus/>
                                    <Form.Input
                              
                                    label="Artist"
                                    placeholder="Artist name"
                                    name="artist"
                                    onChange={handleChange}
                                    value={artist}
                                    autoFocus/>
                                      
                                    <Form.TextArea
                                    error={errors.lyric ? {content:errors.lyric}:null}
                                    label="Lyric"
                                    style={{height:'300px'}}
                                    placeholder="Write your Lyric here"
                                    name="lyric"
                                    onChange={handleChange}
                                    value={lyric}
                                    
                                    autoFocus/>
                                    <Form.Input
                                        label="Upload"
                                        type='file'
                                        onChange={(e)=>setFile(e.target.files[0])}
                                    />
                                    <Button 
                                        primary 
                                        type='submit' 
                                        disabled={progress  !==null && progress<100}>
                                        Submit
                                    </Button>
                                </Form>
                            </>
                        )}
                    </div>
         </div>
    </div>
  )
}

export default Addother;
