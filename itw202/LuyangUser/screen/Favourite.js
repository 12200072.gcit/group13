import Header from './component/Header'
import HomeButton from './component/HomeButton'
import React, { Component } from 'react';
import firebase from '../src/firebaseConnection';
import { StyleSheet, ScrollView, ActivityIndicator, View,Image,Text,TouchableOpacity } from 'react-native';
import { ListItem } from 'react-native-elements'
import { AntDesign,FontAwesome } from '@expo/vector-icons';
import LottieView from 'lottie-react-native';





class Favourite extends Component {
  constructor() {
    super();
    this.docs = firebase.firestore().collection('favourite');
    this.state = {
      isLoading: true,
      Favourite: []
    };
  }

  componentDidMount() {
    this.unsubscribe = this.docs.onSnapshot(this.fetchCollection);
  }

  componentWillUnmount(){
    this.unsubscribe();
  }

  fetchCollection = (querySnapshot) => {
    const Favourite = [];
    querySnapshot.forEach((res) => {
      const { title,lyric,artist,audioUrl } = res.data();
      Favourite.push({
        key: res.id,
        title,
        lyric,
        artist,
        audioUrl
      });
    });
    this.setState({
      Favourite,
      isLoading: false
   });
  }



  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.loader}>
          <LottieView
                   style={{width:'100%',height:130}}
                    source={require('../assets/music.json')}
                    autoPlay
                    loop={true}
                    speed={0.4}
              
                />
         
        </View>
      )
    }    
    return (
      <>
      {/* <Header>Favourite</Header> */}
      <ScrollView style={styles.wrapper}>
          {
            this.state.Favourite.map((res, i) => {
              return (
                <ListItem 
                   key={i} 
                   onPress={() => {
                      this.props.navigation.navigate('Viewfavourite', {
                        userkey: res.key
                      });
                    }}                   
                   bottomDivider>
                    <ListItem.Content style={{flexDirection:'row',justifyContent:'flex-start'}}>
                    <Image source={require('../assets/P.png')} style={{width:'15%',height:25,marginTop:10}}/>
                    <View style={{flexDirection:'column'}}>
                    <ListItem.Title style={{fontSize:20}}>{res.title}</ListItem.Title>
                    <ListItem.Title style={{fontSize:12}}>by-{res.artist}</ListItem.Title>
                  

                    </View>
                   
                  </ListItem.Content>
                  <AntDesign name="rightcircleo" size={24} color="black" />
                </ListItem>
              );
            })
          }
      </ScrollView>
      <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Home')}} 
      style={{position:'absolute',
      zIndex:1,
      top:550,
      left:280,
      backgroundColor:'#096c6f',
      width:50,
      height:50,
      alignItems:'center',
      justifyContent:'center',
       borderRadius:60}}>
     <LottieView
              
              style={{width:'50%',height:50}}
              source={require('../assets/Home.json')}
              autoPlay
              loop={true}
              speed={0.5}
             
        
   />
      </TouchableOpacity>
      </>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
   flex: 1,
   paddingBottom:30,

  },
  loader: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',    
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  }
})

export default Favourite;