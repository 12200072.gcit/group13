
import React, { Component } from "react";
import { Text, View, StyleSheet, BackHandler, Alert } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

class Exit extends Component {

  handleBackButton = () => {
    Alert.alert( 
    'Exit From App', 'Do you want to Exit From App?', 
    [{ text: 'Cancel',onPress: ()=> console.log('Cancel'),   
    style: 'cancel' }, 
   { text: 'OK',  onPress: ()=> BackHandler.exitApp() }, ],    { 
   cancelable: false } ) 
  return true; 
}   
  componentDidMount(){
    BackHandler.addEventListener('hardwareBackPress',     
    this.handleBackButton);
 } 

componentWillUnmount() {  
  BackHandler.removeEventListener('hardwareBackPress', 
  this.handleBackButton);
} 

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.handleBackButton}>
           <Text style={styles.text}>Click Back button!</Text>
        </TouchableOpacity>
      
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    fontSize: 18,
    fontWeight: "bold"
  }
});

export default Exit;