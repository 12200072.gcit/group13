import { View, Text,StyleSheet,TouchableOpacity,ImageBackground,Image,BackHandler,Alert} from 'react-native'
import React,{useEffect} from 'react'
import Header from './component/Header';
import * as Animatable from 'react-native-animatable';
import Constants from 'expo-constants'; 
import { LinearGradient } from 'expo-linear-gradient';



export default function HomeScreen({navigation}) {
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backAction);

    return () =>
        BackHandler.removeEventListener('hardwareBackPress', backAction);
}, []);

  const backAction = () => {
    if (navigation.isFocused()) {
        Alert.alert('Hold on!', 'Are you sure you want to exit app?', [
           {
               text: 'Cancel',
                onPress: () => null,
                style: 'cancel',
            },
           { text: 'YES', onPress: () => BackHandler.exitApp() },
        ]);
        return true;
    }
};
  return (
    <>
    <LinearGradient colors={['#eee','#ccc']} style={styles. container}>
    

    <Animatable.View animation='fadeInUpBig' style={styles.secondCont}>
   
      <TouchableOpacity  onPress={()=>{navigation.navigate('Zhungdra')}} style={styles.box}>  
      <Image source={require("../assets/image/side.png")} style={{position:'absolute',zIndex:1,width:55,height:55,marginLeft:3,marginTop:3}}/>
      <Image source={require("../assets/image/side1.png")} style={{position:'absolute',zIndex:1,width:55,height:55,marginLeft:255,marginTop:3}}/>
      <Image source={require("../assets/image/side4.png")} style={{position:'absolute',zIndex:1,width:55,height:55,marginTop:55,marginLeft:3}}/>
      <Image source={require("../assets/image/side3.png")} style={{position:'absolute',zIndex:1,width:55,height:55,marginLeft:255,marginTop:55,marginBottom:3}}/>
               
        
      <LinearGradient  colors={['#78909C','#ffffff','#ffffff','#78909C']} style={styles.inner}>
               <Text style={styles.text}>གཞུང་སྒྲ།</Text>
          </LinearGradient>       
       
      </TouchableOpacity>
   

      <TouchableOpacity style={styles.box} onPress={()=>{navigation.navigate('Boedra')}}>
      <Image source={require("../assets/image/side.png")} style={{position:'absolute',zIndex:1,width:55,height:55,marginLeft:3,marginTop:3}}/>
      <Image source={require("../assets/image/side1.png")} style={{position:'absolute',zIndex:1,width:55,height:55,marginLeft:255,marginRight:3,marginTop:3}}/>
      <Image source={require("../assets/image/side4.png")} style={{position:'absolute',zIndex:1,width:55,height:55,marginTop:55,marginLeft:3}}/>
      <Image source={require("../assets/image/side3.png")} style={{position:'absolute',zIndex:1,width:55,height:55,marginLeft:255,marginTop:55,marginBottom:3}}/>
               
      <LinearGradient  colors={['#78909C','#ffffff','#ffffff','#78909C']} style={styles.inner}>
               <Text style={styles.text}>འབོད་སྒྲ།</Text>
          </LinearGradient>  
      </TouchableOpacity>

      <TouchableOpacity style={styles.box} onPress={()=>{navigation.navigate('Rigser')}}>
      <Image source={require("../assets/image/side.png")} style={{position:'absolute',zIndex:1,width:55,height:55,marginLeft:3,marginTop:3}}/>
      <Image source={require("../assets/image/side1.png")} style={{position:'absolute',zIndex:1,width:55,height:55,marginLeft:255,marginRight:3,marginTop:3}}/>
      <Image source={require("../assets/image/side4.png")} style={{position:'absolute',zIndex:1,width:55,height:55,marginTop:55,marginLeft:3}}/>
      <Image source={require("../assets/image/side3.png")} style={{position:'absolute',zIndex:1,width:55,height:55,marginLeft:255,marginTop:55,marginBottom:3}}/>
               
      <LinearGradient  colors={['#78909C','#ffffff','#ffffff','#78909C']} style={styles.inner}>
               <Text style={styles.text}>རིག་གསར།</Text>
          </LinearGradient>  
      </TouchableOpacity>


      <TouchableOpacity style={styles.box} onPress={()=>{navigation.navigate('Other')}}>
      <Image source={require("../assets/image/side.png")} style={{position:'absolute',zIndex:1,width:55,height:55,marginLeft:3,marginTop:3}}/>
      <Image source={require("../assets/image/side1.png")} style={{position:'absolute',zIndex:1,width:55,height:55,marginLeft:255,marginRight:3,marginTop:3}}/>
      <Image source={require("../assets/image/side4.png")} style={{position:'absolute',zIndex:1,width:55,height:55,marginTop:55,marginLeft:3}}/>
      <Image source={require("../assets/image/side3.png")} style={{position:'absolute',zIndex:1,width:55,height:55,marginLeft:255,marginTop:55,marginBottom:3}}/>
               
      <LinearGradient  colors={['#90A4AE','#ffffff','#ffffff','#90A4AE']} style={styles.inner}>
               <Text style={styles.text}>གཞན།</Text>
          </LinearGradient>  
      </TouchableOpacity>
      </Animatable.View> 
      </LinearGradient>
 
   
    </> 
  )
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
  
  
    },
    secondCont:{
      alignItems:'center',
      justifyContent:'center',
      width:'100%',
      height:'50%',
      padding:5,
      marginTop:150,
 
    },
    box:{
      marginTop:20,
      marginRight:10,
      width:'90%',
      marginLeft:5,
      height:115,
      padding:5,
      borderRadius:5,
      opacity:0.7,
      elevation:1.6,
      shadowOffset:{
        width: 0,
        height: 0,
      },
       shadowOpacity: 1.2,
       shadowRadius: 1.41,
       elevation:1,
       shadowColor:'#78909C'
    },
    inner:{
      flex:1,

      alignItems:'center',
      justifyContent:'center',
  
    },
    text:{
      fontSize:25,
      fontFamily:'serif',
      color:'black'
  
    },
    image:{
      width:100,
      height:100,
    },
    Image:{
      width:80,
      height:80
  
    }
  });
  