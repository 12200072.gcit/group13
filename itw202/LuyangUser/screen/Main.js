import Header from './component/Header'
import HomeButton from './component/HomeButton'
import React, { Component } from 'react';
import firebase from '../src/firebaseConnection';
import { StyleSheet, ScrollView, ActivityIndicator, View,Image,Text } from 'react-native';
import { ListItem } from 'react-native-elements'
import { AntDesign,FontAwesome } from '@expo/vector-icons';
import LottieView from 'lottie-react-native';





class Main extends Component {
  constructor() {
    super();
    this.docs = firebase.firestore().collection('All');
    this.state = {
      isLoading: true,
      Main: []
    };
  }

  componentDidMount() {
    this.unsubscribe = this.docs.onSnapshot(this.fetchCollection);
  }

  componentWillUnmount(){
    this.unsubscribe();
  }

  fetchCollection = (querySnapshot) => {
    const Main = [];
    querySnapshot.forEach((res) => {
      const { title,lyric,artist,audioUrl } = res.data();
      Main.push({
        key: res.id,
        title,
        lyric,
        artist,
        audioUrl
      });
    });
    this.setState({
      Main,
      isLoading: false
   });
  }



  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.loader}>
          <LottieView
                    style={{width:'90%',height:70}}
                    source={require('../assets/music.json')}
                    autoPlay
                    loop={true}
                    speed={0.4}
              
                />
         
        </View>
      )
    }    
    return (
      <>
      {/* <Header>Main</Header> */}
      <ScrollView style={styles.wrapper}>
          {
            this.state.Main.map((res, i) => {
              return (
                <ListItem 
                   key={i} 
                   onPress={() => {
                      this.props.navigation.navigate('Viewmain', {
                        userkey: res.key
                      });
                    }}                   
                   bottomDivider>
                    <ListItem.Content style={{flexDirection:'row',justifyContent:'flex-start'}}>
                    <Image source={require('../assets/P.png')} style={{width:'15%',height:25,marginTop:10}}/>
                    <View style={{flexDirection:'column'}}>
                    <ListItem.Title style={{fontSize:20}}>{res.title}</ListItem.Title>
                    <ListItem.Title style={{fontSize:12}}>by-{res.artist}</ListItem.Title>
                  
                    </View>
                   
                  </ListItem.Content>
                  <AntDesign name="rightcircleo" size={24} color="black" />
                </ListItem>
              );
            })
          }
      </ScrollView>
      <HomeButton/>
      </>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
   flex: 1,
   paddingBottom:30,

  },
  loader: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',    
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  }
})

export default Main;