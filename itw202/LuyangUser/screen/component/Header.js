import { View, Text,StyleSheet,StatusBar,Button } from 'react-native'
import React from 'react';
import { LinearGradient } from 'expo-linear-gradient';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { FontAwesome5,
  MaterialCommunityIcons,
  MaterialIcons,
  Ionicons,
  AntDesign,
  Entypo} from '@expo/vector-icons';


const Header = ({children}) => {
  const navigation=useNavigation();
  return (
    <LinearGradient style={styles.container} colors={['#00838F','#00838F']}>
    <TouchableOpacity onPress={() => navigation.goBack()}>
      <View style={{marginTop:50,marginLeft:20}}>
      
        <Entypo name="arrow-bold-left" size={24} color="white" />
      </View>
   
   </TouchableOpacity>
   <View style={{marginLeft:100,width:'70%',justifyContent:'center'}}>
   <Text style={{
   fontFamily:'serif',
   fontSize:25,color:'white',marginTop:30}}>
     {children}
   </Text>

   </View>
  
  
</LinearGradient>
      
  )
}
export default Header;

const styles = StyleSheet.create({
    
    container:{
      height:100,
      backgroundColor:'#096c6f',
      flexDirection:'row'
      
  }
})