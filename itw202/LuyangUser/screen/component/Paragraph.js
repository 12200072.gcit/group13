import { StyleSheet, Text, View,ImageBackground } from 'react-native'
const Paragraph = ({children}) => {
  return (
       <View style={styles.container}>
            <Text style={styles. Paragraph}>{children}</Text>
       </View>  
     
  )
}
export default Paragraph;

const styles = StyleSheet.create({
    Paragraph: {
        width:'97%',
        borderRightWidth:2,
        borderLeftWidth:2,
        borderColor:'#cccc',
        fontSize: 23,
        textAlign:'center',
        color:'#000',
        backgroundColor:'#Ffffff',
        padding:5,
        paddingTop:20
     
    },
    container:{
      flex:1,
      width:'100%',
      borderColor:'#ccc',
      alignItems:'center',
      elevation:4,
      shadowColor:'#000',
        
    
    }
})