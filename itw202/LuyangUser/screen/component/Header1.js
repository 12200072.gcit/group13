import { View, Text,StyleSheet,StatusBar,Button,
  BackHandler,Alert
} from 'react-native'
import React,{useEffect} from 'react';
import { LinearGradient } from 'expo-linear-gradient';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { FontAwesome5,
  MaterialCommunityIcons,
  MaterialIcons,
  Ionicons,
  AntDesign,
  Entypo} from '@expo/vector-icons';
   


const Header1 = ({children}) => {

  
  const navigation=useNavigation();
  return (
    <LinearGradient style={styles.container} colors={['#00838F','#00838F']}>
    <TouchableOpacity onPress={()=>navigation.goBack()}>
      <View style={{marginTop:50,marginLeft:20}}>
      <Entypo name="arrow-bold-left" size={24} color="white" />
      </View>
   
   </TouchableOpacity>
   <View style={{marginLeft:10,width:'70%',justifyContent:'center',alignItems:'center',}}>
   <Text style={{
   fontFamily:'serif',
   fontSize:23,color:'white',marginTop:30}}>
     ༼{children}༽
   </Text>

   </View>
  
  
</LinearGradient>
      
  )
}
export default Header1;

const styles = StyleSheet.create({
    
    container:{
      height:100,
      backgroundColor:'#096c6f',
      flexDirection:'row'
      
  }
})