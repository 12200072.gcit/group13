import React, { useState } from "react";
import { Pressable } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import firebase from "../../src/firebaseConnection";

export default function Likebutton  (){
    const [liked,setLiked] = useState(false);


  return (
     <>
        <Pressable onPress={() => setLiked((isLiked) => !isLiked)}>
      <MaterialCommunityIcons
        name={liked ? "bookmark-music-outline" : "bookmark-music-outline"}
        size={30}
        color="white"
      />
    </Pressable>
     </> 
   
  );
};
