import React from "react"
import { StyleSheet, TouchableOpacity, Image } from "react-native"
import { getStatusBarHeight } from "react-native-status-bar-height"
import { FontAwesome5,
    MaterialCommunityIcons,
    MaterialIcons,
    Ionicons,
    AntDesign} from '@expo/vector-icons';

export default function GoBack({ goBack }){
    return(
        <TouchableOpacity
            onPress={goBack} style={styles.container}>
              <Ionicons name="ios-arrow-undo" size={24} color="white"/>    
        </TouchableOpacity>
    );
}
const styles = StyleSheet.create({
    container:{
        position:'absolute',
        top:10 + getStatusBarHeight(),
        left:4,
    },
})
