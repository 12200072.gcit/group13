import { View, Text,TouchableOpacity} from 'react-native'
import React from 'react'
import { useNavigation } from '@react-navigation/native'
import { FontAwesome5,Ionicons } from '@expo/vector-icons';
import LottieView from 'lottie-react-native';

export default function HomeButton() {

  const [text, setText] = React.useState('');

  const hasUnsavedChanges = Boolean(text);

  React.useEffect(
    () =>
      navigation.addListener('beforeRemove', (e) => {
        const action = e.data.action;
        if (!hasUnsavedChanges) {
          return;
        }

        e.preventDefault();

        Alert.alert(
          'Discard changes?',
          'You have unsaved changes. Are you sure to discard them and leave the screen?',
          [
            { text: "Don't leave", style: 'cancel', onPress:()=>{navigation.navigate('Home')}},
            {
              text: 'Discard',
              style: 'destructive',
              onPress: () => navigation.dispatch(action),
            },
          ]
        );
      }),
    [hasUnsavedChanges, navigation]
  );

    const navigation=useNavigation();
  return (
      <TouchableOpacity onPress={()=>{navigation.navigate('Home')}} 
      style={{position:'absolute',
      zIndex:1,
      top:650,
      left:290,
      backgroundColor:'#096c6f',
      width:50,
      height:50,
      alignItems:'center',
      justifyContent:'center',
    borderRadius:60}}>
         <LottieView
              
              style={{width:'50%',height:100,marginBottom:10,position:'absolute'}}
              source={require('../../assets/Mainh.json')}
              autoPlay
              loop={true}
              speed={0.5}
             
        
   />
      </TouchableOpacity>
 
  )
}