import { StyleSheet, Text, View } from 'react-native'
const Title = ({children}) => {
  return (
       <Text style={styles.title}>{children}</Text>
  )
}
export default Title;

const styles = StyleSheet.create({
    title: {
        fontFamily: 'serif',
        borderWidth: 2,
        borderColor: '#fff',
        textAlign: 'center',
        fontSize: 23,
        color:'#fff',
        padding:12,
        backgroundColor:'#FEEAE6',
        // position:'absolute',
        // zIndex:1,
    }
})