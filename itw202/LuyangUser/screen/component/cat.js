import React, { useState, useEffect  } from 'react';
import { StyleSheet, View, ScrollView, Text, TouchableOpacity,ImageBackground} from 'react-native';
import { Table, Row, Cell, TableWrapper } from 'react-native-table-component';
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import { Picker } from '@react-native-picker/picker';
// import call from 'react-native-phone-call';
// import firebase from '../src/FirebaseConnection';
import {LinearGradient} from 'expo-linear-gradient';
import firebase from 'firebase/app';
import "firebase/auth";
import "firebase/firestore"; const Category=()=>{
    const auth = firebase.auth;
    const firestore = firebase.firestore;    const [data , setData] = useState({
    tableHead: ['Enrollment', 'Name', 'Status'],
    allstds: [],
    tableData: [],
    widthArr: [117, 117, 117],
    });
    const [loading, setLoading] = useState(true);
    // const [selectedValue, setSelectedValue] = useState("All");
    // const [selectedCityValue, setSelectedCityValue] = useState("All");    // useEffect(()=>{
    //     const newData = [];
    //     const allDonorsFromFirebase = [];
    //     var donors = firebase.database().ref('donors/');
    //     donors.on('value', (snapshot) => {
    //       const donorsData = snapshot.val();
    //       const keys = Object.keys(donorsData);
    //       keys.forEach((key)=>{
    //         newData.push(donorsData[key]);
    //       });
    //       newData.forEach((data)=>{
    //         const {donorName, dzongkhag,gender,location, bloodGroup, mobileNo} = data;
    //         allDonorsFromFirebase.push([donorName,dzongkhag,location,bloodGroup, mobileNo]);
    //       });
    //           setData({...data,  tableData:allDonorsFromFirebase, allDonors:allDonorsFromFirebase});
    //           setLoading(false);
    //     });
    // },[]);
    // useEffect(() => {
    //   let unmounted = false;
    //   setTimeout(() => {
    //     if (!unmounted) {
    //       // update state here...
    //     }
    //   }, 3000);    //   return () => {
    //     unmounted = true;
    //   };
    // });
    const[year, setYear]=useState('Year');
    const[course, setCourse]=useState('Course');
    const[section, setSection]=useState('Section');
    // const [isloading, setIsloading] = useState(true)    useEffect(()=>{      const fetchStd = async() => {
        try {
          const newData=[];
          const allStdList=[];          await firestore()
          .collection('students')
          .orderBy('enroll', 'asc')
          .get()
          .then('value',(querySnapshot)=>{
            const stdsData = querySnapshot.val();
            const keys = Object.keys(stdsData);
            keys.forEach((key)=>{
              newData.push(stdsData[key]);
            });
              console.log('Total std :', querySnapshot.size);
              querySnapshot.forEach(doc => {
                const {enroll,name} = doc.data();
                newData.push({
                  name : name,
                  enroll: enroll,
                })
                allStdList.push([enroll,name]);
                console.log(allStdList)
              })
            })
          setData(...data, tableData=allStdList, allstds=allStdList);
          console.log('std :',data)        } catch (error) {
          console.log(error)        }
        setLoading(false)
      }     fetchStd();
    },[])    // function to filter results by blood group
    const filterByYear = (itemValue)=>{
    if(year=="Year"){
      if(itemValue=="Year"){
        data.tableData =  data.allstds;
      }
      else{
        data.tableData =  data.allstds.filter((d)=>{
          return d[2]==itemValue;
        });
      }
    }
    else{
      if(itemValue=="Year"){
        data.tableData =  data.allstds.filter((d)=>{
          return (d[1]==year);
        });
      }
      else{
        data.tableData =  data.allstds.filter((d)=>{
          return (d[2]==itemValue && d[1]==year);
        });
      }
    }
    setYear(itemValue)
  }  const filterByCourse = (itemValue)=>{
    if(itemValue=="Course"){
      data.tableData = data.allstds;
    }
    else{
      data.tableData =  data.allstds.filter((d)=>{
        return d[1]==itemValue;
      });
    }
        setCourse(itemValue);
  }
  const filterBySection = (itemValue)=>{
    if(itemValue=="Section"){
      data.tableData = data.allstds;
    }
    else{
      data.tableData =  data.allstds.filter((d)=>{
        return d[1]==itemValue;
      });
    }
        setSection(itemValue);  }
  console.log(year)
  console.log(course)
  console.log(section)
  console.log(data)
  console.log(data.allstds)
  console.log(data.tableData)    // const element = (data, index) => (
    //     <TouchableOpacity style={{justifyContent:"center", alignItems:"center"}} onPress={() => {
    //       const args = {
    //         number: data, // String value with the number to call
    //       }
    //       call(args).catch(console.error);
    //     }}>
    //       <View style={styles.callBtn}>
    //         <Text style={styles.callBtnText}>{data}</Text>
    //       </View>
    //     </TouchableOpacity>
    //   );    return (    loading?    <SkeletonPlaceholder backgroundColor="#E6E6E6
">
      <View style={{ margin:20 }}>
      <View style={{ width: "10%", height: 40, borderRadius: 4, marginTop:10 }} />
      <View style={{ width: "10%", height: 40, borderRadius: 4, marginTop:10 }} />
      <View style={{ width: "10%", height: 40, borderRadius: 4, marginTop:10 }} />
      <View style={{ width: "10%", height: 40, borderRadius: 4, marginTop:10 }} />
      <View style={{ width: "10%", height: 40, borderRadius: 4, marginTop:10 }} />
      <View style={{ width: "10%", height: 40, borderRadius: 4, marginTop:10 }} />
      <View style={{ width: "10%", height: 40, borderRadius: 4, marginTop:10 }} />
      <View style={{ width: "10%", height: 40, borderRadius: 4, marginTop:10 }} />
      <View style={{ width: "10%", height: 40, borderRadius: 4, marginTop:10 }} />
      <View style={{ width: "10%", height: 40, borderRadius: 4, marginTop:10 }} />
      </View>
  </SkeletonPlaceholder>:  <View>
  {/* <LinearGradient colors={['#EBDF75
','orange']}> */}
    <View style={{flexDirection:'row'}}>      <Picker style={styles.picker1}
      selectedValue={year}
      onValueChange={
        (itemValue, itemIndex) => {
          filterByYear(itemValue);
      }}>          <Picker.Item label='Year' value='Year'/>
          <Picker.Item label='1' value='1'/>
          <Picker.Item label='2' value='2'/>
          <Picker.Item label='3' value='3'/>      </Picker>
      <Picker style={styles.picker1}
      selectedValue={course}
      onValueChange={(itemValue, itemIndex) => {
        filterByCourse(itemValue);
      }}>          <Picker.Item label='Course' value='Course'/>
          <Picker.Item label='Bsc.IT' value='Bsc.IT'/>
          <Picker.Item label='Bsc.CS' value='Bsc.CS'/>
          <Picker.Item label='Bsc.CA' value='Bsc.CA'/>      </Picker>
      <Picker style={styles.picker1}
      selectedValue={section}
      onValueChange={(itemValue, itemIndex) => {
        filterBySection(itemValue);
      }}>          <Picker.Item label='Section' value='Section'/>
          <Picker.Item label='A' value='A'/>
          <Picker.Item label='B' value='B'/>
          <Picker.Item label='C' value='C'/>      </Picker>
    </View>      <ScrollView  horizontal={true}>
      <View style={styles.container}>      <Table borderStyle={{borderWidth: 1, borderColor: '#A6A6A6
',}}>          <Row data={data.tableHead} widthArr={data.widthArr} style={styles.head}/>
          {            data.tableData.map((rowData, index) => (              <TableWrapper key={index} style={styles.row} >
                {                  rowData.map((cellData, cellIndex) => (                    <Cell  key={cellIndex} data={cellIndex === 4 ? element(cellData, index) : cellData}  style={styles.cell}/>                  ))                }
              </TableWrapper>
            ))          }        </Table>
      </View>
    </ScrollView>
  {/* </LinearGradient> */}
 </View>    )
  }const styles = StyleSheet.create({
  picker1:{
    height:50,
    width:105,
    borderColor:'black',
    borderWidth:2,
    backgroundColor:'pink',
    margin:10,
    marginTop:20
},
cell:{
    width:100
  },
container: {
    flex: 1,
    padding: 5,
    backgroundColor: '#eee'
},
  head: {
      backgroundColor: '#B81100
',},
  text: {
      margin: 6
},
  headText:{
    margin:6,
    color: 'white',
    fontFamily:'serif'
},
  inputConatiner:{
    width:"44%",
    // backgroundColor:"red",
    padding:1,
},
row: {
    flexDirection: 'row',
    backgroundColor: '#fff'
},
callBtn: {      padding:5,
      alignItems : "center",
      justifyContent : "center",
      backgroundColor: 'green',
      borderRadius: 2 ,
},
callBtnText: {
      textAlign: 'center',
      color: 'white',},
picker:{
    width: "100%",
    marginLeft:10,
    height:30,
    padding:5,
    color:"#666666
",
},
filters:{
    backgroundColor: '#eee',
    width: "100%",
    margin:2,
    justifyContent : "space-around",
    alignItems : "center",
    flexDirection:"row",
    marginTop:30,
},
labelText:{
    padding:1,
    color:"red",
    fontFamily:'serif',
    fontSize:18
},selectBox:{
    width:"100%",
    backgroundColor:"white",
    borderRadius:6,
    alignItems:"center",
    marginLeft:"auto",
    marginRight:"auto",
    marginTop:5,
},
ImageBackground:{
  opacity:0.69
}
});
export default Category;