import { View, Text,Image,StyleSheet, TouchableOpacity,StatusBar,Dimensions,useWindowDimensions} from 'react-native'
import React from 'react';
import { LinearGradient } from 'expo-linear-gradient';

export default function StartScreen({navigation}) {



  return (
    <View style={[styles.container]}>
        <StatusBar  barStyle="light-content" backgroundColor="#096c6f"/> 

       <View style={styles.imagecontainer}>
           <Image source={require('../assets/image/e.png')} style={[styles.Headerimage]}/>
           <Image source={require('../assets/image/11.png')}  style={[styles.image]}/>
       </View>  
        <View>
            <Text style={{fontFamily:'serif',
            textAlign:'center',
            fontSize:20}}>Welcome</Text>
            <LinearGradient colors={['#356859','#B9E4C9','#37966F']} style={styles.innerView}>
                <TouchableOpacity onPress={()=>{navigation.navigate('HomeScreen')}} style={styles.button}>
                    <View >
                        <Text>Start</Text>
                    </View>
                </TouchableOpacity>    
                
            </LinearGradient>
        </View>
        
      
    </View>
  )
}
const styles=StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#eee',
    },
    Headerimage:{
        marginTop:1,
        width:'100%',
        height:80
    },
    image:{

        width:'100%',
        height:370,
        marginTop:20,
    },
    innerView:{
        backgroundColor:'#0336FF',
        height:220,
        borderTopLeftRadius:30,
        borderTopRightRadius:30,
        opacity:0.9,
        justifyContent:'center',
        alignItems:'center',
    },
    button:{
        width:'50%',
        backgroundColor:'#eee',
        height:50,
        borderRadius:12,
        alignItems:'center',
        justifyContent:'center',
    },
   
})