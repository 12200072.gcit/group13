import React, { Component } from 'react';
import { Audio } from 'expo-av';
import firebase from '../src/firebaseConnection';
import ReactAudioPlayer from 'react-audio-player';
import SoundPlayer from 'react-native-sound-player'
import { Alert,ActivityIndicator,
   View, 
   StyleSheet, 
   Text,

   ScrollView,Share} from 'react-native';
import CustomInput from '../src/components/CustomInput';
import Button from '../src/components/Button';
import HomeButton from './component/HomeButton';
import Header1 from './component/Header1';
import Title from './component/Title';
import Paragraph from './component/Paragraph';
import { TouchableOpacity } from 'react-native-gesture-handler';
import ActionButton from 'react-native-action-button';
import LottieView from 'lottie-react-native';
import {AntDesign,Octicons,MaterialCommunityIcons,MaterialIcons,Entypo,Ionicons} from '@expo/vector-icons'
import { types } from 'react-native-document-picker';
import play from './audioController'
class Viewrigser extends Component {
  constructor() {
    super();
    this.state = {
      title: '',
      lyric: '',
      artist:'',
      audioUrl:'',
      playbackObj:null,
      soundObj:null,
      currentAudio:{},
      isPlaying:false,
      isLoading: true
    };
  }
  onValUpdate = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);   
  }
  
  componentDidMount() {
    const docRef = firebase.firestore().collection('Rigser').doc(this.props.route.params.userkey)
    docRef.get().then((res) => {
      if (res.exists) {
        const user = res.data();
        this.setState({
          key: res.id,
          title: user.title,
          lyric: user.lyric,
          artist:user.artist,
          audioUrl:user.audioUrl,
          isLoading: false
        });
      } else {
        console.log("No document found.");
      }
    });
  }
  addFovourite() {

    const docUpdate = firebase.firestore().collection('favourite').doc(this.state.key);
    docUpdate.set({
      title: this.state.title,
      lyric: this.state.lyric,
      artist:this.state.artist,
      audioUrl:this.state.audioUrl,
    }).then((docRef) => {
    
      Alert.alert(`${this.state.title}\n successful added to Favourite.`);
     
    })
    .catch((error) => {
      console.error(error);
     
    });
  }

  inputEl = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }
  shareData = async () => {
		try {
			await Share.share({
        message:`Title:${this.state.title}\nLyric:${this.state.lyric}`,
	
					
			});
		} catch (error) {
			alert(error.message);
		}
	};


  handleAudioPress=async audio=>{
    //playing audio for the first tmie
    if(this.state.soundObj===null){
      const playbackObj=new Audio.Sound();
      const status=await playbackObj.loadAsync(
        {uri:this.state.audioUrl},
        {shouldPlay:true}
        );
        return this.setState({
          ...this.state,
          playbackObj:playbackObj,
          currentAudio:audio,
          soundObj:status
        });
     
    }
    if(this.state.soundObj.isLoaded 
      && this.state.soundObj.isPlaying){
      const status= await this.state.playbackObj.setStatusAsync({shouldPlay:false});
     return this.setState({
      ...this.state,
      soundObj:status,
    });
    }
    //resume audio
    if(this.state.soundObj.isLoaded && !this.state.soundObj.isPlaying
      && this.state.currentAudio.id===audio.id){
        const status= await this.state.playbackObj.playAsync()
        return this.setState({
          ...this.state,
          soundObj:status
        })
      }

  }
   
      
  render() {
  
    if(this.state.isLoading){
      return(
        <View style={styles.loader}>
           <LottieView
                    style={{width:'100%',height:130}}
                    source={require('../assets/music.json')}
                    autoPlay
                    loop={true}
                    speed={0.4}
            
                />
         
        </View>
      )
    }
   
    return (
      <>
      <Header1>འབོད་སྒྲ།</Header1>
      <View style={styles.container}>
      <ScrollView style={{width:'100%'}}>
       
        <View >
        
          <Paragraph >{this.state.lyric}</Paragraph>
         
  
        </View>
      </ScrollView>
      </View> 
      <View>
        
        </View>
        
        <View style={{flexDirection:'row',height:60,borderTopColor:'#cccc',elevation:3,backgroundColor:'#DFE1E3'}}>
          
          <View style={{width:'25%',
          alignItems:'center',
          justifyContent:'center',
          borderLeftColor:'#cccc',
          elevation:3}}>
            <TouchableOpacity onPress={()=>{this.props.navigation.navigate('HomeScreen')}}>
            <LottieView
                      style={{width:'20%',height:50,}}
                      source={require('../assets/Hom.json')}
                      autoPlay
                      loop={true}
                      speed={0.6}
  
             />
  
            </TouchableOpacity>
                
          
                
          </View>
          <View style={{width:'25%',
          alignItems:'center',
          justifyContent:'center',
          borderLeftColor:'#cccc',
          elevation:3}}
        > 
        <TouchableOpacity
        onPress={this.handleAudioPress}
        >
         <LottieView
                  
                  style={{width:'20%',height:50}}
                  source={require('../assets/play.json')}
                  autoPlay
                  loop={true}
                  speed={0.4}
                />
        </TouchableOpacity>
         
          </View>
          <View style={{width:'25%',
          alignItems:'center',
          justifyContent:'center',
          borderLeftColor:'#cccc',
          elevation:3}}> 
          <TouchableOpacity 
           onPress={() => this.addFovourite()}>
              <LottieView
                      style={{width:'20%',height:45}}
                      source={require('../assets/love.json')}
                      autoPlay
                      loop={true}
                      speed={0.4}
                    />
  
          </TouchableOpacity>  
        
          </View>
          
          <View style={{width:'25%',
          alignItems:'center',
          justifyContent:'center',
          borderLeftColor:'#cccc',
          elevation:3}}>
            <TouchableOpacity 
             onPress={this.shareData}>
            <LottieView
                      style={{width:'20%',height:40}}
                      source={require('../assets/shar.json')}
                      autoPlay
                      loop={true}
                      speed={0.4}
                />
  
            </TouchableOpacity>
             
          
          </View>
        </View>
   
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'center',
    backgroundColor:'#ffffff'
  },
  formEl: {
    flex: 1,
    padding: 0,
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#cccccc',
  },
  loader: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',    
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },
  button: {
    marginBottom: 8, 
  }
})

export default Viewrigser;