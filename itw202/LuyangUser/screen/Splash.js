import React, { Component } from 'react';
import { View,Image,Text} from 'react-native';
import LottieView from 'lottie-react-native'


export default class Splash extends Component {
    constructor(props) {
        super();
    }

    render() {
        return (
            <>
            <View style={{flex:1,backgroundColor:'#d5dbdb'}}>
            <Image source={require('../assets/e.png')} style={{width:'100%',height:70,marginTop:5}}/>
            <View
                style={{
                    flex: 1,
                    backgroundColor:'#d5dbdb',marginBottom:350}}>
                <LottieView
                    source={require('../assets/splash.json')}
                    autoPlay
                    loop={false}
                    speed={0.3}
                    onAnimationFinish={() => {
                        console.log('Animation Finished!')
                        this.props.navigation.navigate('HomeScreen');
                    }}
                />
                  <LottieView
                    source={require('../assets/splash1.json')}
                    autoPlay
                    loop={false}
                    speed={0.9}
        
                />
                 
                <Image source={require('../assets/11.png')}
                style={{width:'100%',height:350,borderRadius:100,marginTop:120}}/>
               
                <Text style={{textAlign:'center',fontSize:30,color:'#000000'}}>འབྱོན་པ་ལེགས་སོ།</Text>
                <Text style={{textAlign:'center',fontSize:30,color:'#000000'}}>
                    LUYANG
                    </Text>    
            </View>
            </View>
            </>
        )
    }
}



