import React from 'react';
import { View, Text, StyleSheet, Image, ScrollView } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient';
const About = () => {
  return (
  
    <View style={styles.container}>
      <Text style={{fontSize:24}}>About App</Text>
      <Text>
      {'\  '}Luyang is an application that stores Bhutanese {'\n'}{'\                '}lyrical songs in a digital form.
       {'\n'}{'\  '}The various kind of lyrics are stored in categories{'\  '} for user’s convenience. 
       Along with the lyrics user can listen to the songs. 
       The lyrics will be stored in a database permanently which can be used for future reference.
      </Text>
      <View style={styles.view1}>
       
      </View>
      <Text style={{fontSize:24}}>About Developer</Text>
      <View style={styles.view}>
        <Image style={styles.image} source={require('../assets/image/phu.jpeg')}/>
        <Image style={styles.image} source={require('../assets/image/tan.jpeg')}/>
        <Image style={styles.image} source={require('../assets/image/pema.jpeg')}/>
        <Image style={styles.image} source={require('../assets/image/norbu.jpeg')}/>
      </View>
      <View style={styles.view}>
        <Text>Phuntsho{'\n'}Wangmo</Text>
        <Text>Tandin {'\n'}Wangchuk</Text>
        <Text>Pema {'\n'}Wangmo</Text>
        <Text>Pema {'\n'}Norbu</Text>
      </View>
     
    </View>
  
  );
}
export default About;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor:'#EBA93F

  },
  view:{
    // height:'10%',
    width : '98%',
    // borderWidth:2,
    // borderColor:'red',
    margin:'5%',
    flexDirection:'row',
    justifyContent:'space-around',
    alignItems:'center'
  },
  view1:{
    // height:'10%',
    width : '98%',
    // borderWidth:2,
    // borderColor:'red',
    margin:'5%',
    // flexDirection:'row',
    justifyContent:'center',
    alignItems:'center'
  },
  image:{
    height:90,
    width:90,
    borderRadius:50
  },
  text:{
    // fontFamily:'arial',
    // fontWeight:'normal',
    fontSize:15,
    textAlign:'center'
  },  bg: {
    flex: 1,
  }
});