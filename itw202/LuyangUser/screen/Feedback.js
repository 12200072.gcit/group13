
import HomeButton from './component/HomeButton'
import React, { Component } from 'react';
import {StyleSheet, ScrollView, ActivityIndicator, View, TextInput,Alert,Text,TouchableOpacity} from 'react-native';
import firebase from '../src/firebaseConnection';
import CustomInput from '../src/components/CustomInput';
import Button from '../src/components/Button';
import { Ionicons } from '@expo/vector-icons';
import LottieView from 'lottie-react-native';



class Feedback extends Component {
  constructor() {
    super();
    this.ref = firebase.firestore().collection('Feedback');
    this.state = {
      name: '',
      feedback: '',
      isLoading: false
    };
  }


  onValUpdate = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);

   
  }

  addFeedback() {
    if(this.state.name ===''){
      Alert.alert("Name required."
      )
    }else if(this.state.feedback===''){
      Alert.alert("Feedback is required."
      )

    }
     else {
      this.setState({
        isLoading: true,
      });      
      this.ref.add({
        name: this.state.name,
        feedback: this.state.feedback,
      }).then((res) => {
        this.setState({
          name: '',
          feedback: '',
          isLoading: false,
        });
        this.props.navigation.goBack();
        Alert.alert("Thank You!For your Feedback.")
      
      })
      .catch((err) => {
        console.error("Error occured: ", err);
        this.setState({
          isLoading: false,
        });
      });
    }
  }
  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.loading}>
           <LottieView
                    style={{width:'100%',height:130}}
                    source={require('../assets/music.json')}
                    autoPlay
                    loop={true}
                    speed={0.4}
                    
                />
        </View>
      )
    }
    return (
      <>
      <View style={styles.container}>
      <ScrollView>
        <View >
          <CustomInput
              mode='outlined'
              placeholder='Name'
              value={this.state.name}
              onChangeText={(val) => this.onValUpdate(val, 'name')}
          />
        </View>
        <View>
          <CustomInput mode='outlined'
              style={{borderBottomColor:'#1b959a'}}
              multiline={true}
              numberOfLines={10}
              placeholder='Write  Your feedback...'
              value={this.state.feedback}
              onChangeText={(val) => this.onValUpdate(val, 'feedback')}
          />
        </View>
        <View>
          <Button
          raised
            onPress={() => this.addFeedback()} 
            mode='contained'
            color="#1b959a"
          >
            <Text style={{fontFamily:'serif',fontSize:18}}>
            submit
            </Text>
            {/* <Ionicons name="ios-add-outline" size={24} color="black" /> */}
             
             
          </Button>
        </View>
      </ScrollView>
      </View>
      <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Home')}} 
      style={{position:'absolute',
      zIndex:1,
      top:550,
      left:280,
      backgroundColor:'#096c6f',
      width:50,
      height:50,
      alignItems:'center',
      justifyContent:'center',
       borderRadius:60}}>
     <LottieView
              
              style={{width:'50%',height:50}}
              source={require('../assets/Home.json')}
              autoPlay
              loop={true}
              speed={0.5}
             
        
   />
      </TouchableOpacity>
   
 
     
 
    
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 35,
    backgroundColor:'#ffffff',
  },
  formEle: {
    flex: 1,
    padding: 5,
    marginBottom: 12,
    borderBottomWidth: 1,
    borderBottomColor: '#4e4e4e',
  },
  loading: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',    
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  }
})

export default Feedback;