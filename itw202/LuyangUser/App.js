import { View, Text,Button,StatusBar} from 'react-native'
import React ,{Component}from 'react'
import MyStack from './navigation/Stacknavigation'
import { NavigationContainer } from '@react-navigation/native'
import Splash from './screen/Splash'

export default function App() {
  return (
    <NavigationContainer>
      <StatusBar barStyle='light-content'backgroundColor='#096c6f'/>
       <MyStack/>
    </NavigationContainer> 
    // <Splash/>
   
  )
}

