import { View, Text,StatusBar} from 'react-native'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import StartScreen from '../screen/StartScreen'
import HomeScreen from '../screen/HomeScreen'
import 'react-native-gesture-handler';
import Mydrawer from './Drawernavigation'
import Zhungdra from '../screen/Zhungdra'
import Boedra from '../screen/Boedra'
import Rigser from '../screen/Rigser'
import Other from '../screen/Other'
import Viewzhungdra from '../screen/Viewzhungdra'
import Viewboedra from '../screen/Viewboedra'
import Viewrigser from '../screen/Viewrigser'
import Viewother from '../screen/Viewother'
import Viewfavourite from '../screen/Viewfavourite'
import Viewmain from '../screen/Viewmain'
import Splash from '../screen/Splash'

const Stack =createStackNavigator();
export default function MyStack() {
  return (
          <Stack.Navigator
          initialRouteName='Splash'
          screenOptions={{
              headerStyle:{
                  backgroundColor:' #094f70'
              }
              
          }}>
              <Stack.Screen name='Start' component={StartScreen}  options={{headerShown:false}}/>
              <Stack.Screen name='HomeScreen' component={Mydrawer}  options={{headerShown:false}}/>
              <Stack.Screen name='Zhungdra' component={Zhungdra} options={{headerShown:false}}/>
              <Stack.Screen name='Boedra' component={Boedra}options={{headerShown:false}}/>
              <Stack.Screen name='Rigser' component={Rigser} options={{headerShown:false}}/>
              <Stack.Screen name='Other' component={Other} options={{headerShown:false}}/>
              <Stack.Screen name='Viewzhungdra' component={Viewzhungdra} options={{headerShown:false}}/>
              <Stack.Screen name='Viewboedra' component={Viewboedra} options={{headerShown:false}}/>
              <Stack.Screen name='Viewrigser' component={Viewrigser} options={{headerShown:false}}/>
              <Stack.Screen name='Viewother' component={Viewother} options={{headerShown:false}}/>
              <Stack.Screen name='Viewfavourite' component={Viewfavourite} options={{headerShown:false}}/>
              <Stack.Screen name='Viewmain' component={Viewmain} options={{headerShown:false}}/>
              <Stack.Screen name='Splash' component={Splash} options={{headerShown:false}}/>
              
          </Stack.Navigator>


  )
}