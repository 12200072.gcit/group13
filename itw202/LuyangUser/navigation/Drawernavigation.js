import React,{useState,useEffect} from 'react';
import { StatusBar } from 'expo-status-bar';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList,  useDrawerProgress} from '@react-navigation/drawer';
import {View,
    Text,
    Image,
    BackHandler,
    Alert,
  } from 'react-native';

import Animated from 'react-native-reanimated';
import { TouchableOpacity } from 'react-native';
import { FontAwesome5,
    MaterialCommunityIcons,
    MaterialIcons,
    Ionicons,
    AntDesign,
    Fontisto} from '@expo/vector-icons';

import { LinearGradient } from 'expo-linear-gradient';
import Feedback from '../screen/Feedback';
import About from '../screen/About';
import HomeScreen from '../screen/HomeScreen';
import Favourite from '../screen/Favourite';
import Mytop from './Topnavigation';
import Exit from '../screen/Exit';







const Drawer=createDrawerNavigator();

const UserView=()=>{  
    return(
        <> 
        <LinearGradient colors={['#00838F','#00838F']} style={{height:240,alignItems:'center',justifyContent:'flex-end'}}>
            <Image source={require('../assets/11.png')} style={{width:'65%',height:170,borderRadius:100}}/>
            <Text style={{color:'white',fontSize:25,fontFamily:'serif'}}>གླུ་དབྱངས།</Text>
        </LinearGradient>
        </> 
    )
}

const CustomDrawerContent=(props)=>{
 
  
    
   const progress = useDrawerProgress();

   const translateX = Animated.interpolateNode(progress, {
     inputRange: [-0, 1],
     outputRange: [-100, 0],
   });

   const backAction = () => {
    Alert.alert("Hold on!", "Are you sure you want to exit?", [
      {
        text: "Cancel",
        onPress: () => null,
        style: "cancel"
      },
      { text: "YES", onPress: () => BackHandler.exitApp() }
    ]);
    return true;
  };

    return(


        <View style={{flex:1,backgroundColor:'#00838F'}}>
            <UserView/>
            <View style={{flex:1,backgroundColor:'white'}}>
            <DrawerContentScrollView {...props}>
            <Animated.View style={{ transform: [{ translateX }] }}>
                <DrawerItemList {...props} />  
            </Animated.View>
            
                             
            </DrawerContentScrollView>
            
            <View style={{borderTopWidth:1,borderTopColor:'#ccc',padding:20}}>
                <TouchableOpacity style={{flexDirection:'row'}} onPress={backAction}>
                <Ionicons name="ios-exit-outline" size={26} color='#0f797d'/>
                    <Text style={{fontFamily:'serif',fontSize:15,marginLeft:20,marginTop:5}}>Exit</Text>
                </TouchableOpacity>
                  
            </View> 
        
            </View>
            
            <StatusBar style='light'/>
        </View>
        
    )
}

function Mydrawer(){
  
    return(
     
            <Drawer.Navigator
            
            initialRouteName="Home"
            useLegacyImplementation
            drawerContent={(props) => <CustomDrawerContent {...props} />} 
            screenOptions={{
            headerStyle:{
            backgroundColor:'#00838F',
            height:105},  
            headerTintColor:'#fff',
            headerTitleAlign:'center',
            drawerActiveTintColor:'white',
            drawerActiveBackgroundColor:'#839192',
            height:100,
            
           }}>
                
                <Drawer.Screen 
                  options={{  
                    headerTitle:'གླུ་དབྱངས།',
                    headerTitleStyle:{
                        marginRight:20,
                        fontSize:30,
                        fontFamily:'serif'
                    },
                    drawerIcon: ({focused, size}) => (
                    <Ionicons name="ios-home"  size={26} color={focused ? 'white' : '#0f797d'}/>
                    ),
                 }}
                name='Home' component={HomeScreen}/>
                 <Drawer.Screen 
                  options={{  
                    headerTitle:'Favourite',
                    headerTitleStyle:{
                        marginRight:0,
                        fontSize:25,
                        fontFamily:'serif'
                    },
                    drawerIcon: ({focused, size}) => (
                        <MaterialIcons name="favorite-outline" size={26} color={focused ? 'white' : '#0f797d'}/>
                    ),
                 }}
                name='Favourite' component={Favourite}/>
                
                 <Drawer.Screen
                  options={{
          
                    headerTitle:'Feedback',
                    headerTitleStyle:{
                        marginRight:20,
                        fontSize:20,
                        fontFamily:'serif'
                    },
                    drawerIcon: ({focused, size}) => (

                        <MaterialIcons 
                        name="feedback" 
                        size={26} 
                        color={focused ? 'white' : '#0f797d'} />
                    
                    ),
                 }}
                 
                 name='Feedback' component={Feedback}/>
                
                 <Drawer.Screen
                  options={{
                
                    headerTitle:'About App',
                    headerTitleStyle:{
                        marginRight:20,
                        fontSize:20,
                        fontFamily:'serif'},
                    drawerIcon: ({focused, size}) => (
                     <View>
                     <Ionicons name="ios-information-circle" size={26} color={focused ? 'white' : '#0f797d'} /> 
                        

                     </View>   
                       
                    ),
                 }}
                 
                 name='About' component={About}/>
            </Drawer.Navigator>
    )
}
export default Mydrawer;