import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Zhungdra from '../screen/Zhungdra';
import Boedra from '../screen/Boedra';
import Other from '../screen/Other';
import Rigser from '../screen/Rigser';
import Main from '../screen/Main';
import { FontAwesome5,Ionicons } from '@expo/vector-icons';
import LottieView from 'lottie-react-native';


const Tab = createMaterialTopTabNavigator();

export default function Mytop() {
  return (
   
      <Tab.Navigator
      initialRouteName='Main'
      screenOptions={{
        tabBarActiveTintColor: "#E40066",
        tabBarInactiveTintColor: "black",
        tabBarActiveBackgroundColor: "#171717",
        tabBarInactiveBackgroundColor: "#171717",
        
        
        headerShown: false,
        tabBarStyle: {
          borderBottomWidth:1,
          borderBottomColor:'#ccc',
          elevation:20,
          shadowColor:'#000'
        },
        style: {
          backgroundColor: "#171717",
          
          
        },
      }}
     >
        <Tab.Screen name="Main" component={Main}
           options={{
             tabBarShowLabel:false,
             tabBarIcon: ({ focused, size }) => (
              <Ionicons name="home" size={24} color={focused? '#E40066':'black'} />
            ),
  
          }}/>
        <Tab.Screen name="Zhungdra" component={Zhungdra}
         options={{
          tabBarLabel:"གཞུང་སྒྲ།",
          tabBarLabelStyle:{
            fontSize:16

          },
    
        }}/>
        <Tab.Screen name="Boedra" component={Boedra} 
        options={{
          tabBarLabel:'འབོད་སྒྲ།',
          tabBarLabelStyle:{
            fontSize:16

          },
        }}/>
        <Tab.Screen name="Rigser" component={Rigser} 
        options={{
          tabBarLabel:'རིག་གསར།',
          tabBarLabelStyle:{
            fontSize:16,
            width:'100%'
          }
        }}/>
        <Tab.Screen name="Other" component={Other} 
        options={{
          tabBarLabel:'ལྷག་མ།',
          tabBarLabelStyle:{
            fontSize:16
          }
        }}/>
      </Tab.Navigator>
   
  );
}
