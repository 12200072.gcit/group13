
import firebase from "firebase/compat";
import "firebase/compat/storage";
import firestore from 'firebase/compat/firestore'

const firebaseConfig = {
  apiKey: "AIzaSyDfr_oVeWC9JtvtVuuvB6ZREggHYQi6fhE",
  authDomain: "luyang-ef300.firebaseapp.com",
  databaseURL: "https://luyang-ef300-default-rtdb.firebaseio.com",
  projectId: "luyang-ef300",
  storageBucket: "luyang-ef300.appspot.com",
  messagingSenderId: "5432122993",
  appId: "1:5432122993:web:59cf95feb7dd4faa28c5d0",
  measurementId: "G-DCTHH1H6YR"
};
firebase.initializeApp(firebaseConfig);
firebase.firestore();

const storage = firebase.storage();


export { storage, firebase as default };
