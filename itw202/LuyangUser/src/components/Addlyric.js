import React,{useState,useEffect} from 'react';
import { StyleSheet,Modal,  View ,Text , ScrollView, Alert,TouchableOpacity,Image} from 'react-native';
import {IconButton ,Appbar, Card,  TextInput
  } from 'react-native-paper';
import Button from '../components/Button'  
import { useNavigation } from '@react-navigation/native';
import * as Animatable from 'react-native-animatable';
import HomeButton from '../../screens/HomeButton';


function Addlyric(){
  const navigation = useNavigation();

  return (
    <>
    <ScrollView>
        <Animatable.View style={styles.centeredView} animation='fadeInUpBig'>
      
               
            <View>
            <TouchableOpacity style={styles.box} onPress={()=>{navigation.navigate('ZhungdraForm')}}> 
            <Image source={require("../../assets/image/side.png")} style={{position:'absolute',zIndex:1,width:45,height:45}}/>
            <Image source={require("../../assets/image/side1.png")} style={{position:'absolute',zIndex:1,width:45,height:45,marginLeft:295}}/>
            <Image source={require("../../assets/image/side4.png")} style={{position:'absolute',zIndex:1,width:45,height:45,marginTop:52}}/>
            <Image source={require("../../assets/image/side3.png")} style={{position:'absolute',zIndex:1,width:45,height:45,marginLeft:295,marginTop:52}}/>
                <View style={styles.inner}>
                  <Text style={styles.text}>Zhungdra</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.box} onPress={()=>{navigation.navigate('BoadraForm')}}>
            <Image source={require("../../assets/image/side.png")} style={{position:'absolute',zIndex:1,width:45,height:45}}/>
            <Image source={require("../../assets/image/side1.png")} style={{position:'absolute',zIndex:1,width:45,height:45,marginLeft:295}}/>
            <Image source={require("../../assets/image/side4.png")} style={{position:'absolute',zIndex:1,width:45,height:45,marginTop:52}}/>
            <Image source={require("../../assets/image/side3.png")} style={{position:'absolute',zIndex:1,width:45,height:45,marginLeft:295,marginTop:52}}/>  

              <View style={styles.inner}>
                <Text style={styles.text}>Boadra</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.box} onPress={()=>{navigation.navigate('RigserForm')}}>
            <Image source={require("../../assets/image/side.png")} style={{position:'absolute',zIndex:1,width:45,height:45}}/>
            <Image source={require("../../assets/image/side1.png")} style={{position:'absolute',zIndex:1,width:45,height:45,marginLeft:295}}/>
            <Image source={require("../../assets/image/side4.png")} style={{position:'absolute',zIndex:1,width:45,height:45,marginTop:52}}/>
            <Image source={require("../../assets/image/side3.png")} style={{position:'absolute',zIndex:1,width:45,height:45,marginLeft:295,marginTop:52}}/>  

              <View style={styles.inner}>
                <Text style={styles.text}>Rigser</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.box} onPress={()=>{navigation.navigate('OtherForm')}}>  
            <Image source={require("../../assets/image/side.png")} style={{position:'absolute',zIndex:1,width:45,height:45}}/>
            <Image source={require("../../assets/image/side1.png")} style={{position:'absolute',zIndex:1,width:45,height:45,marginLeft:295}}/>
            <Image source={require("../../assets/image/side4.png")} style={{position:'absolute',zIndex:1,width:45,height:45,marginTop:52}}/>
            <Image source={require("../../assets/image/side3.png")} style={{position:'absolute',zIndex:1,width:45,height:45,marginLeft:295,marginTop:52}}/>

            <View style={styles.inner}>
              <Text style={styles.text}>Others</Text>
            </View>
          </TouchableOpacity>
          </View>              
      </Animatable.View>
  
    </ScrollView>
    <HomeButton/>
    </>
  )
}
export default Addlyric;
const styles = StyleSheet.create({
  centeredView: {
      
    marginTop: 50,
  },
  box:{
    marginRight:10,
    width:'95%',
    marginLeft:10,
    height:100,
    padding:5,
    marginTop:10,
    borderRadius:5,
    opacity:0.7,
    elevation:0.6,
    marginTop:10,
    shadowOffset: {
      width: 0,
      height: 0,
    },
     shadowOpacity: 0.2,
     shadowRadius: 1.41,
     elevation:3,
  

  },
  inner:{
    flex:1,
    backgroundColor:'#eee',
    alignItems:'center',
    justifyContent:'center',

  },
  text:{
    fontSize:20,
    fontFamily:'serif',

  },
});