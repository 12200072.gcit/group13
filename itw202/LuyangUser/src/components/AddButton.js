import { View, Text } from 'react-native'
import React from 'react'
import Addlyric from './Addlyric'
import { TouchableOpacity,Image } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { MaterialCommunityIcons } from '@expo/vector-icons'
export default function AddButton() {

    const navigation=useNavigation();
  return (
    <TouchableOpacity onPress={()=>{navigation.navigate('Addlyric')}} 
    style={{backgroundColor:'#C8E6D9',
    width:55,
    alignItems:'center',
    justifyContent:'center',
    height:55,
    position:'absolute',
    zIndex:1,
    top:560,
    left:290,
    borderRadius:60}}>
        <View>
            {/* <Image source={require('../../assets/image/hand.png')} style={{width:40,height:40}}/> */}
            <MaterialCommunityIcons name="pencil-plus-outline" size={35} color="black" />
        </View>

    </TouchableOpacity>
  )
}